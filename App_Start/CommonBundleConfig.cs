﻿using System.Web;
using System.Web.Optimization;

namespace Softwords.Web
{
  
    // by convention this is placed here - a static method on a public class
    // this is usually invoked from global.asax
    // but 
    public class CommonBundleConfig
    {

           // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /************************************************************************
            // css
             ***********************************************************************/
            // these are provided by the Forza theme
            bundles.Add(new StyleBundle("~/assets/css/theme").Include(

                "~/bower_components/seiyria-bootstrap-slider/dist/css/bootstrap-slider.css",
                "~/bower_components/angular-ui-tree/dist/angular-ui-tree.min.css",
                "~/bower_components/angular-toggle-switch/angular-toggle-switch.css",
                //"~/bower_components/ng-grid/ng-grid.css",  obselete
                "~/bower_components/angular-xeditable/dist/css/xeditable.css",
                "~/bower_components/angular-ui-select/select.css",

                "~/bower_components/jScrollPane/style/jquery.jscrollpane.css",


                "~/assets/fonts/glyphicons/css/glyphicons.min.css",
                "~/assets/plugins/icheck/all.css",
                "~/assets/plugins/form-multiselect/css/multi-select.css",
                "~/assets/plugins/form-fseditor/fseditor.css",
                "~/assets/plugins/form-tokenfield/bootstrap-tokenfield.css",
                "~/assets/plugins/datepaginator/bootstrap-datepaginator.css",
                "~/assets/plugins/jquery-fileupload/css/jquery.fileupload-ui.css",
                "~/assets/plugins/bootstro.js/bootstro.min.css",
                "~/assets/plugins/progress-skylo/skylo.css",
                "~/assets/plugins/jcrop/css/jquery.Jcrop.min.css",
                "~/assets/plugins/form-daterangepicker/daterangepicker-bs3.css",
                "~/assets/plugins/form-markdown/css/bootstrap-markdown.min.css",

                "~/assets/plugins/codeprettifier/prettify.css",
                "~/assets/plugins/form-select2/select2.css",
                "~/assets/plugins/fullcalendar/fullcalendar.css",
                "~/assets/plugins/pines-notify/pnotify.custom.css",

                // this is the output of the grunt task less:theme
                "~/assets/css/theme.css"
                )
           );
            // styes required by angular components
            bundles.Add(new StyleBundle("~/assets/css/angular").Include(
                    "~/bower_components/angular-ui-grid/ui-grid.css"
                    , "~/bower_components/dcjs/dc.css"

                 )
            );

            // point this to assets/css so, any reference to a font-face in a css
            // can be prefeixed ../fonts
            // then put the fonts there
            bundles.Add(new StyleBundle("~/assets/css/app")
                .IncludeDirectory("~/_client/common/css","*.css", true)
                .IncludeDirectory("~/_client/app/css", "*.css", true)
                .Include(
                // created by the grunt LESS dist task - for now its just theme.css
                //"~/assets/css/scholar.css",
                // see the comments below re font paths in css
                // get around this in font-awesome by using a cdn for the actual fonts
                // 

                    // schlar customisations of other stylesheets
                // ui-grid
                // note there is always a going ot be a problem with
                // styles hseets (such as font-awesome and ui-grid
                // that reference fonts by releative path, if the bundling does not preserve the 
                // css file's path. So this file is in assets/css whether bundled or not
                // That way, its font path can be consistent
                    "~/assets/css/uigrid_customise.css"
                    )

           );




            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            // ths bundle adds enquire which checks for media query stuff - Forza has a service that depends on this
            bundles.Add(new ScriptBundle("~/scripts/modernizr_enquire").Include(
                "~/bower_components/modernizr/modernizr.js",
                
                // enquire needs matchMedia polyfill for ie 9 otherwise you get
                // matchMedia not present, legacy browsers require a polyfill
                "~/bower_components/enquire/dist/matchMedia.js",
                "~/bower_components/enquire/dist/matchMedia.addListener.js",
                "~/bower_components/enquire/dist/enquire.js"
                )
            );


            // j-query
            bundles.Add(new ScriptBundle("~/scripts/jquery")
                 .Include(
                         "~/bower_components/jquery/dist/jquery.js"
                 )
            );

            // underscore
            bundles.Add(new ScriptBundle("~/scripts/_")
                .Include(
                // "~/bower_components/underscore/underscore.js"
                       "~/bower_components/lodash/dist/lodash.js"
                )
            );


            // map addins
            bundles.Add(new ScriptBundle("~/scripts/maps")
                .Include(
                // includedirectory does not find these because of the extra . s in the name?
                   "~/assets/mapAddins/d3.geo.projection.v0.min.js",
                   "~/assets/mapAddins/topojson.v1.min.js"
                )
                .IncludeDirectory("~/assets/mapAddins", "*.js", true)

            );

            bundles.Add(new ScriptBundle("~/scripts/angular")
                .Include(
                        "~/bower_components/angular/angular.js"
                )
            );
            // bundle for the core of angular, installed from bower
            // this is The Stack:
            // angular
            // restangular
            // ng-token-auth
            // ui-router
            //ui-grid
            //ui-select
            bundles.Add(new ScriptBundle("~/scripts/ng")
                .Include(
                        "~/bower_components/restangular/dist/restangular.js"
                        , "~/bower_components/angular-ui-router/release/angular-ui-router.js"
                // heavily customised version
                        , "~/bower_components/ng-token-auth/current/ng-token-auth.js"
                // include angular bootstrap i9n the core
                //, "~/bower_components/angular-bootstrap/ui-bootstrap.js"
                // moved to the scripts folder becuase of problems referencing css when minified and the path changes
                        , "~/scripts/angular-ui/ui-bootstrap-tpls.js"
                        , "~/bower_components/angular-animate/angular-animate.js"
                        , "~/bower_components/angular-ui-grid/ui-grid.js"
                        , "~/bower_components/angular-pnotify/src/angular-pnotify.js"
                // angular-ui-select
                        , "~/bower_components/angular-sanitize/angular-sanitize.js"
                        , "~/bower_components/angular-ui-select/select.js"
                //angularjs-google-maps not this is not the angular-ui maps component
                        , "~/bower_components/ngmap/build/scripts/ng-map.js"
                )
            );



            // d3 and charting support
            //-------------------------
            // d3
            // cross filter
            // dc
            // dimple
            // angular-dimple
            bundles.Add(new ScriptBundle("~/scripts/d3")
                .Include(
                        "~/bower_components/d3/d3.js"
                        , "~/bower_components/crossfilter/crossfilter.js"
                        , "~/bower_components/dcjs/dc.js"
                        , "~/bower_components/dimple/dist/dimple.v2.1.0.js"
                        , "~/bower_components/angular-dimple/angular-dimple.js"
                        , "~/bower_components/angular-dc/dist/angular-dc.js"
                        )
           );

            // support nvd3 in angular
            bundles.Add(new StyleBundle("~/assets/css/nvd3")
                .Include(
                      "~/bower_components/nvd3/nv.d3.css"
                        )
            );


            bundles.Add(new ScriptBundle("~/scripts/nvd3")
                .Include(
                        "~/bower_components/nvd3/nv.d3.js"
                        , "~/bower_components/angularjs-nvd3-directives/dist/angularjs-nvd3-directives.js"
                        )
           );


            bundles.Add(new ScriptBundle("~/scripts/flot")
                 .Include("~/bower_components/flot/jquery.flot.js"
                         , "~/bower_components/flot/jquery.flot.stack.js"
                         , "~/bower_components/flot/jquery.flot.pie.js"
                         , "~/bower_components/flot/jquery.flot.resize.js"
                         , "~/bower_components/flot.tooltip/js/jquery.flot.tooltip.js"
                 )
            );



            // jquery ui 
            bundles.Add(new ScriptBundle("~/scripts/jqui")
                 .Include("~/bower_components/jquery.ui/ui/jquery.ui.core.js"
                     , "~/bower_components/jquery.ui/ui/jquery.ui.widget.js"
                     , "~/bower_components/jquery.ui/ui/jquery.ui.mouse.js"
                     , "~/bower_components/jquery.ui/ui/jquery.ui.draggable.js"
                     , "~/bower_components/jquery.slimscroll/jquery.slimscroll.js"
                     , "~/bower_components/jquery.easing/js/jquery.easing.min.js"

                     , "~/bower_components/jquery.pulsate/jquery.pulsate.js"
                     , "~/bower_components/jquery.knob/js/jquery.knob.js"
                     , "~/bower_components/jquery.sparkline/index.js"

                 )
             );

            // angular ui
            bundles.Add(new ScriptBundle("~/scripts/ngui")
                .Include("~/bower_components/angular-ui-tree/dist/angular-ui-tree.js"
                    , "~/bower_components/angular-toggle-switch/angular-toggle-switch.min.js"
                    , "~/bower_components/ng-grid/build/ng-grid.js"
                    , "~/bower_components/angular-xeditable/dist/js/xeditable.js"
                    , "~/bower_components/select2/select2.js"
                    , "~/bower_components/angular-ui-select2/src/select2.js"
                // alternative to angular-ui google maps
                    , "~/bower_components/ngmap/build/scripts/ng-map.min.js"
                )
            );
            // bootstrap base
            bundles.Add(new ScriptBundle("~/scripts/bootstrap")
                .Include("~/bower_components/bootstrap/dist/js/bootstrap.js"
                     , "~/bower_components/bootbox.js/bootbox.js"
                )
            );

            bundles.Add(new ScriptBundle("~/scripts/x")
                .Include(
                    "~/bower_components/seiyria-bootstrap-slider/js/bootstrap-slider.js"
                    , "~/bower_components/iCheck/icheck.min.js"
                    , "~/bower_components/google-code-prettify/src/prettify.js"
                    , "~/bower_components/jquery-autosize/jquery.autosize.js"
                    , "~/bower_components/gmaps/gmaps.js"
                    , "~/bower_components/momentjs/moment.js"
                    , "~/bower_components/bootstrap-daterangepicker/daterangepicker.js"
                    , "~/bower_components/flow.js/dist/flow.js"
                    , "~/bower_components/ng-flow/dist/ng-flow.js"
                    , "~/bower_components/jScrollPane/script/jquery.mousewheel.js"
                    , "~/bower_components/jScrollPane/script/mwheelIntent.js"
                    , "~/bower_components/jScrollPane/script/jquery.jscrollpane.min.js"
                    , "~/bower_components/enquire/dist/enquire.js"
                    , "~/bower_components/shufflejs/dist/jquery.shuffle.js"

              )
          );

            // components of theme       
            bundles.Add(new ScriptBundle("~/scripts/theme")
                .Include(
                    "~/assets/plugins/pines-notify/pnotify.custom.js",
                // this is the nice thin loader at the top of forza - build into state change 
                // forza adds various other progress bars

                    "~/assets/plugins/progress-skylo/skylo.js",
                // this provides the sparkline support
                    "~/bower_components/jquery.sparkline/index.js"


                )
                .IncludeDirectory("~/_client/theme", "*.js", true)
             );

            // soft words common
            bundles.Add(new ScriptBundle("~/scripts/common")
            .Include(
                "~/_client/common/swcommon.js"
                )
            .IncludeDirectory("~/_client/common/js", "*.js", true)


            );

            // application
            bundles.Add(new ScriptBundle("~/scripts/app")
                .Include(
                      "~/_client/app/js/*.js")
                .IncludeDirectory("~/_client/app/js/base", "*.js", true)
                .IncludeDirectory("~/_client/app/js/features", "*.js", true)

            );



            //  BundleTable.EnableOptimizations = false;


        }

       
    }
}
    

