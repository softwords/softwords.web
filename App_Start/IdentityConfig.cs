﻿using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Softwords.Web.Models;
using ad = System.DirectoryServices.AccountManagement;



// this boilerplate comes out of the local App_Starand is referenced from here, 
// picking up the Softwords.Web.Models versions of ApplicationUser
// and ApplicationDbContext

namespace Softwords.Web
{
    // Configure the application user manager used in this application. 
    //  UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {

        }

        private ApplicationDbContext cxt = null;
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames

            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            // save the ApplicationDbcontext for later
            // this link has interesting things to say about these dependencies
            //https://aspnetidentity.codeplex.com/discussions/550953

            manager.cxt = context.Get<ApplicationDbContext>();

            return manager;

        }


        public async override Task<ApplicationUser> FindAsync(string userName, string password)
        {

            ApplicationUser user = await base.FindAsync(userName, password);

            if (user != null)
            {
                user.IsDomainUser = false;
                // have to put this somewhere to get the ClientMenu set - for domain users, its in makeAdUser
                // needs a rethink....
                NavigationManager nm = new NavigationManager();
                user.ClientMenu = await nm.getNavigationMenu(user.MenuKey);
                return user;
            }
            // see if this is an active directory account
            // but only if its not an email
            if (userName.IndexOf("@") == -1)
            {
                // create a "principal context" - e.g. your domain (could be machine, too)
                //using(ad.PrincipalContext pc = new ad.PrincipalContext(ad.ContextType.Machine, "keenyah"))
                //
                // save us some web.config vars
                ad.PrincipalContext pc = getPrincipalContext();
                using (pc)
                {
                    // validate the credentials
                    bool isValid = pc.ValidateCredentials(userName, password);

                    if (isValid)
                    {
                        // create a user now

                        user = await makeAdUser(userName);
                    }
                }
            }
            return user;
        }
        public async override Task<ApplicationUser> FindByNameAsync(string userName)
        {
            ApplicationUser user = await base.FindByNameAsync(userName);
            if (user != null)
            {
                user.IsDomainUser = false;
                return user;
            }
            // second attempt - try the active directory
            var pc = getPrincipalContext();
            ad.UserPrincipal p = ad.UserPrincipal.FindByIdentity(pc, userName);
            if (p != null)
            {
                user = await makeAdUser(userName);
            }
            return user;
        }



        public ApplicationUser FindByName(string userName)
        {
            // the extension does call the async version
            ApplicationUser user = UserManagerExtensions.FindByName(this, userName);

            return user;


        }

        public IList<string> GetRoles(string userId)
        {
            IList<string> ls = UserManagerExtensions.GetRoles(this, userId);
            return ls;
        }
        public async override Task<IList<string>> GetRolesAsync(string userId)
        {
            // are we returning roles out of the database, or active directory groups?
            if (userId.Contains(@"\"))
            {
                ad.PrincipalContext cxt = getPrincipalContext();
                ad.UserPrincipal u = ad.UserPrincipal.FindByIdentity(cxt, userId);
                ad.PrincipalSearchResult<ad.Principal> grps = u.GetGroups();
                List<string> roles = new List<string>();

                foreach (ad.GroupPrincipal g in grps)
                {
                    roles.Add(g.Name);
                }
                return roles;

            }


            return await base.GetRolesAsync(userId);
        }

        public ad.PrincipalContext getPrincipalContext()
        {
            return new ad.PrincipalContext(
                    (string.Equals(System.Environment.UserDomainName, System.Environment.MachineName, System.StringComparison.OrdinalIgnoreCase) ?
                           ad.ContextType.Machine : ad.ContextType.Domain),
                    System.Environment.UserDomainName);
        }

        async Task<ApplicationUser> makeAdUser(string userName)
        {
            ApplicationUser user = new ApplicationUser();
            user.IsDomainUser = true;
            if (!userName.StartsWith(System.Environment.UserDomainName + @"\"))
                userName = System.Environment.UserDomainName + @"\" + userName;
            user.Email = userName;

            user.Id = user.Email;             // this would otherwise by a guid as a string
            user.UserName = user.Email;
            // use Entity Fromaework to get the menu?
            user.MenuKey = "ALL";                // some default from the AD group membership?
            user.Navigation = cxt.Navigation.Find(user.MenuKey);
            NavigationManager nm = new NavigationManager();
            user.ClientMenu = await nm.getNavigationMenu(user.MenuKey);

            return user;

        }

    }
}
