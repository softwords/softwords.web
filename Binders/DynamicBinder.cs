﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Dynamic;
using Softwords.Web;

using System.Security.Claims;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using Newtonsoft.Json;

namespace Softwords.Web.Models
{
    /// <summary>
    /// This binder is only required to enforce IncludeNulls in the specific case that a binder is being serialised
    /// that is, that a package of updated data is being sent back to the client.
    /// The problem is in indirect updating of some image of the updated record - if the Null value is not there, 
    /// we wont know this field got changed.
    /// Specifcally, suppose an array holds an image of a record with a non-null Address1. We edit this record and remove this Address1
    /// The data is broadcats to the controller holding the array - which applies that new data to the array using extend ( or similar)
    /// However, without IncludeNulls, Address1 won;t be in the newData package, so the current value will not get 
    /// cleared out.
    /// </summary>
    public class DynamicBinderWriteConverter : Newtonsoft.Json.JsonConverter
    {

        // inbound serialization presents no problems, so don;t override it ("it ain't broke....")
        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        // any generic DynamicBinder
        public override bool CanConvert(Type objectType)
        {
            return typeof(DynamicBinder<object>).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // this is the key point - incude nulls when returning the dictionary
            serializer.NullValueHandling = NullValueHandling.Include;
            DynamicDictionary dd = value as DynamicDictionary;
            if (dd != null)
            {
                serializer.Serialize(writer, dd.definedProps);
            };
        }
    }

    [JsonConverter(typeof(DynamicBinderWriteConverter))]
    public class DynamicBinder<T> : DynamicDictionary where T : class, new()
    {
        protected string rowVersionProp { get; set; }
        protected string keyProp { get; set; }

        // property in the class that is an Identity column
        // detected by looking for the attribute
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        // Note that this will in normal usage also be the keyProp
        // But in classes that are createTagged they will differ.
        // CreateTagged classes represent editable views that have been equipped with a Guid to act as a key during creation
        // Otherwise EF can't find the newly added record (becuase scope_identity() does not retrieve the ID added in the trigger
        protected string identityProp { get; set; }

        protected ClaimsIdentity identity { get; set; }
        protected DbContext context { get; set; }

        #region constructors
        public DynamicBinder()
        {
            rowVersionProp = getRowVersionProp();
            keyProp = getKeyProp();
            identityProp = getIdentityProp();

        }
        /// <summary>
        /// construct the binder and populate from an exisitng object
        /// </summary>
        /// <param name="seedobject"></param>
        public DynamicBinder(T seedobject)
        {
            rowVersionProp = getRowVersionProp();
            keyProp = getKeyProp();

            LoadValues(seedobject);
        }


        #endregion
        public DynamicBinder<T> Identity(ClaimsIdentity identity)
        {
            this.identity = identity;
            return this;
        }
        public DynamicBinder<T> Context(DbContext context)
        {
            this.context = context;
            return this;
        }
  

        public dynamic Properties()
        {
            return Properties(true);
        }
        public dynamic Properties(bool strict)
        {
            Strict = strict;
            return this as dynamic;
        }

        /// <summary>
        /// alis the key field to ID, whatever the field name is
        /// this is determined by looking for the [Key] attribute
        /// Note we are assume it is a SINGLE field
        /// </summary>
        public virtual object ID
        {
            get
            {

                return getProp(keyProp);
            }
            set
            {
                definedProps.Add(keyProp, value);
            }
        }

        /// <summary>
        /// Save the current data; either by creating or updating
        /// </summary>
        /// <returns></returns>
        public virtual async Task<T> Save()
        {
            DbSet<T> dbset = context.Set<T>();
            T entity = await dbset.FindAsync(ID);
            if (entity == null)
            {
                return await Create();
            }

            SetValues(entity);
            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
            if (rowVersionProp != String.Empty && definedProps.ContainsKey(rowVersionProp))
                entry.OriginalValues[rowVersionProp] = Convert.FromBase64String((string)definedProps[rowVersionProp]);

            await SaveChanges();
            GetValues(entity);
            return entity;

        }
        public virtual async Task<DynamicBinder<T>> Update()
        {
            return await Update(this.ID);
        }

        public virtual async Task<DynamicBinder<T>> Update(object updateID)
        {

            T entity = await GetEntity(updateID);
            if (entity == null)
            {
                throw RecordNotFoundException.Make(ID, null);
            }

            SetValues(entity);
            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
            // support for concurrency checking via a timestamp property
            if (rowVersionProp != null && rowVersionProp != String.Empty)
                entry.OriginalValues[rowVersionProp] = Convert.FromBase64String((string)definedProps[rowVersionProp]);

            await SaveChanges();
            GetValues(entity);
            return this;

        }
        public async Task<T> Read(object searchID)
		{
            return await GetEntity(searchID);
		}
        private async Task<T> GetEntity(object searchID)
		{
            DbSet<T> dbset = context.Set<T>();
            if (identityProp == null || identityProp == keyProp|| !searchID.GetType().IsAssignableFrom(typeof(int)))
			{
                return await dbset.FindAsync(ID);
            }
            // if the search is an int, and identityProp is not the key prop, search on the identityProp
            return dbset.Where(PropertyEquals(identityProp, searchID)).FirstOrDefault();
		}

        /// <summary>
        /// Generate the expression to match a property on the generic T
        /// slightly adapted (type parameters removed) from:
        /// https://stackoverflow.com/questions/22104050/linq-to-entities-does-not-recognize-the-method-system-object-getvalue
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private Expression<Func<T, bool>> PropertyEquals(string propName, object value)
        {
            var propInfo = typeof(T).GetProperty(propName);
            var param = Expression.Parameter(typeof(T));
            var body = Expression.Equal(Expression.Property(param, propInfo), Expression.Constant(value));
            return Expression.Lambda<Func<T, bool>>(body, param);
        }
        public virtual async Task<DynamicBinder<T>> Update(T entity)
        {
            DbSet<T> dbset = context.Set<T>();

            if (entity == null)
            {
                throw RecordNotFoundException.Make(ID, null);
            }

            //if (rowVersionProp != null && (Convert.ToBase64String(entity.Rowversion.ToArray()) != RowVersion))
            //{
            //    // optimistic concurrency error
            //    // we return the most recent version of the record
            //    FromDb(entity);
            //    throw new ConcurrencyException(this.definedProps);
            //}
            SetValues(entity);
            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
            // support for concurrency checking via a timestamp property
            if (rowVersionProp != null && rowVersionProp != String.Empty)
                entry.OriginalValues[rowVersionProp] = Convert.FromBase64String((string)definedProps[rowVersionProp]);

            await SaveChanges();
            GetValues(entity);
            return this;

        }
        public virtual async Task<T> Create()
        {
            DbSet<T> dbset = context.Set<T>();

            T entity = new T();


            dbset.Add(entity);
            SetValues(entity);

            await SaveChanges();
            return entity;
        }

		/// Delete
		/// 5 overrides
		/// Delete(object ID)
		///      ID the key of the entity to delete
		///       this will force the delete by ignoring the rowVersion
		///       
		/// Delete(object ID, string rowVersion)
		///      ID the key of the entity to delete
		///      rowVersion -- the rowVErsion known to the client requesting the delete, as a string representation
		///      if != the current rowVersion in the dbset, throw a concurrency error
		///      (ie the entity has been altered since the client read it
		///
		/// Delete(object ID, byte[] rowversion)
		///      ID the key of the entity to delete
		///      rowVersion -- the rowVersion known to the client requesting the delete, as a byte array
		///      this will mainly be used if the object to delete is instantited on the server, rather than passed back
		///      from the client
		///     
		/// Delete()
		///        gets the ID and rowVersion from the object that instantiated the dynamicBinderie, a json object  <summary>
		///        Delete endpoint
		///        
		/// Delete(T entity)
        ///         Delete the entity - no checking of concurrency
		
		/// <summary>
		/// Delete a row from a supplied ID. No concurrency check
		/// </summary>
		/// <param name="ID">the ID (primary key or identity column if different)</param>
		/// <returns>the image of the deleted entity</returns>
		public virtual async Task<T> Delete(object ID)
        {
            DbSet<T> dbset = context.Set<T>();
            T entity = await GetEntity(ID);
            if (entity == null)
            {
                throw RecordNotFoundException.Make(ID, null);
            }
            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
            // force the rowVersion to force a delete
            entry.OriginalValues[rowVersionProp] = entry.CurrentValues[rowVersionProp];
            dbset.Remove(entity);
            await SaveChanges();
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="rowversion">string representation of the row version</param>
        /// <returns></returns>
        public virtual async Task<T> Delete(object ID, string rowversion)
        {
            DbSet<T> dbset = context.Set<T>();
            T entity = await GetEntity(ID);
            if (entity == null)
            {
                throw RecordNotFoundException.Make(ID, null);
            }

            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
            if (Convert.ToBase64String((byte[])entry.CurrentValues[rowVersionProp]) != rowversion)
            {
                throw ConcurrencyException.Make(rowversion, entity, null);
            }

            dbset.Remove(entity);
            await SaveChanges();
            return entity;
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="rowversion">byte array representation of the row version</param>
        /// <returns></returns>
        public virtual async Task<T> Delete(object ID, byte[] rowversion)
		{
            return await Delete(ID, Convert.ToBase64String(rowversion));
        }

        /// <summary>
        /// Delete the entity current in definedProps
        /// </summary>
        /// <returns></returns>
        public virtual async Task<T> Delete()
        {
            return await Delete(this.ID, (string)getProp(rowVersionProp));
        }
        public virtual async Task<T> Delete(T entity)
        {
            DbSet<T> dbset = context.Set<T>();
            if (entity == null)
            {
                throw RecordNotFoundException.Make(ID, null);
            }
            System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);

            //entry.OriginalValues[rowVersionProp] = Convert.FromBase64String((string)definedProps[rowVersionProp]);
            dbset.Remove(entity);
            await SaveChanges();
            return entity;
        }

        /// <summary>
        /// Set the values on the object from the values in the binder
        /// </summary>
        /// <param name="s">object to apply values to</param>
        public void SetValues(T s)
        {
            DateTime now = DateTime.UtcNow;     // be sure not to get weird variation between create and edit
            foreach (System.Reflection.PropertyInfo piDest in s.GetType().GetProperties())
            {
                string propname = piDest.Name;
                if (!isDbGenerated(piDest))
                {
                    ChangeTrackingAttribute ctattr = getChangeTrackAttribute(piDest);
                    if (ctattr == null)
                    {
                        if (definedProps.ContainsKey(propname))
                        {
                            if (propname == keyProp && piDest.GetValue(s) == null && forceUpperCase(piDest))
                            {
                                piDest.SetValue(s, ((string)definedProps[propname]).ToUpper());
                            }
                            else
                            {
                                // deal with the type conversion and nullable issues in this case
                                SetValue(piDest, s, definedProps[propname]);
                            }
                        }

                    }
                    else
                    {
                        switch (ctattr.ChangeTrackingOption)
                        {
                            case ChangeTrackingOption.CreateUser:
                                // if a new row, use the current user
                                if (piDest.GetValue(s) == null)
                                {
                                    piDest.SetValue(s, identity.Name);
                                }
                                break;
                            case ChangeTrackingOption.CreateDateTime:
                                if (piDest.GetValue(s) == null)
                                {
                                    piDest.SetValue(s, now);
                                }
                                break;
                            case ChangeTrackingOption.EditUser:
                                piDest.SetValue(s, identity.Name);
                                break;
                            case ChangeTrackingOption.EditDateTime:
                                piDest.SetValue(s, now);
                                break;
                            case ChangeTrackingOption.CreateTag:
                                // createTag is a guid field that can be a "fake" primary key on an editable view
                                // most ORMS want to use scope_identity() to retrieve the newly added record
                                // this will fail when the record is added on an insert trigger on a view
                                // by saying that CreateTag is the primary key, and assigning it outside the database
                                // EF can find our record again
								// alternatively, you can assign a createtag on a guid primary key field
								// to have its value set here. This means that this key value is known at insert time
								// - useful on triggers that want to insert into related tables on insert

                                // don't overwrite the createtag if its already on the record
								// note that new Guid() is a 'zero' guid - 0000-000 etc 
                                if (piDest.GetValue(s) == null || (Guid)piDest.GetValue(s) == new Guid())
                                {
                                    // use a passed in value if the client really wants to know the guid ahead of time....
                                    // used to be important in MS Access
                                    if (definedProps.ContainsKey(propname) && definedProps[propname] != null)
                                        SetValue(piDest, s, definedProps[propname]);
                                    else
                                        piDest.SetValue(s, Guid.NewGuid());
                                };
                                break;
                        }
                        // any change-track field should get sent back in the response
                        // even when it was not sent by the client (or may not currently BE on the client)
                        if (!definedProps.ContainsKey(propname))
                        {
                            definedProps.Add(propname, piDest.GetValue(s));
                        }
                    }

                }

            }
        }

        /// <summary>
        /// get the values in the binder from an object
        /// Only updates values that are already in the binder
        /// These are assumed to be the values that the client has sent - and will expect to get back
        /// </summary>
        /// <param name="s">s object to get property values from</param>
        public void GetValues(T s)
        {
            string[] keys = new string[definedProps.Keys.Count];
            definedProps.Keys.CopyTo(keys, 0);
            foreach (var propname in keys)
            {
                System.Reflection.PropertyInfo piDest = s.GetType().GetProperty(propname);
                if (piDest != null)
                    definedProps[propname] = piDest.GetValue(s);
                else
                {
                    // if other values have been sent that are not properties of the object, don;t return them
                    // this overcomes an issue with _rowversion being transmitted back and forth, and breaking optimistic concurrency
                    definedProps.Remove(propname);
                }
            }
        }

        /// <summary>
        /// get the values in the binder from an object
        /// Adds all properties to binder, updating if they already exist
        /// These are assumed to be the values that the client has sent - and will expect to get back
        /// </summary>
        /// <param name="s">s object to get property values from</param>
        public void LoadValues(T s)
        {
            foreach (System.Reflection.PropertyInfo piDest in s.GetType().GetProperties())
            {
                string propname = piDest.Name;
                definedProps.InsertOrUpdate(propname, piDest.GetValue(s));
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await context.SaveChangesAsync();

            }
            // error reporting : step 1 of 4
            // 1) construct the error using a wrapper derived from ClientException - the original exception becomes the inner exception
            // 2) catch this error in the global exception filter, and decide what status code to return with it
            // 3) send the error - it is serialised to Json using ClientExceptionJsonConverter
            // 4) the errors are interpreted by ApiUi service at the client - the options appropriate to each error are presented
			// note this error is sometimes incorrectly thrown - in cases where a view is being updated, and scopeidentity() fails to 
			// return the record just added - remedy is in correcting the trigger, 
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                // return the record we just read
                // pass back only the properties that were passed in
                var entry = ex.Entries.Single();
                var dbvals = await entry.GetDatabaseValuesAsync();
                if (dbvals != null)
                {
                    T entity = (T)dbvals.ToObject();
                    GetValues(entity);
                }
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps, ex);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                // implies some failure on update, such as trying to write a null into a non-null
                // field. These shoul dendeavour to be caught client-side, but if they sneak through
                // we should report as a 400 error, not a plain old 500 
                string theError = ex.Message;
                Exception baseEx = ex.GetBaseException();
                if (baseEx is System.Data.SqlClient.SqlException)
                {
                    throw parseSqlError(baseEx as System.Data.SqlClient.SqlException);
                }
                // can't imagine how this will not be a Sql error inside this EF wrapper?
                throw new ClientException(ClientExceptionType.dbUpdate, null, ex.Message, ex);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw parseSqlError(ex as System.Data.SqlClient.SqlException);
            }
            // this is a validation thrown by Entity Framework, from the attributes
            // of the model object. ie this error is thrown without attempting to save to the database
            // the messages returned are determined by any messages associated to the attributes
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                throw new ValidationException(ex);
            }
            catch (Exception ex)
            {
                // return as 500 error
                throw ex;
            }
        }
        private System.Exception parseSqlError(System.Data.SqlClient.SqlException sqlEx)
        {

            string theError = sqlEx.Message;
            switch (sqlEx.Number)
            {
                case (int)SqlErrors.CannotInsertNull:
                    return Softwords.Web.ValidationException.Make(sqlEx);
                case (int)SqlErrors.DuplicateKey:
                    return Softwords.Web.DuplicateKeyException.Make(keyProp, definedProps[keyProp], sqlEx);

            }
            if (theError.StartsWith("<ValidationError"))
            {
                // various stored procs return a formatted Xml <ValidationError/> as an error message
                // to assist the client in responding in a structured way ( ie change focus) 
                System.Xml.Linq.XDocument x = System.Xml.Linq.XDocument.Parse(theError);
                string field = x.Root.Attribute("field").Value;
                string message = x.Root.Nodes().First().ToString();
                return new Softwords.Web.ValidationException("Validation", field, message, sqlEx);
            }
            // can't do any better than this thin wrapper to tell the client we had a Sql error
            return new Softwords.Web.ClientException("Sql", null, sqlEx.Message, sqlEx);
        }



        #region structureInfo - methods using reflection to provide info about the structure of the target object

        /// <summary>
        /// Will return the name of the property bearing the Timestamp attribute
        /// </summary>
        /// <returns></returns>
        private string getRowVersionProp()
        {
            Type t = typeof(T);
            var prop = t.GetProperties().FirstOrDefault(p =>
              p.GetCustomAttributes(true).Any(a => a is System.ComponentModel.DataAnnotations.TimestampAttribute)
          );
            return (prop != null ? prop.Name : String.Empty);
        }

        /// <summary>
        /// Will return the name of the property bearing the Timestamp attribute
        /// </summary>
        /// <returns></returns>
        private string getIdentityProp()
        {
            Type t = typeof(T);
            var prop = t.GetProperties().FirstOrDefault(p =>
              p.GetCustomAttributes(true).Any(a => a is System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute dga
                && dga.DatabaseGeneratedOption == System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
            );
            return (prop != null ? prop.Name : String.Empty);
        }

        /// <summary>
        /// test if the property is dbGenerated, from its attributes
        /// </summary>
        /// <param name="piDest">preorpty to test</param>
        /// <returns>true if property is dbGenerated</returns>
        private bool isDbGenerated(System.Reflection.PropertyInfo piDest)
        {
            foreach (var attr in piDest.GetCustomAttributes(true))
            {
                // return true if its a timestamp
                if (attr is System.ComponentModel.DataAnnotations.TimestampAttribute)
                {
                    return true;
                }
                if (attr is System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute)
                {
                    var dg = attr as System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute;
                    if (dg.DatabaseGeneratedOption != System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)
                    {
                        return true;
                    }
                    return false;
                }
            }
            // no DatabaseGenerated attribute
            return false;
        }

        /// <summary>
        /// Return the name of the property marked with the key attribute
        /// Only works with single part keys
        /// </summary>
        /// <returns>the property name</returns>
        private string getKeyProp()
        {
            Type t = typeof(T);
            var key = t.GetProperties().FirstOrDefault(p =>
                    p.CustomAttributes.Any(attr => attr.AttributeType == typeof(KeyAttribute)));

            return (key != null ? key.Name : string.Empty);
        }

        /// <summary>
        /// Get any ChangeTracking attribute for the property
        /// </summary>
        /// <param name="piDest"></param>
        /// <returns>the change tracking attribute enum if any associated to this property</returns>
        private ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
        {
            return (ChangeTrackingAttribute)piDest.GetCustomAttributes(true).FirstOrDefault(a => a is ChangeTrackingAttribute);
        }

        private bool forceUpperCase(System.Reflection.PropertyInfo piDest)
        {
            return piDest.GetCustomAttributes(false).Any(a => a is Softwords.Web.Models.ForceUpperCaseAttribute);
        }
        #endregion

        #region nullable support
        //http://technico.qnownow.com/how-to-set-property-value-using-reflection-in-c/
        //https://stackoverflow.com/questions/13270183/type-conversion-issue-when-setting-property-through-reflection
        private void SetValue(System.Reflection.PropertyInfo piDest, object dest, object propertyVal)
        {
            //find the property type
            Type propertyType = piDest.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable - AND the value is NOT null - we need to get the underlying type of the property
            if (propertyVal != null)
            {
                var targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;

                if (targetType == typeof(Guid))
                {
                    propertyVal = Guid.Parse(propertyVal.ToString());
                } else if (targetType == typeof(DateTimeOffset)) {
                    if (propertyVal.GetType() == typeof(DateTime))
                    {
                        DateTime pv = (DateTime)propertyVal;
                        propertyVal = new DateTimeOffset(pv);
                    }
                    //propertyVal = Convert.ChangeType(propertyVal, targetType);
                }
                else if (targetType == typeof(TimeSpan))
                {
                    propertyVal = DateTime.Parse((string)propertyVal).TimeOfDay;
                }
                else if (targetType == typeof(String) && (String)propertyVal == String.Empty)
                {
                    propertyVal = null;
                }
                else
                {
                    //Returns an System.Object with the specified System.Type and whose value is
                    //equivalent to the specified object. doesn;t work with Guid
                    propertyVal = Convert.ChangeType(propertyVal, targetType);
                }

            }
            //Set the value of the property
            piDest.SetValue(dest, propertyVal);

        }
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        #endregion
    }
}