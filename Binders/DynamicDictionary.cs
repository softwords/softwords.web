﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;

namespace Softwords.Web.Models
{
    // The class derived from DynamicObject.
    public class DynamicDictionary : DynamicObject
    {
        // The inner dictionary.
        public readonly Dictionary<string, object> definedProps
            = new Dictionary<string, object>();

        // This property returns the number of elements
        // in the inner dictionary.
        public int Count
        {
            get
            {
                return definedProps.Count;
            }
        }

        /// <summary>
        /// allow the dynamic to return null for names that are not defined
        /// when Strict = false
        /// </summary>
        public bool Strict = true;
        // If you try to get a value of a property 
        // not defined in the class, this method is called.
        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.

            string name = (binder.IgnoreCase ? binder.Name.ToLower() : binder.Name);

            // If the property name is found in a dictionary,
            // set the result parameter to the property value and return true.
            // Otherwise, return false.
            bool found = definedProps.TryGetValue(name, out result);
            if (found)
            {
                return true;
            }
            result = null;
            return (!Strict);
        }

        protected object getProp(string propname)
        {
            object value;
            if (definedProps.TryGetValue(propname, out value))
			{
                return value;
            }
                
            return null;
        }

        // If you try to set a value of a property that is
        // not defined in the class, this method is called.
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            if (value != null && typeof(string).IsAssignableFrom(value.GetType()))
            {
                Guid g;
                if (Guid.TryParse(value.ToString(), out g))
                {
                   value = g;
                }
            }
            string name = (binder.IgnoreCase ? binder.Name.ToLower() : binder.Name);
            definedProps[name] = value;

            // You can always add a value to a dictionary,
            // so this method always returns true.
            return true;
        }
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return definedProps.Keys;
        }
    }

}