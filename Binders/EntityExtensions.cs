﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Softwords.Web.Models
{
	public static class EntityExtensions
	{
		public static T Remove<T>(this T entity, DbContext context) where T : class
		{
			// Find the StoredProcedureAttribute
			var type = typeof(T);
			var deleteprocAttribute = (DeleteProcAttribute)Attribute.GetCustomAttribute(type, typeof(DeleteProcAttribute));
			if (deleteprocAttribute == null)
			{
				DbSet<T> dbset = context.Set<T>();
				System.Data.Entity.Infrastructure.DbEntityEntry entry = context.Entry(entity);
				dbset.Remove(entity);
				return entity;
			}

			var deleteProcName = deleteprocAttribute.Name;

			// Find the Key property
			var keyProperty = type.GetProperties().FirstOrDefault(prop => Attribute.IsDefined(prop, typeof(KeyAttribute)));
			if (keyProperty == null)
			{
				throw new InvalidOperationException("Key property not found on entity.");
			}

			var keyValue = keyProperty.GetValue(entity);
			if (keyValue == null)
			{
				throw new InvalidOperationException("Key property value is null.");
			}

			// Execute the stored procedure
			context.Database.ExecuteSqlCommand($"EXEC {deleteProcName} @p0", keyValue);
			return entity;
		}
	}
}
