﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using System.ComponentModel.DataAnnotations;

namespace Softwords.Web.Models
{
    public interface IModelBinder<T>
    {
        Dictionary<string, object> definedProps { get; }
        bool IsSet(string propname);

        void FromJSON(Newtonsoft.Json.Linq.JObject jobj);

        void ToDb(object context, ClaimsIdentity identity, T dbEntity);
        void FromDb(T dbEntity);

        string RowVersion { get; }            // use a string becuase it will move as a Convert.ToBase64String

    }
   
    public class ModelBinder<T> : IModelBinder<T>
    {
        public Dictionary<string, object> definedProps { get; private set; }
        protected string rowVersionProp { get; set; }
        protected string keyProp { get; set; }
        protected ModelBinder()
        {
            definedProps = new Dictionary<string, object>();
            rowVersionProp = getRowVersionProp();
            keyProp = getKeyProp();
        }
        public bool IsSet(string propname)
        {
            return definedProps.ContainsKey(propname);
        }

        public string RowVersion
        {
            get
            {
                if (definedProps.ContainsKey(rowVersionProp))
                    return (string)definedProps[rowVersionProp];
                return null;
            }
        }
        /// <summary>
        /// alis the key field to ID, whatever the field name is
        /// this is determined by looking for the [Key] attribute
        /// Note we are assume it is a SINGLE field
        /// </summary>
        public virtual object ID
        {
            get
            {
                return getProp(keyProp);
            }
            set
            { 
                definedProps.Add(keyProp, value);
            }
        }

        private object set(string propname, JToken value)
        {
            if (propname == rowVersionProp)
            {
                definedProps.Add(propname, value.Value<string>());
            }
            else
            {
                Type dbType = this.GetType().BaseType.GetGenericArguments()[0];
                // System.Reflection.PropertyInfo pi = this.GetType().GetProperty(propname);
                System.Reflection.PropertyInfo pi = dbType.GetProperty(propname);
                if (pi != null)
                {
                    object newValue = value.Value<string>();
                    if ((string)newValue == String.Empty)
                        newValue = null;
                    if (newValue != null)
                    {
                        if (pi.PropertyType == typeof(string))
                        {
                            newValue = value.Value<string>();
                        }
                        if (pi.PropertyType == typeof(DateTime))
                            newValue = value.Value<DateTime>();

                        if (pi.PropertyType == typeof(bool))
                            newValue = value.Value<bool>();


                        if (pi.PropertyType == typeof(int))
                            // even though the database property is int, we may need to load null from a new record
                            // so the type is int?
                            // typically this field will be dbGenerated and so the null will not get moved into the db record
                            newValue = value.Value<int?>();
                        else if (pi.PropertyType == typeof(float))
                            newValue = value.Value<float>();
                        else if (pi.PropertyType == typeof(short))
                            newValue = value.Value<short>();
                        else if (pi.PropertyType == typeof(long))
                            newValue = value.Value<long>();
                        if (pi.PropertyType == typeof(decimal))
                            newValue = value.Value<decimal>();
                        else if (pi.PropertyType == typeof(Byte[]))
                            newValue = value.Value<string>();
                        else if (pi.PropertyType == typeof(System.Guid))
                            newValue = new System.Guid(value.Value<string>());

                        // nullables
                        if (pi.PropertyType == typeof(DateTime?))
                            newValue = value.Value<DateTime?>();

                        if (pi.PropertyType == typeof(bool?))
                            newValue = value.Value<bool?>();

                        if (pi.PropertyType == typeof(int?))
                            newValue = value.Value<int?>();
                        if (pi.PropertyType == typeof(float?))
                            newValue = value.Value<float?>();
                        if (pi.PropertyType == typeof(short?))
                            newValue = value.Value<short?>();
                        if (pi.PropertyType == typeof(long?))
                            newValue = value.Value<long?>();
                        if (pi.PropertyType == typeof(decimal?))
                            newValue = value.Value<decimal?>();
                    }

                    // pi.SetValue(this, newValue);
                    definedProps.Add(propname, newValue);
                }
            }
            return value;
        }


        public void FromJSON(Newtonsoft.Json.Linq.JObject jobj)
        {
            foreach (var prop in jobj)
            {
                // movement of obects up the wire creates
                // a conflict between javascript naming standards (properties in camelCase)
                // and C# conventions (properties in PascalCase)
                // to ameliorate this somewhat we can translate here
                set(prop.Key, prop.Value);
            }
        }

        public virtual void ToDb(object context, ClaimsIdentity identity, T s) { }

        public void FromDb(T s)
        {
            string[] keys = new string[definedProps.Keys.Count];
            definedProps.Keys.CopyTo(keys, 0);
            foreach (var propname in keys)
            {
                System.Reflection.PropertyInfo piDest = s.GetType().GetProperty(propname);
                definedProps[propname] = piDest.GetValue(s);
            }
        }

        protected object getProp(string propname)
        {
            object value;
            if (definedProps.TryGetValue(propname, out value))
                return value;
            return null;
        }
        // will return the name of any property marked with the Timestamp attribute in the object definition
        private string getRowVersionProp()
        {
            if (this.GetType().BaseType.GenericTypeArguments.Length == 0)
            {
                return String.Empty;
            }
            Type t = this.GetType().BaseType.GenericTypeArguments[0];
            foreach (var prop in t.GetProperties())
            {
                var attrs = prop.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    if (attr is System.ComponentModel.DataAnnotations.TimestampAttribute)
                    {
                        return prop.Name;
                    }
                }
            }
            return string.Empty;
        }
        // will return the name of any property marked with the Key attribute in the object definition
        private string getKeyProp()
        {
            if (this.GetType().BaseType.GenericTypeArguments.Length == 0)
            {
                return String.Empty;
            }
            Type t = this.GetType().BaseType.GenericTypeArguments[0];
            
            var key = t.GetProperties().FirstOrDefault(p =>
                    p.CustomAttributes.Any(attr => attr.AttributeType == typeof(KeyAttribute)));
            if (key != null)
                return key.Name;

            return string.Empty;
        }
    }


    public class ModelBinderConverter<T> : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsSubclassOf(typeof(ModelBinder<T>));
        }

        private ModelBinder<T> Create(Type objectType)
        {
            return (ModelBinder<T>)Activator.CreateInstance(objectType);
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jobj = JObject.Load(reader);
            var t = Create(objectType);
            t.FromJSON(jobj);
            return t;
        }
    }
}
