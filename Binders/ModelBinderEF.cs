﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Claims;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Softwords.Web.Models.Entity
{
    public class ModelBinder<T> : Softwords.Web.Models.ModelBinder<T> where T: class
    {
        private ChangeTrackingAttribute getChangeTrackAttribute(System.Reflection.PropertyInfo piDest)
        {
            foreach (var attr in piDest.GetCustomAttributes(true))
            {
                // return true if its a timestamp
                if (attr is ChangeTrackingAttribute)
                {
                    return attr as ChangeTrackingAttribute;
                }

            }
            return null;
        }
 

        private bool isDbGenerated(System.Reflection.PropertyInfo piDest)
        {
            foreach (var attr in piDest.GetCustomAttributes(true))
            {
                // return true if its a timestamp
                if (attr is System.ComponentModel.DataAnnotations.TimestampAttribute)
                {
                    return true;
                }
                if (attr is System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute)
                {
                    var dg = attr as System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute;
                    if (dg.DatabaseGeneratedOption != System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)
                    {
                        return true;
                    }
                    return false;
                }
            }
            // no DatabaseGenerated attribute
            return false;
        }
        public override void ToDb(object context, ClaimsIdentity identity, T s)
        {
            var cxt = context as System.Data.Entity.DbContext;
            DateTime now = DateTime.UtcNow;     // be sure not to get weird variation between create and edit
            foreach (System.Reflection.PropertyInfo piDest in s.GetType().GetProperties())
            {
                string propname = piDest.Name;
                if (!isDbGenerated(piDest))
                {
                    ChangeTrackingAttribute ctattr = getChangeTrackAttribute(piDest);
                    if (ctattr == null)
                    {
                        if (definedProps.ContainsKey(propname))
                            SetValue(piDest, s, definedProps[propname]);
                    }
                    else
                    {
                        switch (ctattr.ChangeTrackingOption)
                        {
                            case ChangeTrackingOption.CreateUser:
                                // if a new row, use the current user
                                if (piDest.GetValue(s) == null)
                                {
                                    piDest.SetValue(s, identity.Name);
                                }
                                break;
                            case ChangeTrackingOption.CreateDateTime:
                                if (piDest.GetValue(s) == null)
                                {
                                    piDest.SetValue(s, now);
                                }
                                break;
                            case ChangeTrackingOption.EditUser:
                                piDest.SetValue(s, identity.Name);
                                break;
                            case ChangeTrackingOption.EditDateTime:
                                piDest.SetValue(s, now);
                                break;
                            case ChangeTrackingOption.CreateTag:
                                // createTag is a guid field that can be a "fake" primary key on an editable view
                                // most ORMS want to use scope_identity() to retrieve the newly added record
                                // this will fail when the record is added on an insert trigger on a view
                                // by saying that CreateTag is the primary key, and assigning it outside the database
                                // EF can find our record again

                                // don't overwrite the createtag if its already on the record
                                if (piDest.GetValue(s) == null)
                                {
                                    // use a passed in value if the client really wants to know the guid ahead of time....
                                    // used to be important in MS Access
                                    if (definedProps.ContainsKey(propname))
                                        piDest.SetValue(s, definedProps[propname]);
                                    else
                                        piDest.SetValue(s, Guid.NewGuid());
                                };
                                break;
                        }
                        // any change-track field should get sent back in the response
                        // even when it was not sent by the client (or may not currently BE on the client)
                        if (!definedProps.ContainsKey(propname))
                        {
                            definedProps.Add(propname, piDest.GetValue(s));
                        }
                    }

                }

            }

            try
            {
                cxt.SaveChanges();
                FromDb(s);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                // return the record we just read
                // pass back only the properties that were passed in
                FromDb(s);
                throw ConcurrencyException.Make(this.rowVersionProp,this.definedProps, ex);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                // implies some failure on update, such as trying to write a null into a non-null
                // field. These shouldendeavour to be caught client-side, but if they sneak through
                // we should report as a 400 error, not a plain old 500 
                string theError = ex.Message;
                Exception baseEx = ex.GetBaseException();
                if (baseEx is System.Data.SqlClient.SqlException)
                {
                    System.Data.SqlClient.SqlException sqlEx = baseEx as System.Data.SqlClient.SqlException;
                    theError = baseEx.Message;
                    if (sqlEx.Number == 515)
                    {
                        // these messages may look like
                        //Cannot insert the value NULL into column 'istsDurationUnit', table 'scholar.dbo.ISTSession_'; 
                        // column does not allow nulls.INSERT fails. The statement has been terminated.
                        string field = "";
                        string[] a = theError.Split('\'');
                        for (int i = 0; i < a.Length - 1; i++)
                        {
                            if (a[i].Contains("into column"))
                            {
                                // the next next one is the field name
                                field = a[i + 1];
                                break;
                            }
                        }
                        
                        throw new ValidationException(field, theError, sqlEx);
                    }
                }
                throw baseEx;

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string theError = ex.Errors[0].Message;
                if (theError.StartsWith("<ValidationError"))
                {
                    System.Xml.Linq.XDocument x = System.Xml.Linq.XDocument.Parse(theError);
                    string field = x.Root.Attribute("field").Value;
                    string message = x.Root.Nodes().First().ToString();
                    throw new ValidationException(field, message, ex);
                }
                throw ex;
            }
            // this is a validation thrown by Entity Fraework, from the attributes
            // of the model object. ie this error is thrown without attempting to save to the database
            // the messages returned are determined by any messages associated to the attributes
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                throw new ValidationException(ex);
            }
            catch (Exception ex)
            {
                // return as 500 error
                throw ex;
            }
        }


        #region nullable support
        //http://technico.qnownow.com/how-to-set-property-value-using-reflection-in-c/
        //https://stackoverflow.com/questions/13270183/type-conversion-issue-when-setting-property-through-reflection
        private void SetValue(System.Reflection.PropertyInfo piDest, object dest, object propertyVal)
        {
            //find the property type
            Type propertyType = piDest.PropertyType;

            //Convert.ChangeType does not handle conversion to nullable types
            //if the property type is nullable - AND the value is NOT null - we need to get the underlying type of the property
            if (propertyVal != null)
            {
                var targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;

                if (targetType == typeof(Guid))
                {
                    propertyVal = Guid.Parse(propertyVal.ToString());
                }
                else if (targetType == typeof(DateTimeOffset))
                {
                    if (propertyVal.GetType() == typeof(DateTime))
                    {
                        DateTime pv = (DateTime)propertyVal;
                        propertyVal = new DateTimeOffset(pv);
                    }
                    //propertyVal = Convert.ChangeType(propertyVal, targetType);
                }
                else if (targetType == typeof(String) && (String)propertyVal == String.Empty)
                {
                    propertyVal = null;
                }
                else
                {
                    //Returns an System.Object with the specified System.Type and whose value is
                    //equivalent to the specified object. doesn;t work with Guid
                    propertyVal = Convert.ChangeType(propertyVal, targetType);
                }

            }
            //Set the value of the property
            piDest.SetValue(dest, propertyVal);

        }
        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        #endregion
    }

}
