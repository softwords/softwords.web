﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Claims;


namespace Softwords.Web.Models.Linq
{

    public class ModelBinder<T>: Softwords.Web.Models.ModelBinder<T>
    {
        public ModelBinder():base()
        {
             rowVersionProp = "pRowversion";
        }

        public override void ToDb(object context, ClaimsIdentity identity, T s)
        {
            var cxt = context as System.Data.Linq.DataContext;
            System.Data.Linq.Mapping.ColumnAttribute attr = null;
            DateTime now = DateTime.UtcNow;     // be sure not to get weird variation between create and edit

            foreach (System.Reflection.PropertyInfo piDest in s.GetType().GetProperties())
            {
                string propname = piDest.Name;
                attr = piDest.GetCustomAttributes(true)[0] as System.Data.Linq.Mapping.ColumnAttribute;
                if (attr != null && !attr.IsDbGenerated) // is attr == null it's not a column
                {
                    switch (propname)
                    {
                        case "pRowversion":
                            // server will handle it
                            break;
                        case "pCreateUser":
                            // fill it in if it is not defined
                            if (piDest.GetValue(s) == null)
                            {
                                piDest.SetValue(s, identity.Name);
                            }
                            break;
                        case "pCreateDateTime":
                            if (piDest.GetValue(s) == null)
                            {
                                piDest.SetValue(s, now);
                            };
                            break;
                        case "pEditDateTime":
                            piDest.SetValue(s, now);
                            break;
                        case "pEditUser":
                            piDest.SetValue(s, identity.Name);
                            break;
                        case "pCreateTag":
                            if (definedProps.ContainsKey(propname) && definedProps[propname] != null)
                                piDest.SetValue(s, definedProps[propname]);
                            else
                                piDest.SetValue(s, System.Guid.NewGuid());
                            break;
                        default:
                            if (definedProps.ContainsKey(propname))
                                piDest.SetValue(s, definedProps[propname]);
                            break;
                    }
                }
            }
            try
            {
                cxt.SubmitChanges();
                FromDb(s);
            }
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                // return the record we just read
                // pass back only the properties that were passed in
                FromDb(s);
                throw ConcurrencyException.Make(this.rowVersionProp,this.definedProps, ex);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string theError = ex.Errors[0].Message;
                if (theError.StartsWith("<ValidationError"))
                {
                    System.Xml.Linq.XDocument x = System.Xml.Linq.XDocument.Parse(theError);
                    string field = x.Root.Attribute("field").Value;
                    string message = x.Root.Nodes().First().ToString();
                    throw new ValidationException(field, message, ex);
                }
                throw ex;
            }
            catch (Exception ex)
            {
                // return the record we just read

                throw ex;
            }
        }
    }
}
