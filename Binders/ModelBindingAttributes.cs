﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web.Models
{
    public enum ChangeTrackingOption
    {
        CreateUser = 0,
        CreateDateTime = 1,
        EditUser = 2,
        EditDateTime = 3,
        CreateTag = 4
    }

    [AttributeUsage(AttributeTargets.Property| AttributeTargets.Field)]
    public class ChangeTrackingAttribute: Attribute
    {
        public ChangeTrackingAttribute(ChangeTrackingOption changeTrackingOption)
        {
            ChangeTrackingOption = changeTrackingOption;
        }

        public ChangeTrackingOption ChangeTrackingOption { get; }
    }

    /// <summary>
    /// Adds a reference to a client-side lookup list, as supplied by the Lookups service
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ClientLookupAttribute : Attribute
    {
        public ClientLookupAttribute(string clientLookup)
        {
            Lookup = clientLookup;
        }

        /// <summary>
        /// The name of the lookup to use, as defined in the client-side Lookups service
        /// </summary>
        public string Lookup { get; }
    }

    public enum TypedPhoneType
    {
        Fixed,
        Cell,
        Satellite,
        Skype, 
        Any
    }
    /// <summary>
    /// Extends the Phone attribute, by allowing type. Use to construct an icon for the input
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class PhoneTypeAttribute : Attribute
    {
        public PhoneTypeAttribute(TypedPhoneType phoneType)
        {
            PhoneType = phoneType;
        }

        /// <summary>
        /// The type of phone connection
        /// </summary>
        public TypedPhoneType PhoneType { get; }
    }

	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public class DeleteProcAttribute : Attribute
	{
		public string Name { get; }

		public DeleteProcAttribute(string name)
		{
			Name = name;
		}
	}
}
