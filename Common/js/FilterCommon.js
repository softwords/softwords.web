﻿// holdes some standard stuff applicable to any filter model

(function () {
    var basefilter = function ($rootScope, $state) {

        var base = {
            FindNow: findNow,
            Table: table,
            Geo: geo,
            Search: search,
            ShowPage: showPage,
            SortOn: sortOn,

            Reset: reset,
            IsLocked: isLocked,

            GetViewMode: getViewMode,
            SetViewMode: setViewMode,
            SetUserSettings: setUserSettings,

            Action: action,

            FixParams: fixparams
        };
        return base;

        //-------------------------------------------------------------
        // implementation
        //--------------------------------------------------------------
        // this is called when a searcher indicates that FindNow is clicked
        // other listeners may respond by asking the Filter to Search or get a Table
        function findNow() {
            $rootScope.$broadcast('FindNow', { entity: self.entity, filter: this });
            if (this.needSearch) {
                this.needSearch = false;
                this.Search();
            }
            if (this.needTable) {
                this.needTable = false;
                this.Table();
            }
            if (this.needGeo) {
                this.needGeo = false;
                this.Geo();
            }

        }

        function search(api) {
            var self = this;
            // if a view mode is set, its columnSet should get pushed into the params
            // user defaults
            if (this.SelectedVM && this.SelectedVM.columnSet) {
                this.params.ColumnSet = this.SelectedVM.columnSet;
            }
            else {
                this.params.ColumnSet = this.ViewModes.defaults.columnSet;
            }
            
            api.filterPaged(fixparams(self,self.params)).then(
                // return pack is an object with
                // data, headers, config, status and statustext
                 function (returnPack) {


                     self.summary = {
                         numResults: returnPack.NumResults
                         , firstRec: returnPack.FirstRec
                         , lastRec: returnPack.LastRec
                         , pageNo: returnPack.PageNo
                         , pageSize: returnPack.PageSize
                     };
                     $rootScope.$broadcast('SearchComplete', { entity: self.entity, resultset: returnPack.ResultSet, summary: self.summary });
                     self.prepared = true;

                 },
                function () {
                    alert("Sorry, there was a problem!");
                }
            );
        };

        function table(api) {
            var self = this;

            // appDS.filterPaged(self.theFilter).then(
            api.table({ row: self.tableparams.row, col: self.tableparams.col, filter: self.params }).then(
                // return pack is an object with
                // data, headers, config, status and statustext
                 function (returnPack) {

                     self.summary = {
                         numResults: returnPack.NumResults
                         , firstRec: returnPack.FirstRec
                         , lastRec: returnPack.LastRec
                         , pageNo: returnPack.PageNo
                         , pageSize: returnPack.PageSize
                     };
                     $rootScope.$broadcast('SearchComplete', { entity: self.entity + '.table', resultset: returnPack.ResultSet, summary: self.summary });
                     //$scope.$apply();
                     self.prepared = true;
                 },
                function (error) {

                    alert("Sorry, there was a problem!");
                }
            );

        };
        function geo(api) {
            var self = this;

            // appDS.filterPaged(self.theFilter).then(
            api.geo(self.params).then(
                // return pack is an object with
                // data, headers, config, status and statustext
                 function (returnPack) {

                     self.summary = {
                         numResults: returnPack.NumResults
                         , firstRec: returnPack.FirstRec
                         , lastRec: returnPack.LastRec
                         , pageNo: returnPack.PageNo
                         , pageSize: returnPack.PageSize
                     };
                     $rootScope.$broadcast('SearchComplete', {
                         entity: self.entity + '.geo',
                         resultset: returnPack.ResultSet,
                         summary: self.summary,
                         renderOptions: { map: null }
                     });
                     //$scope.$apply();
                     self.prepared = true;
                 },
                function (error) {

                    alert("Sorry, there was a problem!");
                }
            );

        }

        function showPage(newPage, newPageSize) {
            this.params.PageNo = (newPage || 1);
            this.params.PageSize = (newPageSize || this.params.PageSize);
            this.FindNow();
        };
        function sortOn(sortField, sortDirection) {
            this.params.SortColumn = (sortField || 'appID');
            this.params.SortDir = (sortDirection || 'asc');
            this.ShowPage(1);

        }
        function getViewMode() {
            if (this.SelectedVM) {
                return this.SelectedVM.key;
            }
            return '';
        };
        //select a viewMode by key (ie name)
        function setViewMode(newValue) {

            if (this.SelectedVM && this.SelectedVM.key == newValue)
                return;


            this.SelectedVM = _.find(this.ViewModes.modes, function (viewMode) { return (viewMode.key == newValue); });
            if (this.SelectedVM) {
                this.params.ColumnSet = this.SelectedVM.columnSet;
                if (this.prepared) {
                    this.FindNow();
                }

            }
        };
        function setColumnSet(columnset) {
            this.params.ColumnSet = columnset;
        };

        function setUserSettings(settings) {
            this.UserSettings = settings;
            if (settings.allowedViews) {
                // settings .allowed views is an array of the supported view keys
                // filter the ViewModes collectio by membership of this array using lodash functions

                this.ViewModes.modes = _.filter(this.AllViewModes.modes, function (mode) {
                    return (_.indexOf(settings.allowedViews, mode.key) >= 0);;
                });
            }
            else {
                this.ViewModes.modes = _.filter(this.AllViewModes.modes, function (mode) {
                    return (mode.selectable != false);
                });
            };
            if (settings.defaultView) {
                this.SetViewMode(settings.DefaultView);
            };
        };
        
        function reset() {
            // set up the parameters
            this.params = fixparams(this, {});

            this.resultset = null;
            this.summary = null;
            this.FlashString = '';
        };

        function isLocked(paramname) {
            var u = this.userDefaults || { defaults: {}, locked: {} };
            return !(u.locked[paramname] == undefined && (this.ParamDefaults().locked == undefined || this.ParamDefaults.locked[paramname] == undefined));
        };

        function fixparams(fltr, params) {
            var newparams = {};
            var d = fltr.ParamDefaults();
            var u = fltr.UserSettings || {defaults: {}, locked: {}};
            angular.extend(newparams, d.defaults,u.defaults, params, d.locked, u.locked );
            return newparams;
          

        };


        // default action is to invoke the <action> state under the current state, passing the clicked column, the row data, and the id?

        function action(actionName, columnField, rowData, baseState) {
            
            
            var newstate = baseState + '.' + actionName;
            $state.go(newstate, { id: rowData[columnField], rowData: rowData, columnField: columnField });

            
        };
    };
    angular
    .module("sw.common")
    .factory("BaseFilter", ["$rootScope", "$state", basefilter]);

})();

(function () {
    var filtercontroller = function (filter) {
 
        var self = this;
        /* Chart options */


        this.theFilter = filter;

        this.Swap = function () {
            var col = self.theFilter.tableparams.col;
            self.theFilter.tableparams.col = self.theFilter.tableparams.row;
            self.theFilter.tableparams.row = col;
            self.theFilter.FindNow();
        }

        Object.defineProperty(self, 'row', {
            get: function () { return self.theFilter.tableparams.row; },
            set: function (newValue) {
                if (!(self.theFilter.tableparams.row == newValue)) {
                    self.theFilter.tableparams.row = newValue;
                    self.theFilter.FindNow();
                };

            }
        });
        Object.defineProperty(self, 'col', {
            get: function () { return self.theFilter.tableparams.col; },
            set: function (newValue) {
                if (!(self.theFilter.tableparams.col == newValue)) {
                    self.theFilter.tableparams.col = newValue;
                    self.theFilter.FindNow();
                };

            }
        });


    };
    angular
        .module('sw.common')
        // the injection name 'theFilter' comes from the resolve on the ui-router state from which this controller is invoked
        .controller('FilterController', ['theFilter', filtercontroller]);

})();


(function () {
/* 
--------------------------------------------------------------------
Map Params Controller

manages the centre, zoom level etc working with the renderOptions object
--------------------------------------------------------------------
*/
    var mapParamsController = function ($scope, $rootScope, filter) {
        var self = this;

        this.theFilter = filter;

        this.Ready = false;

        this.renderOptions = null;
        this.boxOptions = [];

        var _selectedBox;
        Object.defineProperty(this, 'selectedBox', {
            get: function () { return _selectedBox; },
            set: function (newValue) {
                if (!(_selectedBox  == newValue)) {
                    _selectedBox = newValue;
                    $rootScope.$broadcast("MapChangeBox", { entity: self.theFilter.entity, box: _selectedBox });
                };
                // this.awardTypeTots = this.grpAwardType.all();
            }
        });

        var _cluster = true;
        Object.defineProperty(this, 'cluster', {
            get: function () { return _cluster; },
            set: function (newValue) {
                if (!(_cluster == newValue)) {
                    _cluster = newValue;
                    $rootScope.$broadcast("MapChangeCluster", { entity: self.theFilter.entity, cluster: _cluster });
                };
                // this.awardTypeTots = this.grpAwardType.all();
            }
        });

        $scope.$on('SearchComplete', function (event, resultpack) {
            if (resultpack.entity = self.theFilter.entity + '.geo') {
                // resultset is an array of arrays - it was serialised from a Tables collection
                self.resultset = resultpack.resultset;

                // the first recordset is the data - we don;t need that?
                //self.pointData = resultpack.resultset[0];
                // we grab the marker bounding box
                var markerBoundingBox = resultpack.resultset[1][0];
                markerBoundingBox.couName = "(all markers)";


                // the final array is the bounding boxes of each country
                var o = [];
                o.push(markerBoundingBox);
                o = o.concat(resultpack.resultset[2]);
                self.boxOptions = o;
                // if the currently selectedbounding box is still valid, preserve it
                var i = -1;
                if (self.selectedBox) {
                    var i = _.findIndex(self.boxOptions,{couName: self.selectedBox.couName});
                    
                }
                if (i == -1)
                    i = 0;

                self.selectedBox = self.boxOptions[i];
                
            }

        });
        // using ng-map, this is how we get the goolge map object back to the controller
        $scope.$on('mapInitialized', function (event, map) {

            self.redraw();

        });

        this.makeLatLngBounds = function (nsew) {
            return this.makeLatLngBoundsNSEW(nsew.n, nsew.s, nsew.e, nsew.w);
        };

        this.makeLatLngBoundsNSEW = function (n, s, e, w) {
            var bnds = new google.maps.LatLngBounds();
            var SW = new google.maps.LatLng(s, w);
            var NE = new google.maps.LatLng(n, e);
            bnds.extend(SW);
            bnds.extend(NE);

            return bnds;
        };

        function applyBoundsCountry(couName) {
            var geocoder = new google.maps.Geocoder();
            var cName = zoomSelector.value;

            geocoder.geocode({ 'address': couName }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    map.fitBounds(results[0].geometry.viewport);
                }
            });
        }

    };
    angular
         .module('sw.common')
         // the injection name 'theFilter' comes from the resolve on the ui-router state from which this controller is invoked
         .controller('MapParamsController', ['$scope', '$rootScope', 'theFilter', mapParamsController]);


})();

