﻿(function () {
    /* 
      --------------------------------------------------------------------
      Paged List Controller
    
      manages the display of a list of applications
      --------------------------------------------------------------------
      */

    var pagedListController = function ($scope,  $state, $stateParams, filter) {
        var self = this;

        this.theFilter = filter;

        this.resultset;

        $scope.extern = $scope;
        this.isWaiting = false;

        //if ($stateParams.supportedViewModes) {
        //    this.SupportedViewModes = $stateParams.supportedViewModes;
        //}
        //else {
        //    this.SupportedViewModes = _.pluck(filter.ViewModes.modes, 'key');
        //}
        //this.defaultColumnSet = $stateParams.defaultColumnSet || filter.ViewModes.defaults.columnSet || 1;


        self.renderedViewMode = '';      // that's how its been initilaised        
        self.renderedPageNo = 1;
        self.renderedPageSize = 50;

        self.baseState = $state.current.name;

        self.action = function (actionName, columnField, rowData) {
            self.theFilter.Action(actionName, columnField, rowData, self.baseState);
        }
      

        this.gridOptions = {
            data: self.resultset,

            paginationPageSizes: [10, 25, 50, 100, 500],
            paginationPageSize: 25,
            useExternalPagination: true,
            useExternalSorting: true,
            onRegisterApi: function (gridApi) {
                self.gridApi = gridApi;
                self.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                    if (sortColumns.length == 0) {

                        self.theFilter.SortOn();
                    } else {
                        self.theFilter.SortOn(sortColumns[0].field, sortColumns[0].sort.direction);
                    }
                });
                self.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    if (+newPage != +self.renderedPageNo || +pageSize != +self.renderedPageSize) {
                        self.theFilter.ShowPage(newPage, pageSize);
                    }
                });
            }



        };


        $scope.$on('SearchComplete', function (event, resultpack) {
            
            if (resultpack.entity == self.theFilter.entity) {

                self.resultset = resultpack.resultset;
                if (self.theFilter.ViewMode != self.renderedViewMode) {
                    var cd = new Array();
                    if (self.theFilter.ViewModes.defaults.columnDefs)
                    {
                        // append these columns to the set
                        cd = cd.concat(self.theFilter.ViewModes.defaults.columnDefs);
                    }
                    cd = cd.concat(self.theFilter.SelectedVM.gridOptions.columnDefs);
                    self.gridOptions.columnDefs = cd;
                    self.renderedViewMode = self.theFilter.ViewMode;
                }


                self.gridOptions.data = self.resultset;
                self.gridOptions.totalItems = resultpack.summary.numResults;
                if (resultpack.summary.numResults > 0) {
                    if (self.gridOptions.paginationCurrentPage != resultpack.summary.pageNo) {
                        self.renderedPageNo = resultpack.summary.pageNo
                        self.gridOptions.paginationCurrentPage = resultpack.summary.pageNo;
                    }
                    if (self.gridOptions.paginationPageSize != resultpack.summary.pageSize) {
                        self.renderedPageSize = resultpack.summary.pageSize;
                        self.gridOptions.paginationPageSize = resultpack.summary.pageSize;
                    }

                };
            }
            self.isWaiting = false;
        });

        $scope.$on('FindNow', function (event, data) {
            if (data.filter.entity == self.theFilter.entity) {
                //data.filter.SetColumnSet(self.selectedVM.columnSet == 'undefined' ?self.defaultColumnSet: self.selectedVM.columnSet);
                data.filter.requestSearch();
                self.isWaiting = true;
            }

        });

        // finally, if this Filter has already been searched, tell it we want to search again
        if (this.theFilter.prepared) {
            this.theFilter.FindNow();
        }

    };
    var viewModeController = function ($scope, $stateParams, filter) {
        var self = this;

        this.theFilter = filter;

        if ($stateParams.supportedViewModes) {
            this.SupportedViewModes = $stateParams.supportedViewModes;
        }
        else {
            this.SupportedViewModes = _.pluck(filter.ViewModes.modes, 'key');
        }
       // this.defaultColumnSet = $stateParams.defaultColumnSet || filter.ViewModes.defaults.columnSet || 1;

        // set up the initial state
        self.theFilter.ViewMode = this.SupportedViewModes[0];

    };
    angular
        .module('sw.common')
        // theFilter will be injected by ui-router resolve 
        .controller('PagedListController', ['$scope', '$state', '$stateParams', 'theFilter', pagedListController])
        .controller('ViewModeController', ['$scope', '$stateParams', 'theFilter', viewModeController]);
})();