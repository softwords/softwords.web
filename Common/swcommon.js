﻿/*
    swjs namespace
*/
var swjs = swjs || {};

// Route Helper
(function () {
  
        function listState() {
            var state = {
                // all these are the same becuase the variation is in the Filter object
          
                url: '/list',
                data: {
                    roles: ['scholarUser']
                },
                views: {
                    "navbar@": {
                        templateUrl: "common/PagedListParams",
                        controller: "ViewModeController",
                        controllerAs: "listvm"
                    },
                    "@": {
                        templateUrl: "common/PagedList",       // begin move to server-side mvc routes
                        controller: 'PagedListController',
                        controllerAs: "listvm"
                    }
                }
            };
            return state;
        };
        function chartState() { 
            var state =  {
                url: '/chart',

                views: {
                    "navbar@": {
                        templateUrl: "common/chartParams/school",
                        controller: "FilterController",
                        controllerAs: "chtc"
                    },
                    "@": {
                        templateUrl: "common/chart",
                        controller: "ChartController",
                        controllerAs: "chtc"
                    }
                }
            };
            return state;
        };
        function tableState() {
            var state =  {
                url: '/table',

                views: {
                    "navbar@": {
                        templateUrl: "common/chartParams/school",
                        controller: "FilterController",
                        controllerAs: "chtc"
                    },
                    "@": {
                        templateUrl: "common/table",
                        controller: "ChartController",
                        controllerAs: "chtc"
                    }

                }
            };
            return state;
        };
        function mapState() {
            var state = {
                url: '/map',
                views: {
                    "navbar@": {
                        templateUrl: "common/MapParams",
                        controller: "MapParamsController",
                        controllerAs: "mappc"
                    },
                    "@": {
                        templateUrl: "common/map",
                        controller: "MapController",
                        controllerAs: "mapc"
                    }
                }
            };
            return state;
        };

    // featurename matches the Web Api controller
    // filtername is the filtername js class
    // templatepath matches the mvc controller that dispenses the templates

        function featureState(featurename, filtername, templatepath, usersettings) {
            var defUserSettings = { defaults: {}, locked: {} };
            usersettings = usersettings || defUserSettings;
            filtername = filtername || featurename + 'Filter';
            featurename = featurename.toLowerCase();
            templatepath = templatepath || featurename;
            var state = {
                url: '/' + featurename,
                data: {
                    roles: ['authenticated']
                },
                views: {
                    "headerbar@": {
                        templateUrl: function () { return templatepath + "/searcher"; },            // mvc formulation allows server-side logic to choose searher version, function encapsulation allows future client override too
                        controller: "FilterController",
                        controllerAs: "vm",

                    },
                    "headerbardropdownIcon@": { template: '<i class="fa fa-filter"></i>'},
                    "flashFind@": {
                        templateUrl: "views/templates/common/flashfindsearch.html",
                        controller: "FilterController",
                        controllerAs: "vm"
                    },
                    "flashFindCollapsed@": {template: '<i class="fa fa-bolt opacity-control"></i>'}
                },
                resolve: {
                    theFilter: filtername,
                    UserSettings: [filtername, function (fltr) {
                        fltr.SetUserSettings(usersettings);

                    }]
                }
            };
            return state;
        };

        function addFeatureState($stateProvider, featurename, filtername, templatepath, usersettings) {
            var state = featureState(featurename, filtername, templatepath, usersettings);
            var statename = 'site.' + featurename.toLowerCase();
            $stateProvider
                .state(statename, state);
            return this;
        }

        function addlistState($stateProvider, featurename) {
            var state = listState();
            var statename = 'site.' + featurename.toLowerCase() + '.list';
            $stateProvider
                .state(statename, state);
            return this;
        }

        function addchartState($stateProvider, featurename) {
            var state = chartState();
            var statename = 'site.' + featurename.toLowerCase() + '.chart';
            $stateProvider
                .state(statename, state);
            return this;
        }

        function addtableState($stateProvider, featurename) {
            var state = tableState();
            var statename = 'site.' + featurename.toLowerCase() + '.table';
            $stateProvider
                .state(statename, state);
            return this;
        }

        function addmapState($stateProvider, featurename) {
            var state = mapState();
            var statename = 'site.' + featurename.toLowerCase() + '.map';
            $stateProvider
                .state(statename, state);
            return this;
        }


        swjs.routeHelper = {
            listState: listState,
            featureState: featureState,
            addFeatureState: addFeatureState,
            addListState: addlistState,
            addChartState: addchartState,
            addTableState: addtableState,
            addMapState: addmapState,

        };
})();

(function () {
    angular
        .module('sw.common', []);
})();