﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Xml;
using System.Xml.Linq;


// Custom content types

namespace Softwords.Web.Content
{
    // this came from here:
    //http://stackoverflow.com/questions/15366096/how-to-return-xml-data-from-a-web-api-method

    public class XmlContent : HttpContent
    {
        private readonly MemoryStream _Stream = new MemoryStream();

        

        public XmlContent(XmlDocument document)
        {
            document.Save(_Stream);
            _Stream.Position = 0;
            Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/xml");
        }

        // added some other constructors
        public XmlContent(string filename)
        {
            XmlDocument document = new XmlDocument();
            document.Load(filename);

            document.Save(_Stream);
            _Stream.Position = 0;
            Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/xml");
        }

        // support linq too
        public XmlContent(XDocument document)
        {

            document.Save(_Stream);
            _Stream.Position = 0;
            Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/xml");
        }

        protected override Task SerializeToStreamAsync(Stream stream, System.Net.TransportContext context)
        {

            _Stream.CopyTo(stream);

            var tcs = new TaskCompletionSource<object>();
            tcs.SetResult(null);
            return tcs.Task;
        }

        protected override bool TryComputeLength(out long length)
        {
            length = _Stream.Length;
            return true;
        }
    }
}
