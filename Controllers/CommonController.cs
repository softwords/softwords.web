﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Softwords.Web.Models;

namespace Softwords.Web.mvcControllers
{

    // derive from mvcControllerBase to get CurrentUser and ApplicationUserManager
    public class CommonController : mvcControllerBase
    {


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PagedList()
        {
            return View();
        }
        public ActionResult PagedListEditable()
        {
            return View();
        }
        public ActionResult PagedListParams()
        {
            return View();
        }
        public ActionResult Map()
        {
            return View();
        }
        public ActionResult MapParams()
        {
            return View();
        }
        public ActionResult MapPage()
        {
            return View();
        }
        public ActionResult Table()
        {
            return View();
        }
        public ActionResult TableParams()
        {
            return View();
        }
        public ActionResult TablePage()
        {
            return View();
        }
        public ActionResult Chart()
        {
            return View();
        }

        public ActionResult ChartParams()
        {
            return View();
        }
        public ActionResult ChartPage()
        {
            return View();
        }
        public ActionResult ChartGroupParam()
        {
          return View();
        }

        public ActionResult RowEditManager()
        {
            return View();
        }

        public ActionResult FlashFindSearch()
        {
            return View();
        }

        public ActionResult StateAudit()
        {
            return View();
        }

		public ActionResult RestTest()
		{
			return View();
		}

		public ActionResult ThemeAudit()
        {
            return View();
        }
        // chartparams requires access to project specific data - moved to 'Local'
        //public ActionResult ChartParams(ChartParamsModel model)
        //{

        //    return View(model);
        //}

        //public ActionResult CountrySelector()
        //{
        //    CountrySelectorModel model = new CountrySelectorModel();
        //    return View(model);
        //}
        public ActionResult colorRangeDesigner() { return View(); }
        public ActionResult colorBrewerPicker() { return View(); }

        public ActionResult versionInfo() { return View(); }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult versionPopup() { return View(); }

        public ActionResult versionButton() { return View(); }

        public ActionResult supergrid() { return View(); }
	}
}
