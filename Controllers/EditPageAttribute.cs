﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Softwords.Web.mvcControllers
{
    // This attribute can be applied to an action in a Controller
    // to use the EditPageLayout master page, and specify the permissions
    // required
    // layout will be sourced from mvc Shared view folder

    // http://stackoverflow.com/questions/5161380/how-do-i-specify-different-layouts-in-the-asp-net-mvc-3-razor-viewstart-file

    public class EditPageAttribute : ActionFilterAttribute
    {
        private readonly string masterName = "EditPageLayout";
        private string editPermission;
        public EditPageAttribute(string editPermission = null)
        {
            this.editPermission = editPermission;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.MasterName = masterName;
            }
            filterContext.Controller.ViewBag.EditPermission = editPermission;
        }
    }
}
