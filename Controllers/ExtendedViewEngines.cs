﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Softwords.Web
{
    // this customisation of the view engine is to sepecify an alternative search path for templates
    // see http://stackoverflow.com/questions/632964/can-i-specify-a-custom-location-to-search-for-views-in-asp-net-mvc
    // requires this Engine to be registered in global .asax
    // note the new root folder has to have the appropriate web.config to specify default code settings
    // using mvc under view to separate the mvc views from other 'view' material defined in the theme
    public class RazorViewEngineEx:RazorViewEngine
    {
        public RazorViewEngineEx(string instanceContext, string scope)
        {
            // page path - look in the instance context first

            var viewLocList = new List<string>();
            var masterLocList = new List<string>();
            if ((instanceContext ?? String.Empty) != String.Empty)
            {
                // allow mu;tiple instance contexts separated by comma
                // e.g. public, aaa will search aaa/public then public, then aaa

                if ((scope ?? String.Empty) != String.Empty)
                {
                    viewLocList.Add("~/Views/" + instanceContext + "/" + scope + "/{1}/{0}.cshtml");
                    viewLocList.Add("~/Views/" + instanceContext + "/" + scope + "/Shared/{0}.cshtml");
                    masterLocList.Add("~/Views/" + instanceContext + "/" + scope + "/Master/{0}.cshtml");
                }

                viewLocList.Add("~/Views/" + instanceContext + "/{1}/{0}.cshtml");
                viewLocList.Add("~/Views/" + instanceContext + "/Shared/{0}.cshtml");
                masterLocList.Add("~/Views/" + instanceContext + "/Master/{0}.cshtml");
            }
            if ((scope ?? String.Empty) != String.Empty)
            {
                viewLocList.Add("~/Views/" + scope + "/{1}/{0}.cshtml");
                viewLocList.Add("~/Views/" + scope + "/Shared/{0}.cshtml");
                masterLocList.Add("~/Views/" + scope + "/Master/{0}.cshtml");
            }

            viewLocList.Add("~/Views/default/{1}/{0}.cshtml");
            viewLocList.Add("~/Views/default/Shared/{0}.cshtml");
            masterLocList.Add("~/Views/default/Master/{0}.cshtml");

            var viewLocations = viewLocList.ToArray();
            var masterLocations = masterLocList.ToArray();

            this.PartialViewLocationFormats = viewLocations;
            this.ViewLocationFormats = viewLocations;
            this.MasterLocationFormats = masterLocations;
        }

        public RazorViewEngineEx(string instanceContext):this(instanceContext,String.Empty)
        {
        }
    }

    public class WebFormViewEngineEx : WebFormViewEngine
    {
        public WebFormViewEngineEx(string instanceContext, string scope)
        {
            // page path - look in the instance context first

            var viewLocList = new List<string>();
            var masterLocList = new List<string>();
            if ((instanceContext ?? String.Empty) != String.Empty)
            {
                // allow mu;tiple instance contexts separated by comma
                // e.g. public, aaa will search aaa/public then public, then aaa

                if ((scope ?? String.Empty) != String.Empty)
                {
                    viewLocList.Add("~/Views/" + instanceContext + "/" + scope + "/{1}/{0}.aspx");
                    viewLocList.Add("~/Views/" + instanceContext + "/" + scope + "/Shared/{0}.aspx");
                    masterLocList.Add("~/Views/" + instanceContext + "/" + scope + "/Master/{0}.aspx");
                }
                viewLocList.Add("~/Views/" + instanceContext + "/{1}/{0}.aspx");
                viewLocList.Add("~/Views/" + instanceContext + "/Shared/{0}.aspx");
                masterLocList.Add("~/Views/" + instanceContext + "/Master/{0}.aspx");
            }
            if ((scope ?? String.Empty) != String.Empty)
            {
                viewLocList.Add("~/Views/" + scope + "/{1}/{0}.aspx");
                viewLocList.Add("~/Views/" + scope + "/Shared/{0}.aspx");
                masterLocList.Add("~/Views/" + scope + "/Master/{0}.aspx");
            }

            viewLocList.Add("~/Views/default/{1}/{0}.aspx");
            viewLocList.Add("~/Views/default/Shared/{0}.aspx");
            masterLocList.Add("~/Views/default/Master/{0}.aspx");

            var viewLocations = viewLocList.ToArray();
            var masterLocations = masterLocList.ToArray();

            this.PartialViewLocationFormats = viewLocations;
            this.ViewLocationFormats = viewLocations;
            this.MasterLocationFormats = masterLocations;
        }

        public WebFormViewEngineEx(string instanceContext):this(instanceContext,String.Empty)
        {
        }
    }
}
