﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;


namespace Softwords.Web.mvcControllers
{
    public class HomeController : mvcControllerBase
    {
        public HomeController()
        {
            ViewBag.Title = AppSetting("title");
			// allow the web.config to override the meta tags
			ViewBag.Description = AppSetting("description");
			ViewBag.Author = AppSetting("author");
			ViewBag.Context = AppSetting("context");
		}

        /// <summary>
        /// the fragments local and remote define the links to styles and scripts
        /// These are either served from the local server (by rendering bundles)
        /// or from CDN (explcitly specifying the cdn path to the library)
        /// </summary>
        /// <param name="remote"></param>
        /// <returns></returns>
        [LayoutInjector("start")]
        public ActionResult Index(bool? remote=false)
        {
            if (remote != null)
            {
                return View((bool)remote ? "remote" : "local");
            }
            return View("local");
        }

        // GET teh nav_renderer
        // this overcomes problems with the path to the recursive file

        public ActionResult blank(bool? remote)
        {
            if (this.Request.Url.LocalPath.EndsWith("/"))
            {
				bool isRemote = GetRemote(remote);
                return View((bool)isRemote ? "remote" : "local", "start");
            }

            if (remote != null)
            { 
                return this.RedirectToAction("Index",new { remote = remote });
            }
            return this.RedirectToAction("Index");
        }

		private bool GetRemote(bool? remote)
		{
			try
			{
				// first check if the remote key is present
				if (Request.QueryString.HasKeys())
				{
					string[] r = Request.QueryString.GetValues("remote");
					if (r.Length > 0)
					{
						// the only reason we would explcitly put the value is to turn remote off
						// so check explcitly for false
						return (r[0] == "false" ? false : true);
					}
				}
				// how to identify a querystring key that has no value - it actually appears as a value that has no key!
				//https://stackoverflow.com/questions/3130541/access-query-string-parameters-with-no-values-in-asp-net
				// so ?remote is the same as remote=true


				if (Request.QueryString.GetValues(null) != null && Request.QueryString.GetValues(null).Contains("remote"))
				{
					return true;
				}

				// check for localhost - always local
				System.Net.IPAddress clientIp = GetClientIp();
				if (clientIp.ToString() == "::1" || clientIp.ToString() == "127.0.0.1")
				{
					// running on the server
					return false;
				}

				string ip = WebConfigurationManager.AppSettings["localIp"];

				if (!String.IsNullOrEmpty(ip))
				{
					// if the current request ip matches any of the patterns in 
					// ip, then we are local, otherwise remote
					foreach (string iprange in ip.Split(';'))
					{
						var range = NetTools.IPAddressRange.Parse(iprange.Trim());
						if (range.Contains(clientIp))
						{
							return false;
						}
					}
					// doesn;tmatch any of the local ips
					return true;
				}
				// you can also specify specifc remote Addresses - but local is always executed by preference
				// ie you can't have both
				ip = WebConfigurationManager.AppSettings["remoteIp"];
				if (!String.IsNullOrEmpty(ip))
				{
					// if the current request ip matches any of the patterns in 
					// ip, then we are local, otherwise remote
					foreach (string iprange in ip.Split(';'))
					{
						var range = NetTools.IPAddressRange.Parse(iprange.Trim());
						if (range.Contains(clientIp))
						{
							return true;
						}
					}
					// doesn;tmatch any of the exceptions
					return false;
				}
				return false;
			}
			catch
			{
				// if any error occurs , just go local
				return false;
			}
		}

		private System.Net.IPAddress GetClientIp()
		{
			string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
			if (string.IsNullOrEmpty(ipAddress))
			{
				ipAddress = Request.ServerVariables["REMOTE_ADDR"];
			}
			return System.Net.IPAddress.Parse(ipAddress);
		}

    }
}