﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Softwords.Web.mvcControllers
{
    // This attribute can be applied to an action in a Controller
    // to specify the layout to use
    // layout will be sourced from mvc Shared view folder

    // http://stackoverflow.com/questions/5161380/how-do-i-specify-different-layouts-in-the-asp-net-mvc-3-razor-viewstart-file

    public class LayoutInjectorAttribute : ActionFilterAttribute
    {
        private readonly string _masterName;
        public LayoutInjectorAttribute(string masterName)
        {
            _masterName = masterName;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.MasterName = _masterName;
            }
        }
    }
}
