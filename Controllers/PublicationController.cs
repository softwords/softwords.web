﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.IO;        // ! in a web app!

namespace Softwords.Web.mvcControllers
{
    /// <summary>
    ///  controller to handle the download of static documents
    /// </summary>
    public class PublicationController : mvcControllerBase
    {
        // GET: Publication
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MonthlySummary()
        {
            string name = "MovementSheetScholar*.*";

            return getLatest(name);
        }

        private ActionResult getLatest(string fileStub)
        {
            // iterate theough the Files folder, to find the most recent file beginning with this name
            // find the one with the most recent date; return the preifx and the date as the virtual file name
            //
            string latest = string.Empty;
            DateTime latestdt = DateTime.MinValue;
            string filesPath =  System.IO.Path.Combine(Request.PhysicalApplicationPath, "Files");
            foreach(string f in System.IO.Directory.GetFiles(filesPath,fileStub, SearchOption.TopDirectoryOnly))
            {
                DateTime dt = System.IO.File.GetLastWriteTimeUtc(f);
                if (dt > latestdt)
                {
                    latest = f;
                    latestdt = dt;
                }

              
            }
            if (latest != String.Empty)
                {
                    string n = System.IO.Path.GetFileName(latest);
                    string d = System.IO.Path.GetFileNameWithoutExtension(fileStub).Replace("*","").Replace("%","")
                                    +latestdt.ToString(" yyyy-MMM-dd") + Path.GetExtension(latest);
                    return getFile(n, d);
                }
           return View("NoFile");
        }
        private FileResult getFile(string filename)
        {

            return getFile(filename, filename, MimeMapping.GetMimeMapping(filename));
          

        }
        private FileResult getFile(string filename, string downloadname)
        {

            return getFile(filename, downloadname, MimeMapping.GetMimeMapping(filename));


        }
         private FileResult getFile(string filename, string downloadname, string contenttype)
        {
            string path = System.IO.Path.Combine(Request.PhysicalApplicationPath, "Files",filename);
            FilePathResult fpr = new FilePathResult(path, contenttype);
            fpr.FileDownloadName = downloadname;
            return fpr;

        }
    }
}