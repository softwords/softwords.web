﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
//using System.Net.Http;


namespace Softwords.Web.mvcControllers
{
  // this class is a proxy to redirect to the report server, using the Windows identity of this site
  // This can allow requests from users without a windiws identity, ie token-based uathentication
  // this seems an easier path than trying to reconfigure SSRS to handle different authentication types

  public class SSRSController : mvcControllerBase
  {
    [System.Web.Mvc.HttpPost]
    [System.Web.Mvc.Authorize]
    public async Task<ActionResult> RunReport(string format)
    {
      // the report name, together with the SSRS report parameters
      //, is sent in the post data
      // so, we have to extract this first
      // we allow the target format
      var postData = Request.Form;
      var reportPath = postData["reportPath"];
      // add the format?;
      // remove the reportPath


      // convert to compastible with httpClient
      var postDataOut = new List<KeyValuePair<string, string>>();
      foreach (var k in postData.AllKeys)
      {
        if (k != "reportPath")
        {
          var kv = new KeyValuePair<string, string>(k, postData.GetValues(k)[0]);
          postDataOut.Add(kv);
        }
      }


      return await doRunReport(reportPath, format, postDataOut, "report.xlsx");
    }

    [System.Web.Mvc.HttpPost]
    //[System.Web.Mvc.Authorize]
    [System.Web.Mvc.Route(@"ssrs/json/{reportPath}/{format}")]
    public async Task<ActionResult> RunJsonReport(string reportPath, string format, dynamic json) //Dictionary<string, dynamic>
        {
            // the report name, is in the url
            // so is the format
            //, the report parameters are sent as json in the post data

            // add the format?;
            // remove the reportPath
           

            // convert to compastible with httpClient
            var postDataOut = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(json);
      ////foreach (var k in postData.Keys)
      ////{
      ////  dynamic v = "";
      ////  if (postData.TryGetValue(k, out v))
      ////  {
      ////    var kv = new KeyValuePair<string, string>(k, v.toString());
      ////    postDataOut.Add(kv);
      ////  }

            ////}


            return await doRunReport(reportPath, format, postDataOut, "report.xlsx");
    }
    protected async Task<ActionResult> doRunReport(string reportPath, string format,
      IEnumerable<KeyValuePair<string, string>> postData, string fileName)
    {
      HttpContent content = new FormUrlEncodedContent(postData);
      // get the web client
      var httpClient = new HttpClient(new HttpClientHandler {UseDefaultCredentials = true});

      var rptsvr = WebConfigurationManager.AppSettings["ReportServerUrl"];

      HttpResponseMessage cliResponse = null;
      ;
      reportPath = rptsvr + "?/" + reportPath + "&rs:ClearSession=true&rs:format=" + format;
      try
      {
        cliResponse = await httpClient.PostAsync(reportPath, content);
      }
      catch (Exception ex)
      {
        throw ex;
      }
      //Redirector r = Redirector.getRedirector(reportPath, httpClient);
      //// r.pdfForm = d["sycFormPDF"].ToString();

      var stream = await cliResponse.Content.ReadAsStreamAsync();

      //string content_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      var content_type = "application/vnd.ms-excel";
      return File(stream, content_type, fileName);
    }
  }
}