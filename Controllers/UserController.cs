﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Softwords.Web.Models;

namespace Softwords.Web.mvcControllers
{
    public class UserController : mvcControllerBase
    {
        TitledView TitledVM;
        public UserController()
        {
            TitledVM = new TitledView(this.AppSetting("title"));
        }
        // GET: User
        public ActionResult Login()
        {
            return TitledView();
        }
        public ActionResult AccessDenied()
        {
            return View();
        }
        public ActionResult ChangePassword()
        {
            return TitledView();
        }
		[LayoutInjector("MaterialDialogLayout")]
		public ActionResult ChangePasswordPopup()
		{
			return TitledView();
		}

		public ActionResult ForgotPassword()
        {
            return TitledView();
        }
        public ActionResult ResetPassword()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult InviteUser()
        {
            return View();
        }

        public ActionResult ConfirmInvite()
        {
            return TitledView();
        }

        public ActionResult PasswordValidator()
        {
            return View();
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult LoginPopup()
        {
            return View();
        }

        private  ActionResult TitledView()
        {
            return View(TitledVM);
        }

    }
}