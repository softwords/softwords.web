﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Softwords.Web.Models;
using System.Web.Http.Owin;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Softwords.Web.mvcControllers
{
    /// <summary>
    ///  controllers derive from this class rather than directly from Controller
    ///  This givess access to the ApplicatinUser and the ApplicationUserManager objects
    ///  which can be recontructed from the OwinContext
    ///  /// see http://blogs.msdn.com/b/webdev/archive/2014/02/12/per-request-lifetime-management-for-usermanager-class-in-asp-net-identity.aspx
    ///  Note GetUserManager is an extension method in Microsoft.AspNet.Identity.Owin;
    /// </summary>
    public class mvcControllerBase : Controller
    {
        //
        // GET: /HelloWorld/
        private ApplicationUserManager umgr;
        protected ApplicationUserManager UserManager
        {
            get
            {
                if (umgr == null)
                {
                    getUser();
                }
                return umgr;
            }
        }
        private Softwords.Web.ApplicationUser currentUser;

        protected Softwords.Web.ApplicationUser CurrentUser
        {
            get
            {
                if (currentUser == null)
                {
                    getUser();
                }
                return currentUser;
            }
        }

        private void getUser()
        {
            //The GetOwinContext extension method is in the System.Web.Http.Owin dll
            // which needs to be downloaded as a nuget package (The nuget package name is Microsoft.AspNet.WebApi.Owin)
            //http://stackoverflow.com/questions/22598567/asp-net-webapi-cant-find-request-getowincontext

            umgr = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var id = User.Identity.GetUserId();

            currentUser = umgr.FindById(id);
            if (currentUser.UserName != User.Identity.Name)
            {
                currentUser.Proxify(User.Identity.Name);
            }
        }

        public string Context
        {
            get { return System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? string.Empty; }
        }
        protected ActionResult contextView(string viewName)
        {
            string system;
            var claim = (from c in CurrentUser.Claims where c.ClaimType == "system" select c).First();
            if (claim != null)
            {
                system = claim.ClaimValue;
                try
                {
                    return View(viewName + system);
                }
                catch { }
            }
            return View(viewName);
        }

        protected ActionResult vImage(string filename)
        {
            string fullPath = vImagePath(filename);
            if (fullPath != null)
            {
                string mime = System.Web.MimeMapping.GetMimeMapping(fullPath);
                return File(fullPath, mime);
            }
            return new HttpNotFoundResult(string.Format("Virtual image \"{0}\" could not be resolved", filename));
        }

        protected string vImagePath(string filename)
        {
            string[] extensions = new string[] { "png", "jpg", "jpeg", "gif", "bmp" };
            return vAssetPath(filename, "img", extensions);
        }
        protected string vAssetPath(string filename, string subfolder, string[] extensions)
        {
            string[] paths;
            if (Path.HasExtension(filename))
            {
                // don;t bother with the default extensions
                paths = new string[]
                {
                    "~/assets_local/{2}/{1}",
                    "~/assets_local/{1}",
                    "~/assets/{0}/{2}/{1}",
                    "~/assets/{0}/{1}",
                    "~/assets/{2}/{1}",
                    "~/assets/{1}",
                };
                extensions = new string[] { string.Empty };
            }
            else
            {
                paths = new string[]
                {
                "~/assets_local/{2}/{1}.{3}",
                "~/assets_local/{1}.{3}",
                "~/assets/{0}/{2}/{1}.{3}",
                "~/assets/{0}/{1}.{3}",
                "~/assets/{2}/{1}.{3}",
                "~/assets/{1}.{3}",

                };
            };
            foreach (string path in paths)
            {
                foreach (string ext in extensions)
                {
                    string fullPath = String.Format(path, Context, filename, subfolder, ext);
                    fullPath = Server.MapPath(fullPath);
                    if (System.IO.File.Exists(fullPath))
                    {
                        return fullPath;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Get the value of an appsettingin web .config.
        /// Useful for using an appsetting in a viewbag
        /// </summary>
        /// <param name="settingName">the name of the setting</param>
        /// <returns>string</returns>
        public string AppSetting(string settingName)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[settingName] ?? String.Empty;
        }
    }
}