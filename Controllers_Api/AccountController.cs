﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web;
using Softwords.Web.Models;

using Softwords.Web.Providers;
using Softwords.Web.Results;
using Softwords.Web.Identity;
using System.Net.Mail;
using System.Configuration;

using RazorEngine;
using RazorEngine.Templating;

namespace Softwords.Web.Controllers
{
    [Authorize]
    [RoutePrefix("_api/Account")]           // _api distinguishes "system" calls from "application" calls
    public class AccountController : apiControllerBase
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        // by default, unity will try to use the constructor with the most arguments
        // we just want to use this empty constructor, since we don;t need to push a data factory in to this controller
        [Unity.InjectionConstructor]
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET _api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public async Task<UserInfoViewModel> GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            // http://www.codeproject.com/Articles/843445/ASP-NET-Web-Api-and-Identity-Customizing-Identity
            // shows how to get teh ApplicationUser - then says you wouldn;t ??
            ApplicationUser user = UserManager.FindByName(User.Identity.Name);

            UserInfoViewModel vm = new UserInfoViewModel()
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null

            };
            if (user != null)
            {
                vm.menu = await user.Navigation.menu();
                vm.home = user.Navigation.homestate;           // return the home state
            }

            foreach (var role in UserManager.GetRoles(User.Identity.GetUserId()))
            {
                vm.Roles.Add(role);
            };

            return vm;
        }

        // POST _api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET _api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

            foreach (IdentityUserLogin linkedAccount in user.Logins)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                });
            }

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
                ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
            };
        }

        #region Password Management

        /// <summary>
        /// a logged in user can change their passsword by specifying the existing password,and the new password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST _api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // separate the AD calls from the ASP.NET Identity calls
            ISecurityOps ops = securityOpsFactory(User);
            if (ops == null)
            {
                return InternalServerError();
            }
            IdentityResult result = await ops.changePassword(model);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            using (var logDb = new Logging.LoggingDbContext())
            {
                Logging.Activity activity = new Logging.Activity()
                {
                    activityLogin = User.Identity.Name,
                    activityTask = "Web Change Password",
                    activityTime = DateTime.UtcNow,
					activityScope = ConfigurationManager.AppSettings["scope"]

				};
                logDb.Activities.Add(activity);
                await logDb.SaveChangesAsync();
            }
            return Ok();
        }

        // what is the use for this????
        // POST _api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        /// <summary>
        /// a user not logged in can supply their email - a password reset token will be sent to that email address
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //https://stackoverflow.com/questions/38705109/forgot-password-with-asp-net-identity
        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);

                if (user == null)
                {
                    return Ok();
                }
                string urlbase = Request.RequestUri.AbsoluteUri.ToLower().Replace(@"_api/account/forgotpassword", "");

                bool confirmed = await UserManager.IsEmailConfirmedAsync(user.Id);
                if (!confirmed)
                {
                    // the user has selected resetpassword, but has not yet confirmed their password
                    // send them a email confirmation
                    await new Email.EmailGenerator(UserManager).sendConfirmEmail(user, urlbase, String.Empty, null);
                    return Ok();
                }
                try
                {
                    await new Email.EmailGenerator(UserManager).sendResetPassword(user, urlbase);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
                if (Logging.LoggingDbContext.IsLoggingEnabled())
                {
                    using (var logDb = new Logging.LoggingDbContext())
                    {
                        Logging.Activity activity = new Logging.Activity()
                        {
                            activityLogin = model.Email,
                            activityTask = "Web Reset Password",
                            activityTime = DateTime.UtcNow,
							activityScope = ConfigurationManager.AppSettings["scope"]
						};
                        logDb.Activities.Add(activity);
                        await logDb.SaveChangesAsync();
                    }
                }
                return Ok();
            }

            // If we got this far, something failed, redisplay form
            return BadRequest(ModelState);
        }

  
        // a newly created user (sef-registered or registered by the administrator with custom permissions)
        // can specify their own password - logic on the server is the same as password reset
        // This is forced in response to an administrator creating the account for the user and inviting them
        // via inviteUser
        // Note that the user is identified by userId - is the guid on AspNetUsers in this call
        // this guid is passed back on the Url in the confirm email sent from inviteuser
        //
        // inviteuser method allows the admin to specify some custom role or menu for that user
        [HttpPost]
        [AllowAnonymous]
        [Route("ConfirmInvite")]
        public async Task<IHttpActionResult> ConfirmInvite(ResetPasswordBindingModel model)
        {
            return await doResetPassword("Web Confirm Invitation", model);
        }

        // in repsonse to an email providing a password reset token , the user specifies their new password.
        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordBindingModel model)
        {
            return await doResetPassword("Web Reset Password", model);
        }

        /// <summary>
        /// Resets the password, supplying the password reset token sent to the user
        /// this can be used to force a password reset, or in the context of 'forgot password"
        /// in this instance the token is sent to the supplied email, so we know the user owns that email
        /// </summary>
        /// <param name="context">context of action, for logging purposes only</param>
        /// <param name="model">data supplied from client. the reset token from the email is returned in this package, with the Id and new password</param>
        /// <returns></returns>
        private async Task<IHttpActionResult> doResetPassword(string context, ResetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await UserManager.FindByIdAsync(model.UserId);
            if (user == null)
            {
                // Don't reveal that the user does not exist 
                return Ok();
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.NewPassword);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors.FirstOrDefault());
            }


            // we cut a corner here, relying on the fact that the reset password has occured to verify the email
            // but, we have to have a token to confirm the email via the api, so we generate it, then use it.
            // Probably an alternative is to go straight to entity framework to set this field?
            if (!user.EmailConfirmed)
            {
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                result = await UserManager.ConfirmEmailAsync(user.Id, code);
                if (!result.Succeeded)
                {
                    return BadRequest("Cannot confirm email: " + result.Errors.FirstOrDefault());
                }
            }


            if (user.Email != null && Logging.LoggingDbContext.IsLoggingEnabled())
            {
                using (var logDb = new Logging.LoggingDbContext())
                {
                    Logging.Activity activity = new Logging.Activity()
                    {
                        activityLogin = user.Email,
                        activityTask = context,
                        activityTime = DateTime.UtcNow,
						activityScope = ConfigurationManager.AppSettings["scope"]
					};
                    logDb.Activities.Add(activity);
                    await logDb.SaveChangesAsync();
                }
            }
            return Ok();

        }

        /// <summary>
        /// Get the structure of the password for clientside validations
        /// </summary>
        /// <returns>the password validator, as configured in ApplicationUserManager.Create</returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("PasswordInfo")]
        public IIdentityValidator<string> PasswordInfo()
        {
            return UserManager.PasswordValidator;
        }

        #endregion

        #region External login support

        // add an external login, specifying the login and the source
        // POST _api/Account/AddExternalLogin
        [Route("AddExternalLogin")]
        public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null || ticket.Identity == null || (ticket.Properties != null
                && ticket.Properties.ExpiresUtc.HasValue
                && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
            {
                return BadRequest("External login failure.");
            }

            ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
                new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // remove a login - may be the local login
        // POST _api/Account/RemoveLogin
        [Route("RemoveLogin")]
        public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result;

            if (model.LoginProvider == LocalLoginProvider)
            {
                result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
                    new UserLoginInfo(model.LoginProvider, model.ProviderKey));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // GET _api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user, user.UserName, oAuthIdentity);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET _api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        {
            IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
            List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

            string state;

            if (generateState)
            {
                const int strengthInBits = 256;
                state = RandomOAuthStateGenerator.Generate(strengthInBits);
            }
            else
            {
                state = null;
            }

            foreach (AuthenticationDescription description in descriptions)
            {
                ExternalLoginViewModel login = new ExternalLoginViewModel
                {
                    Name = description.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = description.AuthenticationType,
                        response_type = "token",
                        // ugh but we need to refactor this to be able to share it
                        // public client id doesn;t seem to get changed or used so for now
                        // i will hold my nose ands use a literal
                        client_id = "self", //Startup.PublicClientId,
                        redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
                        state = state
                    }),
                    State = state
                };
                logins.Add(login);
            }

            return logins;
        }
        #endregion

        #region Creating user
        // a user creates an account for themselves
        // they cannot set permissions or roles or menu key - these go to defaults
        // POST _api/Account/Register
        [AllowAnonymous]
        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
			// cannot use register method except when scope = 'public'
			if (this.Scope != "public")
			{
				// play dumb
				return NotFound();
			}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // note the change here to use a username, and additional fields
            var user = new ApplicationUser()
            {
                UserName = (model.UserName == null ? model.Email : model.UserName),
                Email = model.Email,
                MenuKey = model.MenuKey == null ? "PUBLIC" : model.MenuKey,
                Country = model.Country
            };


            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            System.Diagnostics.Debug.Assert(user.Id != null);
            if (model.Permission != null)
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("Permission", model.Permission));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            // see
            //http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset#require
            // for how to proceed in adding email verify
            // we'll use a resetpasswordtoken, not emailconfirmationtoken, 
            // we'll update the passwords on response
            // TO DO: there should be a GOOD way to do this
            string urlbase = Request.RequestUri.AbsoluteUri.ToLower().Replace(@"_api/account/register", "");

            await new Email.EmailGenerator(UserManager).sendConfirmEmail(user, urlbase, null, null);
            return Ok();
        }

        /// <summary>
        ///alternative self-registration process:
        /// begins only with the email address
        /// this fires a confirm message and onconform, the user must change the password
        /// This endpoint is called when the email address is entered; 
        /// it CREATES the account with that emai address and default settings
        /// then, it sends the email message
        /// with the rest password token to that address
        /// Note that this means that spammers will leave dormant accounts in AspNetUsers...
        /// </summary>
        /// <param name="model">binding model</param>
        /// <returns>status code</returns>
        [AllowAnonymous]
        [Route("confirmemail")]
        public async Task<IHttpActionResult> ConfirmEmail(ConfirmEmailBindingModel model)
        {
			// cannot use register method except when scope = 'public'
			if (this.Scope != "public")
			{
				// play dumb
				return NotFound();
			}

			if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            // note the change here to use a username, and additional fields
            var user = new ApplicationUser()
            {
                UserName = (model.UserName == null ? model.Email : model.UserName),
                Email = model.Email,
                MenuKey = "PUBLIC"
            };


            // any old password, irretrievable - but must match the password rules
            // the user will subsequently be prompted to supply their own
            string password = Guid.NewGuid().ToString() + "Abc09$!";      // force all the pssible password validations        

            // email fields
            string title = ConfigurationManager.AppSettings["title"];
            string subject = string.Format(@"Welcome to {0}!", title);
            // TO DO: there should be a GOOD way to do this
            string urlbase = Request.RequestUri.AbsoluteUri.ToLower().Replace(@"_api/account/confirmemail", "");


            IdentityResult result = await UserManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                // at this point, if the reason for failure is that the name or email is already in use

                // then we will report sucess to the client, but send an email to the email address
                // informing that the attempt to create the account has been made
                // 
                user = await UserManager.FindByEmailAsync(model.Email);
                // if the user has already confirmed, send them a reset password link, at this point
                // note that generating the reset password link doe NOT invalidate the current password
                //user.EmailConfirmed
                if (user.EmailConfirmed)
                {
                    await new Email.EmailGenerator(UserManager).sendWarnOnRegister(user, urlbase, null, null);
                    return Ok();
                }
            }
            System.Diagnostics.Debug.Assert(user.Id != null);

            // see
            //http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset#require
            // for how to proceed in adding email verify
            // we'll use a resetpasswordtoken, not emailconfirmationtoken, 
            // we'll update the passwords on response

            await new Email.EmailGenerator(UserManager).sendConfirmEmail(user, urlbase, model.ReturnUrl, model.format);
            return Ok();
        }

        // POST _api/Account/inviteuser
        /// <summary>
        /// Invite is a method where the user information has not been entered by the user - but by the administrator
        /// setting the user's security parameters
        /// From invite, the user will be sent a confirmation message to confirm their email, and enter their own password
        /// This email assumes there is a clientside route #/confirminvite to handle this
        /// this confirminvite state must call back to _api/confirminvite, passing a new password
        /// 
        /// Note that AllowAnonymous is removed here, requires administrators 
        /// note that could be considered the C in CRUD on ApplicationUsers
        /// the RUD are implemented in UserManagementController
        /// UserInfoBindingModel is common to these
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Inviteuser")]
        [Authorize(Roles = "Administrators")]
        public async Task<IHttpActionResult> InviteUser(UserInfoBindingModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // note the change here to use a username, and additional fields
            var user = new ApplicationUser()
            {
                UserName = (model.UserName == null ? model.Email : model.UserName),
                Email = model.Email,
                MenuKey = model.MenuKey == null ? "PUBLIC" : model.MenuKey,
                Country = model.Country
            };

            // any old password, irretrievable - but must match the password rules
            // the user will subsequently be prompted to supply their own
            string password = Guid.NewGuid().ToString() + "Abc09$!";      // force all the pssible password validations        

            IdentityResult result = await UserManager.CreateAsync(user,password);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            System.Diagnostics.Debug.Assert(user.Id != null);
            if (model.PermissionHash != null)
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("Permission", model.PermissionHash));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            // now add any roles that the admin has provded to this user

            var currentRoles = await UserManager.GetRolesAsync(user.Id);

            var rolesNotExists = model.Roles.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();

            if (rolesNotExists.Count() > 0)
            {
                ModelState.AddModelError("", string.Format("Roles '{0}' does not exist in the system", string.Join(",", rolesNotExists)));
                return BadRequest(ModelState);
            }

            IdentityResult removeResult = await UserManager.RemoveFromRolesAsync(user.Id, currentRoles.ToArray());

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to remove user roles");
                return BadRequest(ModelState);
            }

            IdentityResult addResult = await UserManager.AddToRolesAsync(user.Id, model.Roles.ToArray());

            if (!addResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to add user roles");
                return BadRequest(ModelState);
            }

            // see
            //http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset#require
            // for how to proceed in adding email verify
            // we'll use a resetpasswordtoken, not emailconfirmationtoken, 
            // we'll update the passwords on response
            string urlbase = Request.RequestUri.AbsoluteUri.ToLower().Replace(@"_api/account/inviteuser", "");
     
            await new Email.EmailGenerator(UserManager).sendInvitation(user, urlbase, null);
            return Ok();
        }
        // POST _api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var info = await Authentication.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return InternalServerError();
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }

        #endregion

        #region Claims Management / User Administration

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}/assignclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> AssignClaimsToUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToAssign)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await this.UserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (ClaimBindingModel claimModel in claimsToAssign)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {

                    await this.UserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }

                await this.UserManager.AddClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
            }

            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}/removeclaims")]
        [HttpPut]
        public async Task<IHttpActionResult> RemoveClaimsFromUser([FromUri] string id, [FromBody] List<ClaimBindingModel> claimsToRemove)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var appUser = await this.UserManager.FindByIdAsync(id);

            if (appUser == null)
            {
                return NotFound();
            }

            foreach (ClaimBindingModel claimModel in claimsToRemove)
            {
                if (appUser.Claims.Any(c => c.ClaimType == claimModel.Type))
                {
                    await this.UserManager.RemoveClaimAsync(id, ExtendedClaimsProvider.CreateClaim(claimModel.Type, claimModel.Value));
                }
            }

            return Ok();
        }
        #endregion

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion

        ISecurityOps securityOpsFactory(System.Security.Principal.IPrincipal user)
        {
            System.Security.Claims.ClaimsPrincipal principal = user as System.Security.Claims.ClaimsPrincipal;
            if (principal == null)
            {
                return null;
            }
            if (principal.HasClaim(x => x.Type == "Proxy"))
            {
                // its an AD account
                return new SecurityOpsAD(UserManager, principal);
            }
            // its a regular account
            return new SecurityOpsAspNetIdentity(UserManager, principal);
        }

        #region email

        #endregion
    }
}
