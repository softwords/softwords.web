﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Softwords.Redirector;



namespace Softwords.Web.Controllers
{
    public class DocumentController : ApiController
    {
        [HttpPost]
        async public  Task<HttpResponseMessage> getDocument(string url)
        {


            HttpClientHandler handler = new HttpClientHandler()
            {
                UseDefaultCredentials = true
            };

            HttpClient client = new HttpClient(handler);
            var r = Softwords.Redirector.Redirector.getRedirector(url, client);

            HttpResponseMessage resp = new HttpResponseMessage(HttpStatusCode.OK);
            HttpContent content = await r.toContent();
            resp.Content = content;
            return resp;

        }

    }
}
