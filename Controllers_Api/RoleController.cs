﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web;
using Softwords.Web.Models;

using Softwords.Web.Providers;
using Softwords.Web.Results;
using Softwords.Web.Identity;
using System.Net.Mail;
using System.Configuration;

namespace Softwords.Web.Controllers
{
    /// <summary>
    /// This class is api for managing asp net identity accounts 
    /// decision taken to split these methods out of AccountController which is getting large
    /// </summary>
    //[Authorize(Roles = "Admin")]
    [AllowAnonymous]
    [RoutePrefix("api/umgr")]           // _api distinguishes "system" calls from "application" calls
    public class RoleController : apiControllerBase
    {

        [Route("roles")]
        public object GetRoleList()
        {
            return this.AppRoleManager.Roles.ToArray();
            //ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            
            //var qry = from r in cxt.Roles
            //          select new { r.Id, r.Name };
            //return qry.ToArray();
        }

        [HttpGet]
        [Route(@"roles/{id:guid}", Name = "GetRoleById")]
        public async Task<object> GetRoleById(string id)
        {

            //var role = await this.AppRoleManager.FindByIdAsync(id);
            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            var qry = from r in cxt.Roles
                      where r.Id == id
                      select new { r.Id, r.Name };
            
            return new { Tag = "role", ResultSet = qry.FirstOrDefault() };
        }

        [HttpGet]
        [Route(@"roles/{name}")]
        public async Task<object> GetRoleByName(string name)
        {
            var role = await this.AppRoleManager.FindByNameAsync(name);
            return new { Tag = "role", ResultSet = role };
        }

        [HttpPost]
        [Route(@"roles/{name}")]
        public async Task<object> AddRole(CreateRoleBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = new IdentityRole { Name = model.Name };

            var result = await this.AppRoleManager.CreateAsync(role);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            Uri locationHeader = new Uri(Url.Link("GetRoleById", new { id = role.Id }));

            return Created(locationHeader, role);

        }



        #region helpers
        // duplications from accountController
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        #endregion
    }
}