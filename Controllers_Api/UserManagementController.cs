﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Softwords.Web.Models;


namespace Softwords.Web.Controllers
{
    /// <summary>
    /// This class is api for managing asp net identity accounts 
    /// decision taken to split these methods out of AccountController which is getting large
    /// </summary>
    [Authorize(Roles = "Admins")]
    //[AllowAnonymous]
    [RoutePrefix("api/umgr")]           // _api distinguishes "system" calls from "application" calls
    public class UserManagementController : apiControllerBase
    {
        [HttpGet]
        [Route(@"users")]
        public object getUserList()
        {
            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            var qry = from c in cxt.Users select new { c.Id, c.UserName, c.Email, c.MenuKey, c.LockoutEnabled, c.EmailConfirmed };
            return qry.ToArray();
        }


        // allow this method to accept an email address or a guid, or a random string
        [HttpGet]
        [Route(@"users/{id:guid}")]
        public object getUserbyId(string id)
        {
            if (id == String.Empty)
                return getUserList();



            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();

            var qry = from c in cxt.Users
                      where c.Id == id
                      select c;
            return new { Tag = "user", ResultSet = qry.FirstOrDefault() };
        }

        [HttpGet]
        [Route(@"users/{id}")]
        public object getUserbyName(string id)
        {
            if (id == String.Empty)
                return getUserList();

            if (id == "0")
            {
                // this is shorthand for requesting an empty object
                ApplicationUser u = new ApplicationUser();
                u.Id = null;
                return new { Tag = "user", ResultSet = u };
            }

            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            var qry = from c in cxt.Users
                      where c.UserName == id
                      select c;
            if (IsValidEmail(id))
            {
                qry = from c in cxt.Users
                      where c.Email == id
                      select c;
            }
            return new { Tag = "user", ResultSet = qry.FirstOrDefault() };
        }

        //https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// create a user that may be a web user (ie an account in AspNetUsers)
        /// or a mapping in AspNetUsers of an active directory account
        /// the first case is simply InviteUser in AccountController - but it is expedient to use the object model when 
        /// setting an encrypting a password
        /// InviteUser should send out the email here
        /// -- in the case of a mapped AD group, we still create a password,
        /// but it will never be known or used - ie the user cannot log in directly from this  account
        /// the user name is known in this case - it is an AD group
        /// email is required but not relevant - so assign some dummy to the email....?
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(@"users/")]
        public async Task<IHttpActionResult> CreateUser(UserInfoBindingModel model)
        {
            // deal with mapped AD group
            // its an AD group if user name is supplied, and is not an email
            // creter a fake email - something is required
            // if user name is an email, set email to it as well
            if (model == null)
            {
                return BadRequest();
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // note the change here to use a username, and additional fields
            Boolean isGroup = !(model.UserName.isValidEmail());
            if (isGroup)
            {
                model.Email = String.Format("{0}@{1}.org", model.UserName.Replace(" ", "_"), Context);
            }
            else
            {
                model.Email = model.UserName;
            }
            var user = new ApplicationUser()
            {
                UserName = (model.UserName == null ? model.Email : model.UserName),
                Email = model.Email,
                MenuKey = model.MenuKey == null ? "PUBLIC" : model.MenuKey,
                Country = model.Country
            };

            // any old password, irretrievable - but must match the password rules
            // the user will subsequently be prompted to supply their own
            string password = Guid.NewGuid().ToString() + "Abc09$!";      // force all the pssible password validations        

            IdentityResult result = await UserManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            // turn the permission hash into a claim
            System.Diagnostics.Debug.Assert(user.Id != null);
            if (model.PermissionHash != null)
            {
                await UserManager.AddClaimAsync(user.Id, new Claim("Permission", model.PermissionHash));
            }

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            // turn any filter into a claim
            // in Create, don;t need to check for an exisitng claim

            if (model.FilterOn != null && model.FilterValue != null)
            {

                result = await UserManager.AddClaimAsync(user.Id, new Claim(model.FilterOn, model.FilterValue));
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }

            // now add any roles that the admin has provded to this user
            // don;t need to deal with existing roles becuase it is a new user
            //var currentRoles = await UserManager.GetRolesAsync(user.Id);
            if (model.Roles != null)
            {
                var rolesNotExists = model.Roles.Except(this.AppRoleManager.Roles.Select(x => x.Id)).ToArray();

                if (rolesNotExists.Count() > 0)
                {
                    ModelState.AddModelError("", string.Format("Roles '{0}' does not exist in the system", string.Join(",", rolesNotExists)));
                    return BadRequest(ModelState);
                }

                // model.Roles is an array of Ids 
                // we need to turn this into an array of Names
                IQueryable<string> newRoles = AppRoleManager.Roles
                    .Where(x => model.Roles.Contains(x.Id))
                    .Select(x => x.Name);

                IdentityResult addResult = await UserManager.AddToRolesAsync(user.Id, newRoles.ToArray());

                if (!addResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to add user roles");
                    return BadRequest(ModelState);
                }
            }
            // see
            //http://www.asp.net/mvc/overview/security/create-an-aspnet-mvc-5-web-app-with-email-confirmation-and-password-reset#require
            // for how to proceed in adding email verify
            // we'll use a resetpasswordtoken, not emailconfirmationtoken, 
            // we'll update the passwords on response
            string urlbase = Request.RequestUri.AbsoluteUri.ToLower().Replace(@"api/umgr/users", "");

            if (!isGroup)
            {
                await new Email.EmailGenerator(UserManager).sendInvitation(user, urlbase, null);
            }
            //return Ok(user);
            return Ok(new { Tag = "user", ResultSet = user });

        }



        /// <summary>
        /// Update settings for an exisiting user account
        /// This allows roles to be added,
        /// and a permission or fliter Claim
        /// may set MenuKey in here
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(@"users/{id}")]
        public async Task<IHttpActionResult> userUpdate(UserInfoBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            var user = await UserManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                return BadRequest();
            };

            user.MenuKey = model.MenuKey;
            user.Country = model.Country;
            user.UserName = model.UserName;
            await cxt.SaveChangesAsync();



            if (model.PermissionHash != null)
            {
                Claim permissionClaim = (await UserManager.GetClaimsAsync(user.Id))
                                            .Where(c => c.Type == "Permission").FirstOrDefault();
                if (permissionClaim != null)
                {
                    await UserManager.RemoveClaimAsync(user.Id, permissionClaim);
                }
                var result = await UserManager.AddClaimAsync(user.Id, new Claim("Permission", model.PermissionHash));
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }
			// manage country filter Claim
			var countryClaims = (await UserManager.GetClaimsAsync(user.Id))
									.Where(c => c.Type == "filterCountry");
			bool foundCountry = false;
			foreach( Claim c in countryClaims)
			{
				if (model.Country == null|| (model.Country != c.Value) || foundCountry)
				{
					await UserManager.RemoveClaimAsync(user.Id, c);
				} else
				{
					foundCountry = true;
				}


			}
			if (model.Country != null && !foundCountry)
			{
				var result = await UserManager.AddClaimAsync(user.Id, new Claim("filterCountry", model.Country));
				if (!result.Succeeded)
				{
					return GetErrorResult(result);
				}
			}

			// manage adhoc filter Claim
			Claim filterClaim = (await UserManager.GetClaimsAsync(user.Id))
                                        .Where(c => c.Type.StartsWith("filter") && c.Type != "filterCountry").FirstOrDefault();

            if (filterClaim != null)
            {
                if (model.FilterOn == null ||
                    model.FilterValue == null ||
                    filterClaim.Type != model.FilterOn || filterClaim.Value != model.FilterValue)
                    await UserManager.RemoveClaimAsync(user.Id, filterClaim);
            }
            if (model.FilterOn != null && model.FilterValue != null
                && (filterClaim == null || (filterClaim.Type != model.FilterOn || filterClaim.Value != model.FilterValue)))
            {

                var result = await UserManager.AddClaimAsync(user.Id, new Claim(model.FilterOn, model.FilterValue));
                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
            }
            if (model.Roles != null)
            {
                // model.Roles is an array of Ids 
                // we need to turn this into an array of Names
                string[] rolesArray = new string[] { };

                IQueryable<string> newRoles = AppRoleManager.Roles
                    .Where(x => model.Roles.Contains(x.Id))
                    .Select(x => x.Name);

                //var rolesNotExists = model.Roles.Except(this.AppRoleManager.Roles.Select(x => x.Name)).ToArray();
                var rolesNotExists = newRoles.Except(AppRoleManager.Roles.Select(x => x.Name)).ToArray();
                if (rolesNotExists.Count() > 0)
                {
                    ModelState.AddModelError("", string.Format("Roles '{0}' does not exist in the system", string.Join(",", rolesNotExists)));
                    return BadRequest(ModelState);
                }

                // step through the complete set of roles 
                // better is to use except - so we don;t delete and re-add the same records
                // current roles is IList<string> of the role names, not Ids
                var currentRoles = await UserManager.GetRolesAsync(user.Id);

                IdentityResult removeResult = await UserManager.RemoveFromRolesAsync(user.Id,
                        currentRoles.Except(newRoles).ToArray());
                if (!removeResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to remove user roles");
                    return BadRequest(ModelState);
                }

                IdentityResult addResult = await UserManager.AddToRolesAsync(user.Id,
                    newRoles.Except(currentRoles).ToArray());
                if (!addResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to add user roles");
                    return BadRequest(ModelState);
                }
            }
            return Ok(user);
        }

        [HttpDelete]
        [Route(@"users/{id}")]
        public object deleteUserbyId(string id)
        {
            if (id == String.Empty)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "user Id not supplied");

            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();

            var qry = from c in cxt.Users
                      where c.Id == id
                      select c;
            ApplicationUser user = qry.FirstOrDefault();

            if (user == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid user Id");
            }
            cxt.Users.Remove(user);
            cxt.SaveChanges();
            return user;
        }


        #region menus (navigations)

        [Route("menukeys")]
        public object GetMenuKeyList()
        {
            ApplicationDbContext cxt = Request.GetOwinContext().Get<ApplicationDbContext>();
            var qry = from r in cxt.Navigation
                      where r.homestate != null
                      select new { r.id, r.label };
            return qry.ToArray();
        }

        [HttpGet]
        [Route(@"navigations/{key}")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> GetNavigation( string key)
		{
            var nm = new NavigationManager();
            string menu = await nm.getNavigationMenu(key);
            return RawJsonResponse(menu);
		}

        #endregion
        #region helpers
        // duplications from accountController
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }
            // we'd like to determine if this error is caused by a duplicate name 
            // we have to parse the message for this, it looks like
            // Name <name> is already taken

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    string[] errors = result.Errors.ToArray();

                    foreach (string error in result.Errors)
                    {
                        if (error.StartsWith("Name") && error.Contains("is already taken."))
                        {
                            string name = error.Replace("Name ", "").Replace(" is already taken.", "").Trim();
                            throw DuplicateKeyException.Make("Name", name);
                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                throw ValidationException.Make(result);
            }

            return null;
        }
        #endregion


        #region mail test

        [HttpGet]
        [AllowAnonymous]
        [Route("testmail")]
        public async Task<IHttpActionResult> testMail(string r, string s)
        {
            var m = new IdentityEmailService();
            await m.sendTest(r, s, "Sample body");
            return Ok();
        }
        #endregion
    }
}