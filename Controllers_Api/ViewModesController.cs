﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web;
using Softwords.Web.Models;

using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Web.Http;
using System.IO;

namespace Softwords.Web.Controllers
{

    [RoutePrefix("api")]           // _api distinguishes "system" calls from "application" calls
    public class ViewModesController : apiControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        [Route(@"viewmodes/{feature}")]
        public object viewModes(string feature)
        {
            const string viewModeRoot = @"\viewModes";

            Dictionary<string, object> jsons = new Dictionary<string, object>();       // dictionary to hold the collected modes
            string viewModeBase = System.Web.Hosting.HostingEnvironment.MapPath(viewModeRoot);  // the base path
            string folderPath = String.Empty;

            // get any context modes first....
            if (Context != String.Empty)
            {
                folderPath = System.IO.Path.Combine(viewModeBase, Context, feature);
                collectJsonInFolder(folderPath, jsons);
            }
            folderPath = System.IO.Path.Combine(viewModeBase, "default", feature);
            collectJsonInFolder(folderPath, jsons);
            return jsons.Values;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route(@"reportdefs")]
        public object reportdefs()
        {
            const string reportDefRoot = @"reportDefs";

            Dictionary<string, object> jsons = new Dictionary<string, object>();       // dictionary to hold the collected modes
            string viewModeBase = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + (reportDefRoot);  // the base path
            string folderPath = String.Empty;

            // get any context modes first....
            if (Context != String.Empty)
            {
                folderPath = System.IO.Path.Combine(viewModeBase, Context);


                collectJsonInTree(folderPath, jsons);
            }
            folderPath = System.IO.Path.Combine(viewModeBase, "default");
            collectJsonInTree(folderPath, jsons);
            return jsons;
        }

        private void collectJsonInTree(string folderPath, Dictionary<string, object> jsons)
        {
            if (Directory.Exists(folderPath))
            {
                var childFolders = System.IO.Directory.EnumerateDirectories(folderPath);
                foreach (string folderpath in childFolders)
                {
                    string foldername = System.IO.Path.GetFileName(folderpath);
                    Dictionary<string, object> childJsons;
                    if (!jsons.ContainsKey(foldername))
                    {
                        childJsons = new Dictionary<string, object>();
                        jsons.Add(foldername, childJsons);
                    } else
                    {
                        childJsons = (Dictionary<string, object>)jsons[foldername];
                    }
                    
                    collectJsonInFolder(folderpath, childJsons);
                    collectJsonInTree(folderpath, childJsons);
                }
            }
        }

        private void collectJsonInFolder(string folderPath, Dictionary<string, object> jsons)
        {
            if (Directory.Exists(folderPath))
            {
                var jsonFiles = System.IO.Directory.EnumerateFiles(folderPath, "*.json");
                foreach (string filename in jsonFiles)
                {
                    string jsonstring = System.IO.File.ReadAllText(filename);
                    Newtonsoft.Json.Linq.JObject json = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonstring);
                    string key = (string)json["key"];
                    if (!jsons.ContainsKey(key))
                    {
                        jsons.Add(key, json);
                    }
                }
            }
        }
    }
}
