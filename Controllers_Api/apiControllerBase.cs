﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Softwords.Web.Models;
using System.Web.Http.Owin;
using Microsoft.AspNet.Identity.Owin;


using Newtonsoft.Json.Linq;
namespace Softwords.Web.Controllers
{
    public class apiControllerBase: ApiController
    {


		#region Context and Settings
		/// <summary>
		/// Get the value of an appsettingin web .config.
		/// Useful for using an appsetting in a viewbag
		/// </summary>
		/// <param name="settingName">the name of the setting</param>
		/// <returns>string</returns>
		public string AppSetting(string settingName)
		{
			return System.Web.Configuration.WebConfigurationManager.AppSettings[settingName] ?? String.Empty;
		}
		public string Context
        {
            get { return System.Web.Configuration.WebConfigurationManager.AppSettings["context"] ?? string.Empty; }
        }
        public string Scope
        {
            get { return System.Web.Configuration.WebConfigurationManager.AppSettings["scope"] ?? string.Empty; }
        }

		#endregion

		#region User and Security

		private ApplicationUserManager umgr;
		public ApplicationUserManager UserManager
		{
			get
			{
				if (umgr == null)
				{
					//The GetOwinContext extension method is in the System.Web.Http.Owin dll 
					// which needs to be downloaded as a nuget package (The nuget package name is Microsoft.AspNet.WebApi.Owin)
					//http://stackoverflow.com/questions/22598567/asp-net-webapi-cant-find-request-getowincontext

					umgr = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
				}
				return umgr;
			}
			protected set
			{
				umgr = value;
			}
		}
		private Softwords.Web.ApplicationUser currentUser;

		protected Softwords.Web.ApplicationUser CurrentUser
		{
			get
			{
				if (currentUser == null)
				{
					getUser();
				}
				return currentUser;
			}
		}

		private void getUser()
		{
			currentUser = UserManager.FindByName(User.Identity.Name);
		}

		private ApplicationRoleManager _AppRoleManager = null;
        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                if (_AppRoleManager == null)
                {
                    _AppRoleManager = Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
                }
                return _AppRoleManager;
            }
        }
		#endregion	
		protected HttpResponseMessage RawJsonResponse(JObject json)
        {
            string jsonstring = json.ToString();
            return RawJsonResponse(jsonstring);
        }
        protected HttpResponseMessage RawJsonResponse(string jsonstring)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(jsonstring, System.Text.Encoding.UTF8, "application/json");
            return response;
        }

        protected HttpResponseMessage RawJsonResponse(JObject json, string tag)
        {
            string jsonstring = json.ToString();
            return RawJsonResponse(jsonstring, tag);
        }
        protected HttpResponseMessage RawJsonResponse(string jsonstring, string tag)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(jsonstring, System.Text.Encoding.UTF8, "application/json");
            response.Headers.Add("Tag", tag);
            return response;
        }

    }
}
