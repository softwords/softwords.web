﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using RazorEngine;
using RazorEngine.Templating;

namespace Softwords.Web.Email
{
    /// <summary>
    ///  standard place to generate and send emails relating to touch points of identity
    /// </summary>
    internal class EmailGenerator
    {
		
		
		/// <summary>
		/// Get the value of an appsettingin web .config.
		/// Useful for using an appsetting in a viewbag
		/// </summary>
		/// <param name="settingName">the name of the setting</param>
		/// <returns>string</returns>
		public string AppSetting(string settingName)
		{
			return System.Web.Configuration.WebConfigurationManager.AppSettings[settingName] ?? String.Empty;
		}

		ApplicationUserManager UserManager;
        string hashBang = "#";
        internal EmailGenerator(ApplicationUserManager userManager)
        {
            UserManager = userManager;
            hashBang = ConfigurationManager.AppSettings["hashBang"] ?? "#";
        }

		/// <summary>
		/// Generates the fully qualified base URL for
		/// the application.
		/// </summary>
		/// <returns>The application's base URL.</returns>
		public static string GetBaseUrl()
		{
			var request = System.Web.HttpContext.Current.Request;
			var appRootFolder = request.ApplicationPath;
			if (!appRootFolder.EndsWith("/"))
			{
				appRootFolder += "/";
			}
			return string.Format(
				"{0}://{1}{2}",
				request.Url.Scheme,
				request.Url.Authority,
				appRootFolder
			);
		}
		#region Confirm Email

		/// <summary>
		/// Send the email to the user to confirm an email address, and register a password
		/// </summary>
		/// <param name="user">the user to send the email to</param>
		/// <param name="urlbase">the base url of the application - embedded urls are elative to this</param>
		/// <param name="returnUrl">if supplied, a url to go back to after completing</param>
		/// <param name="format">may be supplied to override the template from the default - confirmEmail </param>
		/// <returns></returns>
		internal async Task sendConfirmEmail(ApplicationUser user, string urlbase, string returnUrl, string format, string subjectFormat = null)
        {
            string title = ConfigurationManager.AppSettings["title"];
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            code = Uri.EscapeDataString(code);
            System.Uri callbackUrl;
            urlbase = urlbase + hashBang + "/";
            // callbackUrl is the clientside state where the user may enter their own password
            // this state must call back to 
            if (returnUrl != null)
            {
                callbackUrl = new Uri(urlbase + "confirminvite/" + user.Id +
                    "?e=" + user.Email + "&back=" + returnUrl + "&code=" + code);

            }
            else
            {
                callbackUrl = new Uri(urlbase + "confirminvite/" + user.Id + "?e=" + user.Email + "&code=" + code);
            }
            // registerUrl is the clientside state for self-registration. This can be ignored in the email body
            // if there is no client-side registration process
            var registerUrl = new Uri(urlbase + "register");
            if (subjectFormat == null)
            {
                subjectFormat = @"{0}: Complete your account setup";
            }
            string subject = string.Format(subjectFormat, title);
            await UserManager.SendEmailAsync(user.Id, subject,
               confirmEmailBody(callbackUrl, registerUrl, format));

        }

        /// <summary>
        /// Generates the body text of the confirm Email message
        /// This is held in the template ConfirmEmail.cshtml, which is maintained by the host system.
        /// The host system is responsible for configuring the Razor Engine
        /// 
        /// </summary>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        internal string confirmEmailBody(System.Uri callbackUrl, System.Uri registerUrl, string format)
        {
            dynamic dd = new System.Dynamic.ExpandoObject();
            dd.callbackUrl = callbackUrl.ToString();
            dd.registerUrl = registerUrl.ToString();
			// bring these into the Model - rather than try to use a model and a ViewBag
			dd.Title = this.AppSetting("title");
			dd.Program = this.AppSetting("program");

		string template = "confirmEmail.cshtml";
            if (format != null)
            {
                template = format + ".cshtml";
            }
			// also pass the TitledView
            return Engine.Razor.RunCompile(template,null, (object)dd);
        }
        #endregion

        #region WarnOnRegister
        internal async Task sendWarnOnRegister(ApplicationUser user, string urlbase, string returnUrl, string format)
        {

            string title = ConfigurationManager.AppSettings["title"];
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            urlbase = urlbase + hashBang + "/";
            code = Uri.EscapeDataString(code);
            System.Uri callbackUrl;
            // callbackUrl is the clientside state where the user may reset their own password
            // this state must call back to 
            if (returnUrl != null)
            {
                callbackUrl = new Uri(urlbase + "resetpassword/" + user.Id +
                    "?e=" + user.Email + "&back=" + returnUrl + "&code=" + code);

            }
            else
            {
                callbackUrl = new Uri(urlbase + "resetpassword/" + user.Id + "?e=" + user.Email + "&code=" + code);
            }
            // registerUrl is the clientside state for self-registration. This can be ignored in the email body
            // if there is no client-side registration process
            var registerUrl = new Uri(urlbase + "register");
            var signinUrl = new Uri(urlbase + "signin");
            var forgotpasswordUrl = new Uri(urlbase + "forgotpassword");

            string subject = string.Format(@"Complete your {0} Registration", title);

            await UserManager.SendEmailAsync(user.Id, subject,
               warnOnRegisterBody(callbackUrl, signinUrl, forgotpasswordUrl));
        }



        /// <summary>
        /// Generates the body text of an information email sent
        /// when a user attempts to register an email that is already an account
        /// Reporting this directly to the user is a potential security breach 
        /// i.e. it informs that person that the owner of the email has registered on the site
        /// So the approach taken is to email the user, warn theat the reset attempt was attempted
        /// if it was them , explain to do a forgot password
        /// Don't disable the email account which would be highly annoying to the real user
        /// 
        /// </summary>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        internal static string warnOnRegisterBody(System.Uri callbackUrl, System.Uri signinUrl, System.Uri forgotpasswordUrl)
        {
            dynamic dd = new System.Dynamic.ExpandoObject();
            dd.callbackUrl = callbackUrl.ToString();
            dd.signinUrl = signinUrl.ToString();
            dd.forgotpasswordUrl = forgotpasswordUrl.ToString();

            const string template = "warnOnRegister.cshtml";
            return Engine.Razor.RunCompile(template, null, (object)dd);
        }

        #endregion


        #region Invite User
        /// <summary>
        /// After creating an account for a user, send an email notifying them of their account
        /// </summary>
        /// <param name="user"></param>
        /// <param name="urlbase"></param>
        /// <param name="returnUrl"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        internal async Task sendInvitation(ApplicationUser user, string urlbase, string returnUrl)
        {
            
            await sendConfirmEmail(user, urlbase, returnUrl, "UserInvitation",@"Welcome to {0}!");
        }

        #endregion

        #region Reset Password
        /// <summary>
        /// Send an email with a rest password token, and link to the resetpassword page
        /// </summary>
        /// <param name="user"></param>
        /// <param name="urlbasel"></param>
        /// <returns></returns>
        internal async Task sendResetPassword(ApplicationUser user, string urlbase)
        {

            var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            // TO DO: there should be a GOOD way to do this
            code = Uri.EscapeDataString(code);
            urlbase = urlbase + hashBang + "/";
            var callbackUrl = new Uri(urlbase + "resetpassword/" + user.Id + "?e=" + user.Email + "&code=" + code);
            string subject = "Reset your password";
            await UserManager.SendEmailAsync(user.Id, subject,
                ResetPasswordBody(callbackUrl));

        }
        /// <summary>
        /// Generates the body text of the Reset Password Email message
        /// This is held in the template ResetPasswordEmail.cshtml, which is maintained by the host system.
        /// The host system is responsible for configuring the Razor Engine
        /// 
        /// </summary>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        private string ResetPasswordBody(System.Uri callbackUrl)
        {
            dynamic dd = new System.Dynamic.ExpandoObject();
            dd.callbackUrl = callbackUrl.ToString();
			// bring these into the Model - rather than try to use a model and a ViewBag
			dd.Title = this.AppSetting("title");
			dd.Program = this.AppSetting("program");

			const string template = "resetPasswordEmail.cshtml";
            return Engine.Razor.RunCompile(template, null, (object)dd);
        }
		#endregion

		#region Confirm password change
		/// <summary>
		/// Send the email to the user to confirm an email address, and register a password
		/// </summary>
		/// <param name="user">the user to send the email to</param>
		/// <param name="urlbase">the base url of the application - embedded urls are relative to this</param>
		/// <returns></returns>
		internal async Task sendConfirmPasswordChangeEmail(ApplicationUser user)
		{
			string title = ConfigurationManager.AppSettings["title"];
			string urlbase = GetBaseUrl();
			var signinUrl = new Uri(urlbase + "signin");
			var forgotpasswordUrl = new Uri(urlbase + "forgotpassword");

			string subject = $"Your password has been changed on {title}";

			await UserManager.SendEmailAsync(user.Id, subject,
			   confirmPasswordChangeBody(signinUrl, forgotpasswordUrl));

		}

		internal string confirmPasswordChangeBody(System.Uri signinUrl, System.Uri forgotpasswordUrl)
		{
			dynamic dd = getDynamic();
			dd.signinUrl = signinUrl.ToString();
			dd.forgotpasswordUrl = forgotpasswordUrl.ToString();

			const string template = "confirmPasswordChange.cshtml";
			return Engine.Razor.RunCompile(template, null, (object)dd);
		}


		#endregion

		#region dynamic 
		private dynamic getDynamic()

		{
			dynamic dd = new System.Dynamic.ExpandoObject();
			// bring these into the Model - rather than try to use a model and a ViewBag
			dd.Title = this.AppSetting("title");
			dd.Program = this.AppSetting("program");
			dd.Context = this.AppSetting("context");
			dd.Scope = this.AppSetting("scope");
			return dd;
		}


		#endregion
	}
}
