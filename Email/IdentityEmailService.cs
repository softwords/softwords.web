﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Configuration;

namespace Softwords.Web
{
    /// <summary>
    /// ASPNet Identity defines IIdentityMessageService to send mesages to the user
    /// There is not a built-in implementation of this to use SmtpClient
    /// 
    /// // but this is good:
    /// ///http://stackoverflow.com.80bola.com/questions/22797845/asp-net-identity-2-0-how-to-implement-iidentitymessageservice-to-do-async-smtp/22797955
    /// </summary>
    class IdentityEmailService : IIdentityMessageService
    {
        string senderName = string.Empty;
        string senderEmail = string.Empty;
        public IdentityEmailService()
        {
            senderName = ConfigurationManager.AppSettings["emailSenderName"] ?? "Web Site Administrator";
            senderEmail = ConfigurationManager.AppSettings["emailSenderAddress"] ?? "webadmin@portal.org";

        }
        public async Task SendAsync(IdentityMessage message)
        {
            // convert IdentityMessage to a MailMessage
            var email =
               new MailMessage(new MailAddress(senderEmail, senderName),
               new MailAddress(message.Destination))
               {
                   Subject = message.Subject,
                   Body = message.Body,
                   IsBodyHtml = true
               };
            string bcc = ConfigurationManager.AppSettings["EmailBcc"];
            if (bcc != null)
            {
                string[] bccs = bcc.Split(';');
                foreach (string s in bccs)
                {
                    email.Bcc.Add(new MailAddress(s));
                }
            }
            using (var client = new SmtpClient()) // SmtpClient configuration comes from config file
            {
                await client.SendMailAsync(email);
            }
        }

        public async Task sendTest(string recipient, string subject, string body)
        {
            var email = new MailMessage(new MailAddress(senderEmail, senderName),
            new MailAddress(recipient))
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = false
            };
            using (var client = new SmtpClient()) // SmtpClient configuration comes from config file
            {
                await client.SendMailAsync(email);
            }
        }
    }
}
