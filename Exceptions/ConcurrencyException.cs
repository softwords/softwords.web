﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Net;
using Newtonsoft.Json;


namespace Softwords.Web
{
    public class ConcurrencyExceptionJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var ex = value as ConcurrencyException;
            writer.WriteStartObject();
            writer.WritePropertyName("inner");
            serializer.Serialize(writer, ex.InnerException);
            writer.WritePropertyName("type");
            serializer.Serialize(writer, ex.ExceptionType);
            writer.WritePropertyName("field");
            serializer.Serialize(writer, ex.Field);
            writer.WritePropertyName("message");
            serializer.Serialize(writer, ex.Message);
            writer.WritePropertyName("data");
            serializer.Serialize(writer, ex.NewVersion);

            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(ConcurrencyException).IsAssignableFrom(objectType);
        }
    }

    /// <summary>
    /// ConcurrencyException will raise a 409 error when the recored to be updated has changed since it was read
    /// by the client.
    /// Usually, this is controlled by the presence of a timestamp on the record. The client passes back the value of the timestamp
    /// as part of the data package, and optionally as the IfMatch header.
    /// </summary>
    [JsonConverter(typeof(ConcurrencyExceptionJsonConverter))]
    public class ConcurrencyException : ClientException
    {
        public object NewVersion { get; set; }

        public ConcurrencyException(string message, Exception inner) : base(message, inner)
        {
            ExceptionType = ClientExceptionType.Concurrency;
        }

        /// <summary>
        /// Raise a conncurrency exception
        /// </summary>
        /// <param name="rowVersionProperty">fieldname of the timestamp field</param>
        /// <param name="newVersion">current version of the record on which the update was attempted</param>
        /// <param name="inner">inner exception</param>
        /// <returns>new ConcurrencyException</returns>
        public static ConcurrencyException Make(string rowVersionProperty, object newVersion, Exception inner)
        {
            return new ConcurrencyException((inner != null)?inner.Message : "Concurrency exception", inner) { NewVersion = newVersion, Field = rowVersionProperty };
        }

        /// <summary>
        /// Raise a conncurrency exception
        /// </summary>
        /// <param name="rowVersionProperty">fieldname of the timestamp field</param>
        /// <param name="newVersion">current version of the record on which the update was attempted</param>
        /// <returns>new ConcurrencyException</returns>
        public static ConcurrencyException Make(string rowVersionProperty, object newVersion)
        {
            return ConcurrencyException.Make(rowVersionProperty, newVersion, null);
        }

    }
}
