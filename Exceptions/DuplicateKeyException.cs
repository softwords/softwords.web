﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Softwords.Web
{
    public class DuplicateKeyException : ClientException
    {
        
        const string MSG = "Key value already exists";
  
        public DuplicateKeyException(object keyValue, Exception inner) : base(MSG, inner)
        {
            FieldValue = keyValue;
        }

        public DuplicateKeyException(string field, object keyValue, string message, Exception inner) 
            : base(field, message, inner)
        {
            ExceptionType = "DuplicateKey";
            FieldValue = keyValue;
        }

        /// <summary>
        /// Return a duplicate key exception
        /// </summary>
        /// <param name="keyprop">name of the primary key</param>
        /// <param name="keyValue">value of the primary key giving the duplicate</param>
        /// <param name="sqlEx">inner (sql) exception if duplicate key was repotteed in a sql insert or update</param>
        /// <returns>DuplicateKeyException</returns>
        public static DuplicateKeyException Make(string keyprop, object keyValue, System.Data.SqlClient.SqlException sqlEx)
        {
            return new DuplicateKeyException(keyprop, keyValue, MSG, sqlEx);
        }

        /// <summary>
        /// Return a duplicate key exception
        /// </summary>
        /// <param name="keyprop">name of the primary key</param>
        /// <param name="keyValue">value of the primary key giving the duplicate</param>
        /// <returns>DuplicateKeyException</returns>
        public static DuplicateKeyException Make(string keyprop, object keyValue)
        {
            return DuplicateKeyException.Make(keyprop, keyValue, null);
        }
    }


}
