﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace Softwords.Web
{
    // Sql errors are in the table SYSMessages
    // this is just a subset of those we mayy convert into particular errors for the client
    // 
    public enum SqlErrors
    {
        CannotInsertNull = 515,
        DuplicateKey = 2627

    }
    public class ExceptionFilterAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
    {

        public override void OnException(System.Web.Http.Filters.HttpActionExecutedContext context)
        {
            HttpStatusCode code = HttpStatusCode.BadRequest; // a reasonable default;

            //if we already have a packaged client exception, decide what status code to send
            if (context.Exception is ClientException)
            {
                ClientException cex = context.Exception as ClientException;
                
                switch(cex.ExceptionType)
                {
                    case ClientExceptionType.RecordNotFound:
                        code = HttpStatusCode.NotFound;         //404    
                        break;
                    case "Concurrency":
                        code = HttpStatusCode.Conflict;         //409
                        break;
                    case ClientExceptionType.Validation:
                    case ClientExceptionType.DbValidation:
                        code = HttpStatusCode.BadRequest;         //400
                        break;
                    case ClientExceptionType.DuplicateKey:
                        code = (HttpStatusCode)422;             // unprocessable entity  not in the enum
                        break;                    
                }
                var response = context.Request.CreateResponse(code, cex);
                context.Response = response;
                return;
            }
            /// can't be reached becuase ValidatioException is a ClientException and so is handled above
            //if (context.Exception is ValidationException)
            //{
            //    var e = (ValidationException)context.Exception;
            //    Dictionary<string, string> d = new Dictionary<string, string>();
            //    d.Add("type", "Validation");
            //    d.Add("field", e.Field);
            //    d.Add("message", e.Message);
            //    var response = context.Request.CreateResponse(HttpStatusCode.BadRequest, e);

            //    context.Response = response;
            //}
            // this case handles errors thrown by Ef validations generated from attributes of the class
            if (context.Exception is System.Data.Entity.Validation.DbEntityValidationException)
            {
                var e = (System.Data.Entity.Validation.DbEntityValidationException)context.Exception;
            
                // 422 is recommended widely for validation errors - we'll keep using 400 for errors that are thrwon in the SQl server
                var response = context.Request.CreateResponse((HttpStatusCode)422, e.EntityValidationErrors);

                context.Response = response;
            }

            // handlers 
            if (context.Exception is System.Data.SqlClient.SqlException)
            {
                var e = (System.Data.SqlClient.SqlException)context.Exception;
                ClientException cex = ClientException.Make(ClientExceptionType.SqlError, e);
                HttpStatusCode statusCode = HttpStatusCode.BadRequest;
                // support the Sql ser ver telling us 'forbidden'
                switch (context.Exception.Message)
                {
                    case "Forbidden":
                        statusCode = HttpStatusCode.Forbidden;
                        break;
                    case "Unauthorized":
                        statusCode = HttpStatusCode.Unauthorized;
                        break;

                }

                context.Response = context.Request.CreateResponse(statusCode, cex);
                return;
            }

            // TO DO Json Serializer for this?
            // derive from ClientException?
            if (context.Exception is PermissionException)
            {
                var e = (PermissionException)context.Exception;
                Dictionary<string, object> d = new Dictionary<string, object>();
                d.Add("type", "Permission");
                d.Add("Topic", e.Topic);
                d.Add("Required", e.Required.ToString());
                d.Add("Current", e.CurrentString);
                var response = context.Request.CreateResponse(HttpStatusCode.Forbidden, d);

                context.Response = response;
                return;
            }

            // anything else we put in a light wrapper
            if (context.Exception.Source != "EntityFramework")
            {
                code = HttpStatusCode.InternalServerError;  // 500
            }

            ClientException cex2 = ClientException.Make(ClientExceptionType.Error, context.Exception);
            var r = context.Request.CreateResponse(code, cex2);
            context.Response = r;
            return;

        }

    }
}
