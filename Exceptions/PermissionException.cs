﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Softwords.Web
{
    public class PermissionException : System.Exception, ISerializable
    {
        public int Topic { get; set; }
        public PermissionAccess Required { get; set; }
        public PermissionAccess Current { get; set; }
        public PermissionException(int topic, PermissionAccess required) : base()
        {
            Topic = topic;
            Required = required;
        }
        public PermissionException(int topic, PermissionAccess required, PermissionAccess current) : base()
        {
            Topic = topic;
            Required = required;
            Current = current;
        }

        public string CurrentString
        {
            get
            {
                StringBuilder s = new StringBuilder();
                foreach (PermissionAccess a in Enum.GetValues(typeof(PermissionAccess)))
                {
                    if (Current.HasFlag(a))
                    {
                        s.Append(a.ToString());
                        s.Append(" ");
                    }
                }
                return s.ToString();
            }
        }

    }


}
