﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Softwords.Web
{
    public class RecordNotFoundException : ClientException
    {

        public RecordNotFoundException(object keyValue, string message, Exception inner) :
            base("RecordNotFound", null, keyValue, message, inner) { }

        public static RecordNotFoundException Make(object keyValue, Exception inner)
        {
            string message = String.Format("Record not found for key '{0}'", keyValue);
            return new Web.RecordNotFoundException(keyValue, message, inner);
        }

        public static RecordNotFoundException Make(object keyValue)
        {
            return Make(keyValue, null);
        }
    }


}
