﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using System.Data.Entity.Validation;

namespace Softwords.Web
{
    public static class ClientExceptionType
    {
        public const string RecordNotFound = "RecordNotFound";     // on an update to a record that cannot be found 
        public const string DuplicateKey = "DuplicateKey";       // sql has reported key violation via error number 515

        public const string Validation = "Validation";
        public const string DbValidation = "DbValidation";
        // validation exceptions can arise in 3 ways:
        // 1) dbValidationException - the data violates rules encoded as Attributes in the data model
        // this validation will occur without sending to Sql Server. Note that the clinet should be able to catch these
        // before submitting; Note that this category can return multiple errors at once

        // 2) SqlException - the validation occurs in the Sql Server. Normally, many such exceptions
        // will be eliminated by the the Attributes in the data model (see 1) ) if Entity Framework is handling
        // the operation. However, other cases, such  Duplicate Keys, will be caught in the SQL Exception, and not pre-empted
        // by EF
        // this category involves parsing the SQl Exception, using the Number (see table SysMessages for a full list)
        // and parsing the (language 1033!) version of the text to get the field if pssible
        // note that we may have the SQlException thrown directly ( by a SqlClinet operation) or else, if the operation is
        // handled by EF, the SQlException will be the Inner Exception of a DbUpdate exception

        // 3) SP Validation Pineapples and Scholar databases use a technique to return an XML object from a SQL error
        // this sends the message as an XML object in the form
        // <ValidationError field="xxx">message</ValidationError>
        // This can be parsed to send the field and message to the client

        // On the client, the vaidation handler will offer these choices:
        // Undo - reread the record and discard changes
        // Edit:  leave the record intact, exit the Handler so that the data may be edited to fix the problem
        // If the Field is known to the Exception, try to put the focus on that field
        // Resubmit - send the record for update again - assumes that you have edited the record!

        // When sending to the client, we follows these rules:
        // the Exception is a subclass of ClientException class
        // DbValidation - ExceptionType: dbValidation 
        // Field and Message - get from first _ValidationError, (note that the whole Exception is passed as inner)

        // Sql Validation - Exception type Validation
        // Field: extracted where possible from the message
        // Message - may be paraphrased from the message or verbatim

        // Concurrency wraps the EF dbConcurrencyException, and provides enough information (ie the Rowversion and currenct data
        // to allow the client to implement Overwrite ( by updating the Rowversion on the submitted data) or rollback
        // Note older implementations explcitly tested the concurrency - now (DynamicBinder) we rely on EF to do it 
        // transparently inside the transaction
        public static string Concurrency = "Concurrency";

        // the following types are used for simle wrappers around errors we don't otherwise know what they are
        // error reported by the Sql Server
        public const string SqlError = "SqlError";              // a sql exception that cannot otherwise be interpreted or parsed

        // error reported by ef update
        // usually this will wrap a SqlException, so it will be reported as a Sql Error
        // circumstances therefore where we may actually use this are hypothetical
        public const string dbUpdate = "dbUpdate";

        public const string Error = "Error";                     // any other exception - just send the whole thing back
    }
    public class ClientExceptionJsonConverter: JsonConverter 
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.NullValueHandling = NullValueHandling.Ignore;
            var ex = value as ClientException;
            writer.WriteStartObject();
            writer.WritePropertyName("inner");
            serializer.Serialize(writer, ex.InnerException);

            writer.WritePropertyName("type");
            serializer.Serialize(writer, ex.ExceptionType);

            writer.WritePropertyName("field");
            serializer.Serialize(writer, ex.Field);

            writer.WritePropertyName("value");
            serializer.Serialize(writer, ex.FieldValue);

            writer.WritePropertyName("message");
            serializer.Serialize(writer, ex.Message);
            if (ex.Data != null)
            {
                writer.WritePropertyName("data");
                serializer.Serialize(writer, ex.Data);
            }
 
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(ClientException).IsAssignableFrom(objectType);
        }
    }

    [JsonConverter(typeof(ClientExceptionJsonConverter))]
    /// <summary>
    /// Base class for exceptions that will go to the client
    /// 
    /// </summary>
    public class ClientException: System.Exception, ISerializable
    {
        public ClientException(string message, Exception inner) : base(message, inner) { }

        protected string message = null;
        public override string Message
        {
            get
            {
                if (base.Message != null)
                    return base.Message;
                if (InnerException != null)
                    return InnerException.Message;
                return null;
            }
        }

        public ClientException(string field, string message, Exception inner) : base(message, inner)
        {
            Field = field;
        }
        public ClientException(string exceptionType, string field, string message, Exception inner) : base(message, inner)
        {
            ExceptionType = exceptionType;
            Field = field;
        }

        public ClientException(string exceptionType, string field, object fieldValue, string message, Exception inner) : base(message, inner)
        {
            ExceptionType = exceptionType;
            Field = field;
            FieldValue = fieldValue;
        }

        public static ClientException Make(string type, Exception inner)
        {
            return new Web.ClientException(type, null, inner.Message, inner);
        }

        /// <summary>
        /// if the error relates to a partiular field, its name is here
        /// This allows the clinet to focus this field in the UI
        /// </summary>
        public string Field { get; set; }
        public object FieldValue { get; set; }
        public string ExceptionType { get; set;  }
    }


    public class ValidationExceptionJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.NullValueHandling = NullValueHandling.Ignore;
            var ex = value as ValidationException;
            writer.WriteStartObject();
            writer.WritePropertyName("inner");
            serializer.Serialize(writer, ex.InnerException);

            writer.WritePropertyName("type");
            serializer.Serialize(writer, ex.ExceptionType);

            writer.WritePropertyName("field");
            serializer.Serialize(writer, ex.Field);

            writer.WritePropertyName("value");
            serializer.Serialize(writer, ex.FieldValue);

            writer.WritePropertyName("message");
            serializer.Serialize(writer, ex.Message);
            if (ex.Data != null)
            {
                writer.WritePropertyName("data");
                serializer.Serialize(writer, ex.Data);
            }
            writer.WritePropertyName("validationErrors");
            serializer.Serialize(writer, ex.ValidationErrors);


            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(ClientException).IsAssignableFrom(objectType);
        }
    }
    // rreturned by some parsable Sql update/insert failure
    [JsonConverter(typeof(ValidationExceptionJsonConverter))]
    public class ValidationException : ClientException, ISerializable
    {
        public ICollection<DbValidationError> ValidationErrors;
        public ValidationException(string field, string message, Exception inner) : base(field, message, inner)
        {
            ExceptionType = ClientExceptionType.Validation;
        }

        public ValidationException(DbEntityValidationException ex)
            : base(ClientExceptionType.DbValidation, null, ex.Message, null)
        {
            ValidationErrors = new List<DbValidationError>();
            // pull out an array of the ValidationErrors 
            foreach (var vr in ex.EntityValidationErrors)
            {
                foreach (var e in vr.ValidationErrors)
                {
                    ValidationErrors.Add(e);
                    if (Field == null)
                    {
                        Field = e.PropertyName;
                        //                        Message = e.ErrorMessage;
                    }
                }
            }
            // set up the first field from the validations
        }
        public ValidationException(string exceptionType, string field, string message, Exception inner)
            : base(exceptionType, field, message, inner) { }

        public static ValidationException Make(System.Data.SqlClient.SqlException sqlEx)
        {
            string theError = sqlEx.Message;
            string message = sqlEx.Message;
            string field = string.Empty;
            string exceptionType = String.Empty;

            switch (sqlEx.Number)
            {
                case (int)SqlErrors.CannotInsertNull:
                    exceptionType = "Validation";
                    // these messages may look like
                    //Cannot insert the value NULL into column 'istsDurationUnit', table 'scholar.dbo.ISTSession_'; 
                    // column does not allow nulls.INSERT fails. The statement has been terminated.

                    string[] a = theError.Split('\'');
                    for (int i = 0; i < a.Length - 1; i++)
                    {
                        if (a[i].Contains("into column"))
                        {
                            // the next next one is the field name
                            field = a[i + 1];
                            break;
                        }
                    }
                    message = String.Format(@"You must supply a value for {0}", field);
                    return new ValidationException(exceptionType, field, message, sqlEx);

            }
            return new ValidationException(exceptionType, field, message, sqlEx);

        }
        public static ValidationException Make(Microsoft.AspNet.Identity.IdentityResult result)
        {
            string message = string.Join("\n\r", result.Errors.ToArray());
            string field = string.Empty;
            string exceptionType = "validation";

            return new ValidationException(exceptionType, field, message, null);
        }

    }
}
