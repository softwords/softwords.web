﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;


namespace Softwords.Web.HtmlHelpers
{
    public static partial class AngularErrorsFor
    {
        /// <summary>
        /// Generate a standard block of error messages using ng-messages 
        /// This is generated from the validations in attributes on the field
        /// Attributes that are used to generate messages, and correspond to html attributes
        /// added by ngBindTo:
        /// --------------------------------------------------------------------------------
        ///  Data Annotation      | Html attribute             | ng-message
        /// -------------------------------------------------------------------------------
        ///  StringLength         | max-length / min-length    | maxlength
        /// -------------------------------------------------------------------------------
        ///  MaxLength            | maxlength                  | maxlength
        /// -------------------------------------------------------------------------------
        ///  Required             | required                   | required
        /// -------------------------------------------------------------------------------
        ///  EmailAddress         | ng-pattern (email regex)   | ng-pattern
        /// -------------------------------------------------------------------------------
        ///  RegularExpression    | ng-pattern (from attribute)| ng-pattern
        /// -------------------------------------------------------------------------------
        /// Notes - 
        /// * maxlength supplied by StringLength will override MaxLength annotation
        /// * maxlength is used to prevent input beyond maxlength, however the error
        ///   may still be needed if data is pasted or loaded into the input
        /// * required is indicated by angularjs material with an * on the label
        /// 
        /// ngErrorMessages includes the enclosing ng-messages div 
        /// ngErrors is just the <ng-message></ng-message> rows generated as above
        ///  Use ngErrors if you want to add additional messages, or customise the 
        ///  enclosing div in some way
        ///  
        /// With ngErrorMessages, the enclosing div has the ng-message directive pointing to the 
        /// control. This uses the name of the control, and assumes the form name is
        /// modelForm
        /// As well, the enclosing div has an ng-if to prevent displays of errors. 
        /// This ng-if assumes the existence of vm.showErrors; 
        /// ie a showErrors method on the controller.
        /// showErrors take the name of the property as argument
        /// showErrors(propertyname) and returns a boolean.
        /// The default rules are:
        /// -- the form is submitted OR the input propertyname is dirty.
        /// This method is implemented as such as in EditController.ts
        /// 
        /// Hints
        /// Following the example here, a Description value in the Display attribute is used as a hint when:
        /// -- there is no error on the field to display (iethe field is valid, etc)
        /// -- the input is empty
        /// -- the Description value differs from both the name and the prompt
        /// 
        /// ngMessageTags 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="customMessages"></param>
        /// <returns></returns>
        /// 
        public static MvcHtmlString ngErrors<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
           , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            var customMessages = new Dictionary<string, object>();
            return ngErrors(htmlHelper, expression, customMessages);
        }

        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">linq expression for field</param>
        /// <param name="customMessages">anonymous object of extra or override messages</param>
        /// <returns></returns>
        /// 
        public static MvcHtmlString ngErrors<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , object customMessages)
        {
            IDictionary<string, object> messageDictionary = customMessages.ToDictionary<object>();
            return ngErrors(htmlHelper, expression, messageDictionary);
        }


        /// <summary>
        /// Impllementation of ngErrors
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="customMessages">in dictionary form</param>
        /// <returns></returns>
        internal static MvcHtmlString ngErrors<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> customMessages)
        {
            var sb = new System.Text.StringBuilder();
            string message = string.Empty;
            Dictionary<string, object> messages = new Dictionary<string, object>();

            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            // use of the Insert extension to add key only when it does not exist - this allows the user
            // to override any attribute when the helper is used by specifiting htmlAttributes argument

            // htmlAttributes.Insert("class", "form-control");
            System.Linq.Expressions.MemberExpression memberExpression = expression.Body as System.Linq.Expressions.MemberExpression;
            var propertyInfo = HelperBase.getPropertyInfo(expression);

            if (memberExpression != null)
            {
                var member = memberExpression.Member;
                string key = string.Empty;
                string defaultmessage = string.Empty;

                //Get display attribute first
                // if it defines a description, we should use that before using the older description attribute.
                DisplayAttribute da = HelperBase.getDisplayAttribute(propertyInfo);

                // StringLength should supercede MaxLength, so look for that first
                StringLengthAttribute sla = propertyInfo.getCustomAttribute<StringLengthAttribute>();

                if (sla != null)
                {
                    if (sla.MinimumLength > 0)
                    {
                        key = "minlength";
                        defaultmessage = String.Format(@"Minimum length for {0} is {1}", displayname, sla.MinimumLength);
                        messages.Add(key, sla.FormatErrorMessage(displayname).applyDefault(defaultmessage));
                    }
                    key = "maxlength";
                    defaultmessage = String.Format(@"Maximum length for {0} is {1}", displayname, sla.MaximumLength);
                    messages.Add(key, sla.FormatErrorMessage(displayname).applyDefault(defaultmessage));
                }

                //iterate the CustomAttributes collection ...
                foreach (var attribute in propertyInfo.getValidationAttributes())
                {
                    switch (attribute.GetType().Name)
                    {
                        case "MaxLengthAttribute":
                            key = "maxlength";
                            defaultmessage = "Exceeds maximum length";
                            break;

                        case "RequiredAttribute":
                            key = "required";
                            defaultmessage = "Required";
                            break;
                        case "EmailAddressAttribute":
                            // add email validation
                            // for best reusability, we'll put the ng-pattern regex as a literal in the output
                            key = "pattern";
                            defaultmessage = string.Format("{0} must be a valid email address", displayname);
                            break;
                        case "RegularExpressionAttribute":
                            key = "pattern";
                            defaultmessage = "Invalid format";
                            break;
                    }
                    messages.Insert(key, attribute.FormatErrorMessage(displayname).applyDefault(defaultmessage));
                }
            }

            // add any validations that depend only on the data type 
            // DateTime is the key example
            Type t = HelperBase.getDataType(propertyInfo);

            if (t == typeof(DateTime))
            {
                messages.Add("valid", "Invalid date");
            }

            // now add in the custom messages
            foreach (string m in customMessages.Keys)
            {
                messages.InsertOrUpdate(m, customMessages[m]);
            }

            // finally, always add a default required message, to avoid lots of overrides for 
            // conditionally required fields
            messages.Insert("required", "Required");

            // convert to html
            foreach (string m in messages.Keys)
            {
                string line = @"<p ng-message=""{0}"">{1}</p>";
                sb.AppendLine(String.Format(line, m, messages[m]));
            }

            return MvcHtmlString.Create(sb.ToString());
        }

        #region ngErrorMessages overloads
        /// <summary>
        /// Display error messages and hints
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static MvcHtmlString ngErrorMessages<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
                        , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            Dictionary<string, object> messageDictionary = new Dictionary<string, object>();
            return ngErrorMessages(htmlHelper, expression, messageDictionary);
        }

        /// <summary>
        /// Display error messages and hints. Includes capacity to override
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="extraMessages">anonymous object with any custom messages to add</param>
        /// <returns></returns>
        public static MvcHtmlString ngErrorMessages<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
                    , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
                    , object customMessages)
        {
            IDictionary<string, object> messageDictionary = customMessages.ToDictionary<object>();
            return ngErrorMessages(htmlHelper, expression, messageDictionary);
        }

        /// <summary>
        /// Internal version accepts a dictionary of extra arguments
        /// Client cshtml pages should supply this as an anonymous object
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="customMessages">custom messages to add</param>
        /// <returns></returns>
        internal static MvcHtmlString ngErrorMessages<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
                        , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
                    , IDictionary<string, object> customMessages)
        {
            string messages = ngErrors(htmlHelper, expression, customMessages).ToString();
            var propertyInfo = HelperBase.getPropertyInfo(expression);
            string tags = errorTags(propertyInfo.Name);
            string html = @"<div {0}>{1}</div>{2}";
            string hint = getHint(propertyInfo);
            html = string.Format(html, tags, messages, hint);
            return MvcHtmlString.Create(html);
        }

        #endregion
        private static string getHint(System.Reflection.PropertyInfo property)
        {
            DisplayAttribute da = HelperBase.getDisplayAttribute(property);

            if (da != null && da.Description != null
                // don;t add the hint if it adds nothing
                && da.Description != da.Name && da.Description != da.Prompt)
            {
                string nif = String.Format(@"vm.showHints('{0}')", property.Name);
                string html = @"<div class=""hint"" ng-if=""{0}"">{1}</div>";
                return String.Format(html, nif, da.Description);

            }
            return null;
        }
        private static string errorTags(string propertyName)
        {
            string message = String.Format(@"vm.modelForm.{0}.$error", propertyName);
            string nif = String.Format(@"vm.showErrors('{0}')", propertyName);

            var sb = new System.Text.StringBuilder();

            sb.AppendLine(String.Format(@"ng-messages=""{0}"" ", message));
            sb.AppendLine(String.Format(@"ng-if=""{0}"" ", nif));

            return sb.ToString();
        }

    }
}

