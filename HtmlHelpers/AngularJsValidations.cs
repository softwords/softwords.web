﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;

namespace Softwords.Web.HtmlHelpers
{
    public static partial class angularJsHelper
    {
        public static MvcHtmlString ngEmail(this HtmlHelper htmlHelper)
        {
            string str = String.Format(" ng-pattern=\"{0}\"", HelperBase.patternEmail);
            return MvcHtmlString.Create(str); 
        }
    }
}
