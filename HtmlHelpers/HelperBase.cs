﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;
using Softwords.Web.Models;

/// <summary>
/// AngularJs Helpers - model-driven templates for producing 
/// boiler pplate angularjs html markup
/// 
/// From the lowest level of the hierarchy:
/// 
/// ngBindTo 
/// - produces the standard minimal attribute markup for an input
/// e.g. ng-model, name, id, 
/// validations if they are present in the property attributes in EF 
/// max-length, required, ng-pattern for email
/// 
/// ngInputFor 
/// an input control wrapped around ngBindTo. This will determine the correct input type
/// based on the data type of the EF property
/// 
/// ngContainerFor 
/// In bootstrap, a form-group cintaing the input (ngInputFor) and a label
/// In material a md-input-container containing the input control.
/// Note that handling of labels can be automatic in material
/// 
/// Display attribute properties
/// the Display attribute on the EF field can contain name, description, and prompt and short name
/// 	Description	- Gets or sets a value that is used to display a description in the UI.
///     GroupName - Gets or sets a value that is used to group fields in the UI.
///     Name - Gets or sets a value that is used for display in the UI.
///     Prompt - Gets or sets a value that will be used to set the watermark for prompts in the UI.
///     ResourceType - Gets or sets the type that contains the resources for the ShortName, Name, Prompt, and Description properties.
///     ShortName - Gets or sets a value that is used for the grid column label.
///     Granular helpers can get these string values  
/// 
namespace Softwords.Web.HtmlHelpers
{
    public static class HelperBase
    {
        // this email regex pattern comes from:
        //  https://stackoverflow.com/questions/5342375/regex-email-validation
        // here it adds starting and ending / for clientside use
        public static string patternEmail =
                    @"/^$|" +           // allow an empty string ^$
                    @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                    @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                    @")+" +
                    @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$/";

        public static string patternTimeOfDay =
                    @"/^$|" +                   // empty string                  
                    @"^(([1-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])" +
                    @"(:[0-5][0-9])?" +                                              // seconds optional in 24 hour format
                    @"$|" +                                                         // 24 hour format
                    @"^(([1-9]|0[1-9]|1[0-2]):[0-5][0-9]" +
                    @"(:[0-5][0-9])?" +
                    @"(\s?)(AM|PM|pm|am))$/";     // am pm format

        #region utilities

        public static ModelMetadata GetMetadata<TModel, TProperty>(
            HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression
            , object attributesObject)
        {
            // let Mvc assemble the meatadata in its usual way
            // note that its possible to regsiter a 'MetadataProvider'
            // to control how this happens
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            // now collect in detail the html attributes on the property
            // put these in AdditionalValues
            // AdditionalValues can automatically pick up dataannotations
            // of type AdditionalMetaData -- any such if there ever are will already be in there
            getAttributes(metadata);
            if (attributesObject != null)
            {
                var clientAttributes = attributesObject.ToDictionary(true);
                foreach (string key in clientAttributes.Keys)
                {
                    metadata.AdditionalValues.InsertOrUpdate(key, clientAttributes[key]);
                }
            }
            return metadata;
        }

        /// <summary>
        /// Core function to assemble the Html attributes that are required on an input control for the field
        /// Values are added to htmlAttributes dictionary
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="viewData"></param>
        /// <param name="expression">Linq expression that defines the field</param>
        /// <param name="htmlAttributes">dictionary of htmlAttributes</param>
        /// <param name="addClasses">add default css classes</param>
        public static void getAttributes<TModel, TProperty>(ViewDataDictionary<TModel> viewData
            , Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes, bool addClasses)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, viewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            System.Linq.Expressions.MemberExpression memberExpression = expression.Body as System.Linq.Expressions.MemberExpression;
            if (memberExpression != null)
            {
                var member = memberExpression.Member as System.Reflection.PropertyInfo;
                var targetType = IsNullableType(member.PropertyType) ? Nullable.GetUnderlyingType(member.PropertyType) : member.PropertyType;

                // tests based on the property data type - so far, only acting on timespan to set a regex
                if (targetType == typeof(System.TimeSpan))
                {
                    htmlAttributes.Insert("ng-pattern", patternTimeOfDay);
                }

                //Get display attribute first
                // if it defines a description, we should use that before using the older description attribute.
                DisplayAttribute da = member.getCustomAttribute<DisplayAttribute>();
                if (da != null)
                {
                    if (da.GetDescription() != null)
                    {
                        htmlAttributes.Insert("title", da.GetDescription());
                    }
                    if (da.GetPrompt() != null)
                    {
                        htmlAttributes.Insert("placeholder", da.GetPrompt());
                    }
                    // we add a groupname property - this has no funactional use, but canbe reported on when iterating through controls
                    if (da.GetGroupName() != null)
                    {
                        htmlAttributes.Insert("groupname", da.GetGroupName());
                    }

                }

                // similarly, StringLength should supercede MaxLength, so look for that first
                StringLengthAttribute sla = member.getCustomAttribute<StringLengthAttribute>();

                if (sla != null)
                {
                    if (sla.MinimumLength > 0)
                    {
                        htmlAttributes.Insert("minlength", sla.MinimumLength);
                    }
                    htmlAttributes.Insert("maxlength", sla.MaximumLength);
                }

                //iterate the CustomAttributes collection ...
                foreach (var attribute in memberExpression.Member.GetCustomAttributes(false))
                {

                    switch (attribute.GetType().Name)
                    {
                        case "MaxLengthAttribute":
                            MaxLengthAttribute mla = attribute as MaxLengthAttribute;
                            {
                                htmlAttributes.Insert("maxlength", mla.Length);
                            }
                            break;
                        case "RequiredAttribute":
                            // note "true" is a string, true -> True otherwise which doesn;t work on the javascript side
                            htmlAttributes.Insert("ng-required", "true");
                            break;
                        case "DescriptionAttribute":
                            System.ComponentModel.DescriptionAttribute dsa = attribute as DescriptionAttribute;
                            htmlAttributes.Insert("title", dsa.Description);
                            break;
                        case "EmailAddressAttribute":
                            // add email validation
                            // for best reusability, we'll put the ng-pattern regex as a literal in the output
                            htmlAttributes.Insert("ng-pattern", HelperBase.patternEmail);
                            break;
                        case "RegularExpressionAttribute":
                            RegularExpressionAttribute rla = attribute as RegularExpressionAttribute;
                            // make a client-side valid regex by enclosing in //
                            htmlAttributes.Insert("ng-pattern", String.Format(@"/{0}/",rla.Pattern));
                            break;
                        case "ForceUpperCaseAttribute":
                            // we'll display it upper case, and then let the server deal with making it upper case
                            htmlAttributes.Insert("style", "text-transform: uppercase");
                            break;
                        case "ClientLookupAttribute":
                            // other attribute we need is client lookup
                            ClientLookupAttribute cla = attribute as ClientLookupAttribute;
                            htmlAttributes.Insert("lkp", cla.Lookup);
                            break;
                    }
                }
                htmlAttributes.Insert("ng-disabled", "!vm.isEditing");
                htmlAttributes.Insert("ng-model", "vm.model." + member.Name);
                htmlAttributes.Insert("name", member.Name);
                htmlAttributes.Insert("id", member.Name);
                // aria support
                // use the description so it is the same as title - some screen readers will not repeat it
                htmlAttributes.Insert("aria-label", da.GetDescription());           
                // placeholder gets repeated which is annoying - kill it
                if (htmlAttributes.ContainsKey("maxlength") && Convert.ToInt32(htmlAttributes["maxlength"]) >= 200)
                {
                    htmlAttributes.Insert("aria-placeholder",
                        String.Format(@"maximum length; {0} characters", htmlAttributes["maxlength"]));
                } else
                {
                    htmlAttributes.Insert("aria-placeholder", "");
                }

            }
        }
        
public static void getAttributes(ModelMetadata metadata)
        {
            var propertyName = metadata.PropertyName;
            var displayname = metadata.DisplayName;
            var description = metadata.Description;
            System.Reflection.PropertyInfo member = metadata.ContainerType.GetProperty(metadata.PropertyName);

            Type targetType = IsNullableType(metadata.ModelType) ? Nullable.GetUnderlyingType(metadata.ModelType) : metadata.ModelType;

            Dictionary<string, object> htmlAttributes = metadata.AdditionalValues;

            // tests based on the property data type - so far, only acting on timespan to set a regex
            if (targetType == typeof(System.TimeSpan))
            {
                htmlAttributes.Insert("ng-pattern", patternTimeOfDay);
            }

            if (member != null)
            {
                //Get display attribute first
                // if it defines a description, we should use that before using the older description attribute.
                DisplayAttribute da = member.getCustomAttribute<DisplayAttribute>();
                if (da != null)
                {
                    if (da.GetDescription() != null)
                    {
                        htmlAttributes.Insert("title", da.GetDescription());
                    }
                    if (da.GetPrompt() != null)
                    {
                        htmlAttributes.Insert("placeholder", da.GetPrompt());
                    }
                    // we add a groupname property - this has no funactional use, but canbe reported on when iterating through controls
                    if (da.GetGroupName() != null)
                    {
                        htmlAttributes.Insert("groupname", da.GetGroupName());
                    }

                }

                // similarly, StringLength should supercede MaxLength, so look for that first
                StringLengthAttribute sla = member.getCustomAttribute<StringLengthAttribute>();

                if (sla != null)
                {
                    if (sla.MinimumLength > 0)
                    {
                        htmlAttributes.Insert("minlength", sla.MinimumLength);
                    }
                    htmlAttributes.Insert("maxlength", sla.MaximumLength);
                }

                //iterate the CustomAttributes collection ...
                foreach (var attribute in member.GetCustomAttributes(false))
                {

                    switch (attribute.GetType().Name)
                    {
                        case "MaxLengthAttribute":
                            MaxLengthAttribute mla = attribute as MaxLengthAttribute;
                            {
                                htmlAttributes.Insert("maxlength", mla.Length);
                            }
                            break;
                        case "RequiredAttribute":
                            // note "true" is a string, true -> True otherwise which doesn;t work on the javascript side
                            htmlAttributes.Insert("ng-required", "true");
                            break;
                        case "DescriptionAttribute":
                            System.ComponentModel.DescriptionAttribute dsa = attribute as DescriptionAttribute;
                            htmlAttributes.Insert("title", dsa.Description);
                            break;
                        case "EmailAddressAttribute":
                            // add email validation
                            // for best reusability, we'll put the ng-pattern regex as a literal in the output
                            
                            break;
                        case "RegularExpressionAttribute":
                            RegularExpressionAttribute rla = attribute as RegularExpressionAttribute;
                            // make a client-side valid regex by enclosing in //
                            htmlAttributes.Insert("ng-pattern", String.Format(@"/{0}/", rla.Pattern));
                            break;
                        case "ForceUpperCaseAttribute":
                            // we'll display it upper case, and then let the server deal with making it upper case
                            htmlAttributes.Insert("style", "text-transform: uppercase");
                            break;
                        case "ClientLookupAttribute":
                            // other attribute we need is client lookup
                            ClientLookupAttribute cla = attribute as ClientLookupAttribute;
                            htmlAttributes.Insert("lkp", cla.Lookup);
                            break;
                    }
                }
                // attributes derived from data type 
                // note that we may  data type again
                switch (metadata.DataTypeName)
                {
                    case "EmailAddress":
                        htmlAttributes.Insert("ng-pattern", HelperBase.patternEmail);
                        break;
                    case "MultilineText":
                        // make an educated guess about the number of rows
                        int rows = 3;
                        if (htmlAttributes.ContainsKey("maxlength"))
                        {
                            rows = Convert.ToInt32(htmlAttributes["maxlength"]);
                            htmlAttributes.Insert("md-maxlength", rows);
                            rows = 1 + (rows / 110);
                        }
                        htmlAttributes.Insert("rows", rows);
                        break;
                }


                // defaults - always add these before overloading anything from the client
                htmlAttributes.Insert("ng-disabled", "!vm.isEditing");
                htmlAttributes.Insert("ng-model", "vm.model." + member.Name);
                htmlAttributes.Insert("name", member.Name);
                htmlAttributes.Insert("id", member.Name);
                // aria support
                // use the description so it is the same as title - some screen readers will not repeat it
                if (da != null)
                {
                    htmlAttributes.Insert("aria-label", da.GetDescription());
                }
                // placeholder gets repeated which is annoying - kill it
                if (htmlAttributes.ContainsKey("maxlength") && Convert.ToInt32(htmlAttributes["maxlength"]) >= 200)
                {
                    htmlAttributes.Insert("aria-placeholder",
                        String.Format(@"maximum length; {0} characters", htmlAttributes["maxlength"]));
                }
                else
                {
                    htmlAttributes.Insert("aria-placeholder", "");
                }

            }
        }

        /// <summary>
        /// Convert dictionary into string of html attributes
        /// </summary>
        /// <param name="htmlAttributes"></param>
        /// <returns>Html string to insert into the cshtml page</returns>
        public static MvcHtmlString putAttributes(IDictionary<string, object> htmlAttributes)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (string key in htmlAttributes.Keys)
            {

                // allow for attributes that don't have a value
                // these may be passed in via an anonymous object
                //e.g.
                // new { flex=""}
                object v = htmlAttributes[key];
                if (v != null)
                {
                    if (v.ToString() == String.Empty)
                    {
                        sb.Append(String.Format(" {0} ", key));
                    }
                    else
                    {
                        sb.Append(String.Format(" {0}=\"{1}\"", key, v));
                    }
                }

            }
            return MvcHtmlString.Create(sb.ToString());
        }


        /// <summary>
        /// Classes require special handling - 
        /// if there is already a class attribute, it should be extended, not replaced 
        /// </summary>
        /// <param name="htmlAttributes">dicationary of html attributes</param>
        /// <param name="classesToAdd">additional css classes to add</param>
        public static void addClasses(IDictionary<string, object> htmlAttributes, string classesToAdd)
        {
            object classes = string.Empty;
            if (htmlAttributes.TryGetValue("class", out classes))
            {
                htmlAttributes.Update("class", classes.ToString() + " " + classesToAdd);
            }
            else
            {
                htmlAttributes.Insert("class", classesToAdd);
            }

        }

        #endregion
        //--------------------------------------------------------


        //--------------------------------------------------------------------
        #region Data Type utilities
        //--------------------------------------------------------------------

        /// <summary>
        /// Get the Type of the field
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression">linq expression defining field</param>
        /// <returns>Type (string if error)</returns>
        internal static Type getDataType<TModel, TProperty>(
             System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
        )
        {
            var member = getPropertyInfo<TModel, TProperty>(expression);
            if (member == null)
            {
                return typeof(String);
            }
            var targetType = IsNullableType(member.PropertyType) ? Nullable.GetUnderlyingType(member.PropertyType) : member.PropertyType;
            return getDataType(member);
        }

        /// <summary>
        /// Get the type of the field, from its PropertyInfo
        /// </summary>
        /// <param name="propertyInfo">proeprtyInfo for the field</param>
        /// <returns>Type</returns>
        internal static Type getDataType(System.Reflection.PropertyInfo propertyInfo)
        {
            var targetType = IsNullableType(propertyInfo.PropertyType) ? Nullable.GetUnderlyingType(propertyInfo.PropertyType) : propertyInfo.PropertyType;
            return targetType;
        }
        /// <summary>
        /// Return true if the type is a Nullable type
        /// </summary>
        /// <param name="type">Type to test</param>
        /// <returns></returns>
        public static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }
        #endregion
        //--------------------------------------------------------

        //--------------------------------------------------------
        #region Specific string values extracted from attributes
        //--------------------------------------------------------

        /// <summary>
        /// Return the Name property from Display; ie the property's UI name
        /// </summary>
        /// <typeparam name="TModel">type of the model</typeparam>
        /// <typeparam name="TProperty">property of the model</typeparam>
        /// <param name="expression">link expression to define the model and property</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayName<TModel, TProperty>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            DisplayAttribute da = getDisplayAttribute(expression);
            if (da == null)
            {
                return null;
            }
            return MvcHtmlString.Create(da.GetName());
        }

        /// <summary>
        /// Return the Description property from Display
        /// </summary>
        /// <typeparam name="TModel">type of the model</typeparam>
        /// <typeparam name="TProperty">property of the model</typeparam>
        /// <param name="expression">link expression to define the model and property</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayDescription<TModel, TProperty>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            DisplayAttribute da = getDisplayAttribute(expression);
            if (da == null)
            {
                return null;
            }
            return MvcHtmlString.Create(da.GetDescription());
        }

        /// <summary>
        /// Return the Prompt property from Display - this is recommended as the place to hold the watermark
        /// </summary>
        /// <typeparam name="TModel">type of the model</typeparam>
        /// <typeparam name="TProperty">property of the model</typeparam>
        /// <param name="expression">link expression to define the model and property</param>
        /// <returns></returns>
        public static MvcHtmlString DisplayPrompt<TModel, TProperty>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            DisplayAttribute da = getDisplayAttribute(expression);
            if (da == null)
            {
                return null;
            }
            return MvcHtmlString.Create(da.GetPrompt());
        }
        #endregion
        //--------------------------------------------------------

        //--------------------------------------------------------
        #region Helper accessors for EF attributes
        //--------------------------------------------------------

        // overloads allow access from either the link expression,
        // or from the PropertyInfo that link expression resolves to
        // key attributes like display have their own methods, otherwise use the generic
        // See also ReflectionExtensions which are used in here

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static DisplayAttribute getDisplayAttribute<TModel, TProperty>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            return getCustomAttribute<TModel, TProperty, DisplayAttribute>(expression);
        }

        /// <summary>
        /// Helper function to retrieve a PropertyInfo from the link expression
        /// Given a link expression, we can get a propertyInfo, and from that 
        /// get the attributes that decorate that property
        /// </summary>
        /// <typeparam name="TModel">the model object</typeparam>
        /// <typeparam name="TProperty">the property of the model</typeparam>
        /// <param name="expression">link expression defining the property, as passed from the cshtml</param>
        /// <returns>the PropertyInfo for the property</returns>
        public static System.Reflection.PropertyInfo getPropertyInfo<TModel, TProperty>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            System.Linq.Expressions.MemberExpression memberExpression = expression.Body as System.Linq.Expressions.MemberExpression;
            if (memberExpression != null)
            {
                return memberExpression.Member as System.Reflection.PropertyInfo;
            }
            return null;
        }
        public static System.Reflection.PropertyInfo getPropertyInfo(ModelMetadata metadata)
        {
            return metadata.ContainerType.GetProperty(metadata.PropertyName); ;
        }
        /// <summary>
        /// Return a custom attribute from the model definition
        /// </summary>
        /// <typeparam name="TModel">the type of the model object</typeparam>
        /// <typeparam name="TProperty">the property of the object</typeparam>
        /// <typeparam name="T">the type of the required attribute</typeparam>
        /// <param name="expression">link expression, as passed from the cshtml
        /// e.g. m => m.familyName 
        /// ie m is TModel m.familyName is TProperty
        /// that specifies the property of the model from which to retrieve the attribute
        /// </param>
        /// <returns>the attribute</returns>
        public static T getCustomAttribute<TModel, TProperty, T>(System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            var member = getPropertyInfo(expression);
            if (member != null)
            {
                return member.getCustomAttribute<T>();  //extension method
            }
            return default(T);
        }

        /// <summary>
        /// Specific method to get the displayAttribute
        /// </summary>
        /// <param name="member">the propertyinfo of the </param>
        /// <returns>the DisplayAttribute of the member</returns>
        internal static DisplayAttribute getDisplayAttribute(System.Reflection.PropertyInfo member)
        {
            return member.getCustomAttribute<DisplayAttribute>();
        }
        #endregion
        //--------------------------------------------------------
    }
}
