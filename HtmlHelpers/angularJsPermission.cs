﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;

namespace Softwords.Web.HtmlHelpers
{
    public static partial class angularJsHelper
    {
        public static MvcHtmlString ngPermission(this HtmlHelper htmlHelper,
                object permissionSet)
        {
            string str = String.Empty;

            // if no permissions, return a space to ensure that other attributes are correctly separated
            if (permissionSet == null)
                return MvcHtmlString.Create(" ");

            if (permissionSet.GetType() == typeof(string))
            {
                str = @" permission permission-only=""['{0}']""";
                str = String.Format(str, permissionSet);
                return MvcHtmlString.Create(str);
            }
            if (permissionSet.GetType().IsArray)
            {
                StringBuilder sb = new StringBuilder();
                object[] arr = permissionSet as object[];
                string[] outarr = new string[arr.Count()] ;
                for (int i = 0;i< arr.Count(); i++)
                {
                    outarr[i] = string.Format("'{0}'", arr[i].ToString());
                }
                str = @" permission permission-only=""[{0}]""";
                str = String.Format(str, String.Join(",", outarr));
                return MvcHtmlString.Create(str);
            }

            return MvcHtmlString.Create(" "); 
        }
    }
}
