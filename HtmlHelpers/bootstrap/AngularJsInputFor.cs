﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;

namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
, System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            var label = System.Web.Mvc.Html.LabelExtensions.LabelFor(htmlHelper, expression, htmlAttributes);

            return ngInputFor(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , object attributesObject)
        {
            IDictionary<string, object> htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(attributesObject);
            return ngInputFor(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            Boolean addClasses = true;

            // first decide what type of ui control is RequiredAttribute for this property
            // this may depend on the data type, as well as the attributes

            // use of the Insert extension to add key only when it does not exist - this allows the user
            // to override any attribute when the helper is used by specifiting htmlAttributes argument

            getAttributes(htmlHelper.ViewData, expression, htmlAttributes, false);

            string controlTag = "input";
            Type targetType = HelperBase.getDataType(expression);

            if (targetType == typeof(DateTime))
            {
                controlTag = "sw-date-input";
                addClasses = false;
            } else if (htmlAttributes.Keys.Contains("lkp"))
            {
                controlTag = "lookup-selector";
                addClasses = false;
            } else
            {
                controlTag = "input";
            }
            if (addClasses)
            {
                AddDefaultClasses(htmlAttributes);
            }
            string inputstr = "<{0} {1} />";

            inputstr = String.Format(inputstr, controlTag,HelperBase.putAttributes(htmlAttributes));
            return MvcHtmlString.Create(inputstr);
        }

    }
}
