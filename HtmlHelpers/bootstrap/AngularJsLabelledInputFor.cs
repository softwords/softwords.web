﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;

namespace Softwords.Web.HtmlHelpers.Bootstrap

{
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngLabelledInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
, System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            var label = System.Web.Mvc.Html.LabelExtensions.LabelFor(htmlHelper, expression, htmlAttributes);

            return ngLabelledInputFor(htmlHelper, expression, htmlAttributes);
        }
        public static MvcHtmlString ngLabelledInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes)
        {
            // get the string for the input control
            MvcHtmlString controlstr = ngInputFor(htmlHelper, expression, htmlAttributes);
            // get the string for the label
            Dictionary<string, object> labelAttributes = new Dictionary<string, object>();
            labelAttributes.Add("class", "col-sm-4 control-label");
            var label = htmlHelper.LabelFor(expression, labelAttributes);
            string inputstr = "<{0} {1} />";
            // return them in a form-group
            string html = @"<div class=""form-group"">"
                         + label.ToString()
                + @"<div class=""col-sm-8"">" + controlstr.ToString()
                + @"</div>
                    </div>";
            return MvcHtmlString.Create(html);
        }
    }
}
