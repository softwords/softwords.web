﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;
using Softwords.Web.Models;

namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngLabelledLookup<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
, System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();

            // this is copied verbatim from LabelledText - TO DO refactor
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            // use of the Insert extension to add key only when it does not exist - this allows the user
            // to override any attribute when the helper is used by specifiting htmlAttributes argument

            getAttributes(htmlHelper.ViewData, expression, htmlAttributes, false);

            
            Dictionary<string, object> labelAttributes = new Dictionary<string, object>();
            labelAttributes.Add("class", "col-sm-4 control-label");
            var label = htmlHelper.LabelFor(expression, labelAttributes);

            string lookupstr = "<lookup-selector {0} ></lookup-selector>";

            lookupstr = String.Format(lookupstr, HelperBase.putAttributes(htmlAttributes));
            //var input = htmlHelper.TextBoxFor(expression, htmlAttributes);
            //string lookupstr = input.ToString().Replace("input", "lookup-selector");
            //lookupstr = lookupstr.Replace("type=\"text\"", String.Empty);
            //lookupstr = lookupstr.Replace("value=\"\"", "");


            string html = @"<div class=""form-group"">"
                         + label.ToString()
                + @"<div class=""col-sm-8"">" + lookupstr
                + @"</div>
                    </div>";
            return MvcHtmlString.Create(html);
        }
    }
}
