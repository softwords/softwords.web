﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using Softwords.Web.HtmlHelpers;


namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    /// <summary>
    /// Provider for Mvc HtmlHelpers that will emit code marked up for AngularJs
    /// according to the standards of this framework:
    /// ie - the controllerAs is assumed to be vm
    /// - the data object is vm.model
    /// This will use any data annotations on the model to generate these tags:
    /// -- maxlength
    /// -- minlength
    /// -- required
    /// -- placeholder
    /// -- title
    /// 
    /// 
    /// </summary>
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
  , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression, string layout)
        {
            string template = @"<md-radio-group layout=""{1}"" {0}>
                 <md-radio-button value = ""1"">Yes</md-radio-button>
                 <md-radio-button value = ""0"">No</md-radio-button>
            </ md-radio-group>";

            var htmlAttributes = new Dictionary<string, object>();
            getAttributes(htmlHelper.ViewData, expression, htmlAttributes, false);
            string attrs = HelperBase.putAttributes(htmlAttributes).ToString();

            string tag = String.Format(template, attrs, layout);
            return MvcHtmlString.Create(tag);
        }
        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
 , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            return ngYesNo(htmlHelper, expression, "column");
        }
    }
}