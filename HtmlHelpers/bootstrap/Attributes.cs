﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    public static partial class AngularJsHelper
    {
        public static void getAttributes<TModel, TProperty>(ViewDataDictionary<TModel> viewData
     , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
     , IDictionary<string, object> htmlAttributes, bool addClasses)
        {
            HelperBase.getAttributes(viewData, expression, htmlAttributes, addClasses);
            if (addClasses)
            {
                AddDefaultClasses(htmlAttributes);
            }
        }

        public static void AddDefaultClasses(IDictionary<string, object> htmlAttributes)
        {
            HelperBase.addClasses(htmlAttributes, "form-control");
        }
    }
}
