﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using Softwords.Web.HtmlHelpers;


namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    /// <summary>
    /// Provider for Mvc HtmlHelpers that will emit code marked up for AngularJs
    /// according to the standards of this framework:
    /// ie - the controllerAs is assumed to be vm
    /// - the data object is vm.model
    /// This will use any data annotations on the model to generate these tags:
    /// -- maxlength
    /// -- minlength
    /// -- required
    /// -- placeholder
    /// -- title
    /// 
    /// 
    /// </summary>
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngInputText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
  , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            return ngInputText(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngInputText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , object attributesObject)
        {
            IDictionary<string, object> htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(attributesObject);
            return ngInputText(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngInputText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            // use of the Insert extension to add key only when it does not exist - this allows the user
            // to override any attribute when the helper is used by specifiting htmlAttributes argument


            // for class attribute, we append the new class to any previously specified
            System.Linq.Expressions.MemberExpression memberExpression = expression.Body as System.Linq.Expressions.MemberExpression;
            if (memberExpression != null)
            {
                var member = memberExpression.Member;
                
                //Get display attribute first
                // if it defines a description, we should use that before using the older description attribute.
                DisplayAttribute da = memberExpression.Member
                                   .GetCustomAttributes(typeof(DisplayAttribute), false)
                                   .Cast<DisplayAttribute>()
                                   .SingleOrDefault();
                if (da != null)
                {
                    if (da.Description != null)
                    {
                        
                        htmlAttributes.Insert("title", da.Description);
                    }
                    if (da.Prompt != null)
                    {
                        htmlAttributes.Insert("placeholder", da.Prompt);
                    }
                }

                // similarly, StringLength should supercede MaxLength, so look for that first
                StringLengthAttribute sla = memberExpression.Member
                                   .GetCustomAttributes(typeof(StringLengthAttribute), false)
                                   .Cast<StringLengthAttribute>()
                                   .SingleOrDefault(); ;
                if (sla != null)
                {
                    if (sla.MinimumLength > 0)
                    {
                        htmlAttributes.Insert("minlength", sla.MinimumLength);
                    }
                    htmlAttributes.Insert("maxlength", sla.MaximumLength);
                }

                //iterate the CustomAttributes collection ...
                foreach (var attribute in memberExpression.Member.GetCustomAttributes(false))
                {

                    switch (attribute.GetType().Name)
                    {
                        case "MaxLengthAttribute":
                            MaxLengthAttribute mla = attribute as MaxLengthAttribute;
                            {
                                htmlAttributes.Insert("maxlength", mla.Length);
                            }
                            break;
                        case "RequiredAttribute":
                            // note "true" is a string, true -> True otherwise which doesn;t work on the javascript side
                            htmlAttributes.Insert("ng-required", "true");
                            break;
                        case "DescriptionAttribute":
                            System.ComponentModel.DescriptionAttribute dsa = attribute as DescriptionAttribute;
                            htmlAttributes.Insert("title", dsa.Description);
                            break;
                    }
                }
                htmlAttributes.Insert("ng-disabled", "!vm.isEditing");
                htmlAttributes.Insert("ng-model", "vm.model." + member.Name);
            }

            return System.Web.Mvc.Html.InputExtensions.TextBoxFor(htmlHelper, expression, htmlAttributes);
        }

        #region BindTo
        public static MvcHtmlString ngBindTo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            return ngBindTo(htmlHelper, expression, htmlAttributes, true);
        }
        public static MvcHtmlString ngBindTo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
    , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression, bool addClasses)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            return ngBindTo(htmlHelper, expression, htmlAttributes, addClasses);
        }
        public static MvcHtmlString ngBindTo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
    , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
    , IDictionary<string, object> htmlAttributes, bool addClasses)
        {
            getAttributes(htmlHelper.ViewData, expression, htmlAttributes, addClasses);
            return HelperBase.putAttributes(htmlAttributes);
        }

        #endregion

    }


}
