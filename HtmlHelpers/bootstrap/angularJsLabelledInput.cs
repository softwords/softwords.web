﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;


namespace Softwords.Web.HtmlHelpers.Bootstrap
{
    /// <summary>
    /// Provider for Mvc HtmlHelpers that will emit code marked up for AngularJs
    /// according to the standards of this framework:
    /// ie - the controllerAs is assumed to be vm
    /// - the data object is vm.model
    /// This will use any data annotations on the model to generate these tags:
    /// -- maxlength
    /// -- minlength
    /// -- required
    /// -- placeholder
    /// -- title
    /// 
    /// 
    /// </summary>
    public static partial class AngularJsHelper
    {
        public static MvcHtmlString ngLabelledText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
  , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            htmlAttributes.Add("class", "form-control");
            var label = System.Web.Mvc.Html.LabelExtensions.LabelFor(htmlHelper, expression, htmlAttributes);

            return ngLabelledText(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngLabelledText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , object attributesObject)
        {
            IDictionary<string, object> htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(attributesObject);
            return ngLabelledText(htmlHelper, expression, htmlAttributes);
        }

        public static MvcHtmlString ngLabelledText<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes)
        {
            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            // use of the Insert extension to add key only when it does not exist - this allows the user
            // to override any attribute when the helper is used by specifiting htmlAttributes argument


            getAttributes(htmlHelper.ViewData, expression, htmlAttributes, true);
            Dictionary<string, object> labelAttributes = new Dictionary<string, object>();
            labelAttributes.Add("class", "col-sm-4 control-label");
            var label = htmlHelper.LabelFor(expression, labelAttributes);

            var input = htmlHelper.TextBoxFor(expression, htmlAttributes);
            string html = @"<div class=""form-group"">"
                         + label.ToString()
                + @"<div class=""col-sm-8"">" + input.ToString()
                + @"</div>
                    </div>";
            return MvcHtmlString.Create(html);
        }
    }
}
