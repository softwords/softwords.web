﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;
using Softwords.Web.Models;

namespace Softwords.Web.HtmlHelpers.Material
{
    public static partial class AngularJsHelper
    {
        /// <summary>
        /// Generate input UI for a property, based on its data type and attributes
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">Linq expression defining the property</param>
        /// <returns></returns>
        public static MvcHtmlString ngInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            var label = System.Web.Mvc.Html.LabelExtensions.LabelFor(htmlHelper, expression, htmlAttributes);

            return ngInputFor(htmlHelper, expression, null);
        }

        /// <summary>
        /// Generate input UI for a property, based on its data type and attributes
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">Linq expression defining the property</param>
        /// <param name="attributesObject">additional / override attributes for the input element</param>
        /// <returns></returns>
        public static MvcHtmlString ngInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression
            , object attributesObject)
        {

            var metadata = HelperBase.GetMetadata(htmlHelper, expression, attributesObject);
            var displayname = metadata.DisplayName;
            var description = metadata.Description;

            var propertyInfo = HelperBase.getPropertyInfo(metadata);
            IDictionary<string, object> htmlAttributes = metadata.AdditionalValues;

            // first decide what type of ui control is Required. Attribute for this property
            // this may depend on the data type, as well as the attributes

            string controlTag = "input";
            Type targetType = HelperBase.getDataType(expression);
            string inputType = "text";
            string icon = string.Empty;
            string children = string.Empty;

            switch (Type.GetTypeCode(targetType))
            {
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    inputType = "number";
                        break;
                case TypeCode.DateTime:
                    controlTag = @"md-datepicker";
                    htmlAttributes.Add("md-current-view", "year");
                    break;
            }

            switch (metadata.DataTypeName)
            {
                case "EmailAddress":
                    inputType = "email";
                    icon = @"<md-icon class=""material-icons"">email</md-icon>";
                    break;
                case "Phone":
                    inputType = "tel";
                    string iconName = "phone";
                    if (propertyInfo.hasCustomAttribute<PhoneTypeAttribute>())
                    {
                        TypedPhoneType tpt = propertyInfo.getCustomAttribute<PhoneTypeAttribute>().PhoneType;
                        switch (tpt)
                        {
                            case TypedPhoneType.Cell:
                                iconName = "phone_android";
                                break;
                            case TypedPhoneType.Skype:
                                iconName = "skype";
                                break;
                        }
                    }
                    icon = String.Format(@"<md-icon class=""material-icons"">{0}</md-icon>", iconName);
                    break;
                case "MultilineText":
                    controlTag = "textarea";
                    // the getAttributes has already set up rows and md-maxlength, so that these are available
                    // to 
                    
                    break;
            };

            if (htmlAttributes.Keys.Contains("lkp"))
            {
                controlTag = "md-select";
                if (!metadata.IsRequired)
                {
                    children = @"<md-option md-option-empty ng-value=""""></md-option>";
                }
                children += @"
                    <md-option ng-repeat=""c in vm.lookups.cache['{0}']"" ng-value=""c.C"">{{{{c.N}}}}</md-option>";
                children = String.Format(children, htmlAttributes["lkp"]);
                //htmlAttributes.Add("material", "true");
            }

            if (controlTag == "input")
            {
                // append the input Type
                controlTag = String.Format(@"input type=""{0}""", inputType);
            }

            string inputstr = String.Empty;
            if (children == string.Empty)
            {
                inputstr = "{2}<{0} {1} />";
            } else
            {
                inputstr = "{2}<{0} {1}>{3}</{0}>";
            }


            inputstr = String.Format(inputstr, controlTag, HelperBase.putAttributes(htmlAttributes), icon, children);
            return MvcHtmlString.Create(inputstr);
        }

        #region mdContainerFor

        /// <summary>
        /// Contruct an md-input-container for the field, including an input and error block
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">linq expression defining the field</param>
        /// <returns>html string</returns>
        public static MvcHtmlString mdContainerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            // if its an email or phone number, get an icon for it

            return mdContainerFor(htmlHelper, expression, null, true, false);
        }

        /// <summary>
        /// Contruct an md-input-container for the field, including an input and error block
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">linq expression defining the field</param>
        /// <param name="containerAttributes">attributes to add to the container element</param>
        /// <returns>html string</returns>
        public static MvcHtmlString mdContainerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, object containerAttributes)
        {
            // if its an email or phone number, get an icon for it

            return mdContainerFor(htmlHelper, expression, containerAttributes, true, false);
        }

        /// <summary>
        /// Contruct an md-input-container for the field, optionally including error block or hint block
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="withErrors">true if error blaock should be included</param>
        /// <param name="withHints">true if hint block should be included></param>
        /// <returns>html string</returns>
        public static MvcHtmlString mdContainerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
, System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression, bool withErrors, bool withHints)
        {
            return mdContainerFor(htmlHelper, expression, null, withErrors, withHints);
        }

        /// <summary>
        /// Contruct an md-input-container for the field, optionally including error block or hint block
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression"></param>
        /// <param name="containerAttributes">attributes to add to the container element</param>
        /// <param name="withErrors">true if error blaock should be included</param>
        /// <param name="withHints">true if hint block should be included></param>
        /// <returns>html string</returns>
        public static MvcHtmlString mdContainerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, object containerAttributes, bool withErrors, bool withHints)
        {
            MvcHtmlString s = ngInputFor(htmlHelper, expression);
            string errorString = string.Empty;
            if (withHints)
            {
                errorString = AngularErrorsFor.ngErrorMessages(htmlHelper, expression).ToString();
            } else if (withErrors)
            {
                errorString = AngularErrorsFor.ngErrorMessages(htmlHelper, expression).ToString();
            }
            string conAttr = String.Empty;
            if (containerAttributes != null)
            {
                var dic = containerAttributes.ToDictionary(true);
                conAttr = HelperBase.putAttributes(dic).ToString();
            }
            string html = @"<md-input-container {2}>{0}{1}</md-input-container>";

            return MvcHtmlString.Create(String.Format(html, s.ToString(), errorString, conAttr));
        }

        #endregion
    }
}
