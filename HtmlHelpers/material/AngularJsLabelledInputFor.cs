﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc.Html;

namespace Softwords.Web.HtmlHelpers.Material

{
    public static partial class AngularJsHelper
    {

        public static MvcHtmlString ngPromptInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression, string prompttext)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            
            MvcHtmlString controlstr = ngInputFor(htmlHelper, expression, htmlAttributes);
            // get the string for the label
            Dictionary<string, object> labelAttributes = new Dictionary<string, object>();
            // im aterial, the label is subsumed into the input tag...
            //labelAttributes.Add("flex-gt-md", "40");

            var label = htmlHelper.LabelFor(expression, labelAttributes);
            // return them in an md-input-container
            string html = @"<md-input-container><p>{0}</p>{1}</md-input-container>";
            html = string.Format(html, prompttext, controlstr.ToString());
            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString ngLabelledInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
, System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression)
        {
            IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
            // first get the string representing the label
            var label = System.Web.Mvc.Html.LabelExtensions.LabelFor(htmlHelper, expression, htmlAttributes);

            return ngLabelledInputFor(htmlHelper, expression, htmlAttributes);
        }
        public static MvcHtmlString ngLabelledInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression
            , IDictionary<string, object> htmlAttributes)
        {
            // get the string for the input control
            MvcHtmlString controlstr = ngInputFor(htmlHelper, expression, htmlAttributes);
            // get the string for the label
            Dictionary<string, object> labelAttributes = new Dictionary<string, object>();
            // im aterial, the label is subsumed into the input tag...
            //labelAttributes.Add("flex-gt-md", "40");

            var label = htmlHelper.LabelFor(expression, labelAttributes);
            // return them in an md-input-container
            string html = @"<md-input-container>"    // layout-gt-md=""row"" layout-padding>"
                         + label.ToString()
                //+ @"<div flex>"
                    + controlstr.ToString()
                //+ @"</div>
                  + @"</md-input-container>";
            return MvcHtmlString.Create(html);
        }
    }
}
