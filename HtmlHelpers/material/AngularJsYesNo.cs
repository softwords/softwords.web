﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel; // use the older annotations to get to DescriptionAttribute
using System.ComponentModel.DataAnnotations;
using Softwords.Web.HtmlHelpers;
using System.Linq.Expressions;              // supplies type Expression

namespace Softwords.Web.HtmlHelpers.Material
{
    /// <summary>
    /// Provider for Mvc HtmlHelpers that will emit code marked up for AngularJs
    /// according to the standards of this framework:
    /// ie - the controllerAs is assumed to be vm
    /// - the data object is vm.model
    /// This will use any data annotations on the model to generate these tags:
    /// -- maxlength
    /// -- minlength
    /// -- required
    /// -- placeholder
    /// -- title
    /// 
    /// 
    /// </summary>
    public static partial class AngularJsHelper
    {
        /// <summary>
        /// Display a pair of radio buttons, representing a Yes/No option
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">the field</param>
        /// <param name="layoutstring">"row", column" or set of md-layout tags e.g. layout="column" layout-gt-sm="row"</param>
        /// <param name="values">'YN' - use Y and N, '10' - use 1 and 0</param>
        /// <returns></returns>

        /// <summary>
        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, object attributesObject, string layoutstring, string values)
        {
            ModelMetadata metadata = HelperBase.GetMetadata(htmlHelper, expression, attributesObject);
            return ngYesNo(metadata, layoutstring, values);
        }

        private static MvcHtmlString ngYesNo(ModelMetadata metadata, string layoutstring, string values)
        {
            // track focus on this group, to highlight the label, which comes equipped with ng-class to add md-input-focused class
            metadata.AdditionalValues.Insert("ng-focus", String.Format("vm.focus('{0}')", metadata.PropertyName));
            metadata.AdditionalValues.Insert("ng-blur", String.Format("vm.blur('{0}')", metadata.PropertyName));

            var htmlAttributes = metadata.AdditionalValues;

            string y = "1";
            string n = "0";

            switch (values.ToUpper())
            {
                case "YN":
                    y = "Y";
                    n = "N";
                    break;
                case "10":
                    y = "1";
                    n = "0";
                    break;
            }

            switch (layoutstring) {
                case "row":
                case "column":
                    layoutstring = string.Format(@"layout=""{0}""", layoutstring);
                    break;
                // use these arguments to indicate the break between column and row layouts
                case "gt-xs":
                case "gt-sm":
                case "gt-md":
                case "gt-lg":
                    layoutstring = string.Format(@"layout=""column"" layout-{0}=""row""", layoutstring);
                    break;
            }
            string template = @"<md-radio-group {1} {0}>
                 <md-radio-button value=""{2}"" aria-label=""Yes"">Yes</md-radio-button>
                 <md-radio-button value=""{3}"" aria-label=""No"">No</md-radio-button>
            </md-radio-group>";

            string attrs = HelperBase.putAttributes(htmlAttributes).ToString();

            string tag = String.Format(template, attrs, layoutstring, y, n);
            return MvcHtmlString.Create(tag);
        }
        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression)
        {
            string defaultlayout = @"gt-sm";
            return ngYesNo(htmlHelper, expression, defaultlayout);
        }


        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, object attributes)
        {
            string defaultlayout = @"gt-sm";
            return ngYesNo(htmlHelper, expression, attributes, defaultlayout,"10");
        }

        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, string layoutstring)
        {
            
            return ngYesNo(htmlHelper, expression, null, layoutstring, "10" );
        }

        public static MvcHtmlString ngYesNo<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, object attributes, string layoutstring)
        {

            return ngYesNo(htmlHelper, expression, attributes, layoutstring, "10");
        }

        #region YesNo with Prompt

        /// <summary>
        /// A specialized version of Yes/No radio buttons, that includes a text prompt. Use for survey type questions
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="htmlHelper"></param>
        /// <param name="expression">linq expression defining the property</param>
        /// <param name="prompt">prompt</param>
        /// <param name="attributes">additional attributes</param>
        /// <returns></returns>
        public static MvcHtmlString ngYesNoPrompt<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, string prompt, object attributesObject)
        {
            string defaultlayout = @"row";
            return ngYesNoPrompt(htmlHelper, expression, prompt, attributesObject, defaultlayout);
        }

        public static MvcHtmlString ngYesNoPrompt<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
     , Expression<Func<TModel, TProperty>> expression, string prompt, object attributesObject, string layout)
        {
            
            ModelMetadata metadata = HelperBase.GetMetadata(htmlHelper, expression, attributesObject);

            var propertyName = metadata.PropertyName;
            string labelName = String.Format(@"{0}_prompt", propertyName);

            metadata.AdditionalValues.Insert("aria-labelledby", labelName);

            string label = ngPrompt(metadata, prompt, null).ToString();
            string yn = ngYesNo(metadata, layout, "10").ToString();
            string output = @"<div>{0} 
                            {1}</div>";
            return MvcHtmlString.Create(String.Format(output, label, yn));
        }
        public static MvcHtmlString ngYesNoPrompt<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, string prompt)
        {
            object attributes = null;
            return ngYesNoPrompt(htmlHelper, expression, prompt, attributes);
        }
        #endregion

        #region prompt
        public static MvcHtmlString ngPrompt<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, string prompt)
        {
            object attributes = null;

            return ngPrompt(htmlHelper, expression, prompt, attributes);
        }
        public static MvcHtmlString ngPrompt<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper
            , Expression<Func<TModel, TProperty>> expression, string prompt, object attributes)
        {
            ModelMetadata metadata = HelperBase.GetMetadata(htmlHelper, expression, null);
            return ngPrompt(metadata, prompt, attributes);
        }

         
        internal static MvcHtmlString ngPrompt(ModelMetadata metadata, string prompt, object attributes)
        {
            IDictionary<string, object> customAttributes = new Dictionary<string, object>();
            string customAttributeString = String.Empty;
            if (attributes != null)
            {
                customAttributes = attributes.ToDictionary(true);
            }
            if (metadata.IsRequired)
            {
                customAttributes.Insert("class", "prompt md-no-float md-container-ignore md-required");
                string ngc = String.Format(@"{{'disabled':!vm.isEditing, 
                                        'md-input-focused': vm.hasFocus('{0}'),
                                         'invalid': vm.hasError('{0}') && vm.showErrors('{0}')}}",
                                metadata.PropertyName);
                customAttributes.Insert("ng-class", ngc);
            }
            else
            {
                customAttributes.Insert("class", "prompt md-no-float md-container-ignore");
                string ngc = String.Format(@"{{'disabled':!vm.isEditing, 
                                        'md-required': vm.isRequired('{0}'),
                                        'md-input-focused': vm.hasFocus('{0}') }}",
                                metadata.PropertyName);
                customAttributes.Insert("ng-class",ngc);
            }
            customAttributeString = HelperBase.putAttributes(customAttributes).ToString();
            string labelName = String.Format(@"{0}_prompt", metadata.PropertyName);
            // note wthe double {{ }} here is not an angular tag! it is an escape seq for String.Format
            string output = @"<label id=""{0}"" for=""{1}"" 
                           
                            {3}>{2}</label>";

            output = String.Format(output, labelName, metadata.PropertyName, prompt, customAttributeString);
            return MvcHtmlString.Create(output);
        }
        public static MvcHtmlString ngPrompt(this HtmlHelper htmlHelper
            , string forProperty, string prompt)
        {
            object attributes = null;
            return ngPrompt(htmlHelper, forProperty, prompt, attributes);
        }
        public static MvcHtmlString ngPrompt(this HtmlHelper htmlHelper
            , string forProperty, string prompt, object attributes)
        {
            string labelName = String.Format(@"{0}_prompt", forProperty);
            IDictionary<string, object> customAttributes = new Dictionary<string, object>();
            string customAttributeString = String.Empty;
            if (attributes != null)
            {
                customAttributes = attributes.ToDictionary(true);
                customAttributeString = HelperBase.putAttributes(customAttributes).ToString();
            }
            // note wthe double {{ }} here is not an angular tag! it is an escape seq for String.Format
            string output = @"<label id=""{0}"" for=""{1}"" 
                            class=""prompt md-no-float md-container-ignore"" 
                            ng-class=""{{'disabled': !vm.isEditing, 'md-required': vm.isRequired('{1}')}}""
                            {3}>{2}</label>";

            output = String.Format(output, labelName, forProperty, prompt, customAttributeString);
            return MvcHtmlString.Create(output);
        }
        #endregion


    }
}