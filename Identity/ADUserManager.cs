﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Softwords.Web.Models;
using ad = System.DirectoryServices.AccountManagement;

namespace Softwords.Web
{
    /// <summary>
    ///  Searching for user in Active Directory
    /// </summary>
    class ADUserManager
    {
        private ApplicationUserManager appUserManager;
        public ADUserManager(ApplicationUserManager appUserMgr)
        {
            appUserManager = appUserMgr;
        }
        public async Task<ApplicationUser> FindAsync(string userName, string password)
        {
            ApplicationUser user = null;
            // see if this is an active directory account
            // but only if its not an email
            if (userName.IndexOf("@") == -1)
            {
                // create a "principal context" - e.g. your domain (could be machine, too)
                //using(ad.PrincipalContext pc = new ad.PrincipalContext(ad.ContextType.Machine, "keenyah"))
                //
                // save us some web.config vars
                ad.PrincipalContext pc = getPrincipalContext();
                using (pc)
                {
                    // validate the credentials
                    bool isValid = pc.ValidateCredentials(userName, password);

                    if (isValid)
                    {
                        // create a user now

                        user = await makeAdUser(userName);
                    }
                }
            }
            return user;
        }

        async Task<ApplicationUser> makeAdUser(string userName)
        {
            ApplicationUser user = null;
            // to conform to the identity built-in behaviour, the ApplicationUser returned here must have all its claims
            // and (specifically, becuase they are claims) roles in place

            // So the strategy is to collected all the Ad group memberships
            // add as roles any that are in the Identity list of roles ( so we don;t end up with lots of unwanted)
            // Next look through ASPNET users for each role, to find a default profile
            // add the claims and roles - and menu - associated to this profile
            // if there is none, use DOMAIN_USER
            // the prupose of this class is to shoe-horn some values in to the claims identity
            // the exposed properties of IIdentity are all readonly
            ad.PrincipalContext cxt = getPrincipalContext();
            ad.UserPrincipal pu = ad.UserPrincipal.FindByIdentity(cxt, userName);
            if (pu == null)
                return null;
            // System.Security.Principal.WindowsIdentity wi = new System.Security.Principal.WindowsIdentity(userName);
            // System.Security.Principal.WindowsPrincipal wp = new System.Security.Principal.WindowsPrincipal(wi);

            // if found - grab its groups
            //ad.PrincipalSearchResult<ad.Principal> groups = 
            List<string> grplist = pu.GetGroups(cxt).Select(o => o.Name).ToList();
            // iterate over all groups
            // to find at least one AspNetUser representing a group this user belongs to
            // Pass the roles to the permissionHash stored proc
            foreach (string p in grplist)
            {
                user = await appUserManager.FindByNameAsync(p);
                if (user != null)
                {
                    break;
                }
            }

            if (user == null)
            {
                // use tyeh default account for DOMAIN_USER
                user = await appUserManager.FindByNameAsync("DOMAIN_USER");
            }
            // no default in AspNetUsers - cannot log in with a domai account
            if (user == null)
                return null;
            user.Proxify(userName);


            // go through the groups again and add these as groups
            // they will get turned into Claims later
            foreach (string p in grplist)
            {
                // make sure to add only group principals
                    //if (rm.FindByName(p.Name) != null)
                    //{
                        user.Groups.Add(p);
                    //}

                
            }
            // Use this list of AD groups to get all the permissions in effect for this user
            await user.getEffectivePermissions();
            await user.getEffectiveRoles();
            await user.getEffectiveMenuKey();
            // this function handles machine or domain principal creation


            NavigationManager nm = new NavigationManager();
            user.ClientMenu = await nm.getNavigationMenu(user.MenuKey);

            return user;

        }
        /// <summary>
        /// get the principal context for verifying a windows user login
        /// Note that this is the context of the 'active user' ( ie the user running the apppool)
        /// so, to verify against the AD, this apppool account should be an AD account too.
        /// </summary>
        /// <returns></returns>
        public ad.PrincipalContext getPrincipalContext()
        {
            return new ad.PrincipalContext(
                    (string.Equals(System.Environment.UserDomainName, System.Environment.MachineName, System.StringComparison.OrdinalIgnoreCase) ?
                           ad.ContextType.Machine : ad.ContextType.Domain),
                    System.Environment.UserDomainName);
        }
    }
}
