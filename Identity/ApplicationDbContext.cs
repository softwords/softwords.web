﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Softwords.Web
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        // add this definition to get to the menus


        public async Task<string> getNavItems(string menuKey)
        {
            var param = new System.Data.SqlClient.SqlParameter
            {
                ParameterName = "menuKey",
                Value = menuKey
            };
            var result = await this.Database.SqlQuery<NavData>("exec getNavigationXml @menuKey", param).ToListAsync();
            return result[0].navs;

        }

        public async Task<string> getEffectivePermissions(List<string> grps)
        {
            List<SqlParameter> gpparams = getGroupParams(grps);
            string cmd = "exec dbo.getEffectivePermissions " + 
                " @user0, @user1, @user2, @user3, @user4, @user5, @user6, @user7, @user8, @user9, " +
                " @user10, @user11, @user12, @user13, @user14, @user15, @user16, @user17, @user18, @user19";
            var result = await this.Database.SqlQuery<EffectivePermissionData>(cmd,
                gpparams.ToArray()).ToListAsync();
            return result[0].Permission;

        }
        public async Task<List<string>> getEffectiveRoles(List<string> grps)
        {
            List<SqlParameter> gpparams = getGroupParams(grps);
            string cmd = "exec dbo.getEffectiveRoles " +
                " @user0, @user1, @user2, @user3, @user4, @user5, @user6, @user7, @user8, @user9, " +
                " @user10, @user11, @user12, @user13, @user14, @user15, @user16, @user17, @user18, @user19";
            var result = await this.Database.SqlQuery<EffectiveRoleData>(cmd,
                gpparams.ToArray()).ToListAsync();
            var roles = new List<string>();
            foreach(EffectiveRoleData r in result)
            {
                roles.Add(r.Role);
            }
            return roles;

        }
        public async Task<string> getEffectiveMenuKey(List<string> grps)
        {
            List<SqlParameter> gpparams = getGroupParams(grps);
            string cmd = "exec dbo.getEffectiveMenuKey " +
                " @user0, @user1, @user2, @user3, @user4, @user5, @user6, @user7, @user8, @user9, " +
                " @user10, @user11, @user12, @user13, @user14, @user15, @user16, @user17, @user18, @user19";

            var result = await this.Database.SqlQuery<EffectiveMenuKeyData>(cmd,
                gpparams.ToArray()).ToListAsync();
            var roles = new List<string>();
            return result[0].MenuKey;

        }
        private List<SqlParameter> getGroupParams(List<string> grps)
        {
            SqlParameter pg = null;
            List<SqlParameter> gpparams = new List<SqlParameter>();

            for (int i = 0; i < 20; i++)
            {
                pg = new System.Data.SqlClient.SqlParameter(string.Format("user{0}", i), System.Data.SqlDbType.NVarChar, 256);
                if (i < grps.Count)
                {
                    pg.Value = grps[i];
                }
                else
                {
                    pg.Value = DBNull.Value;
                }
                gpparams.Add(pg);
            }
            return gpparams;
        }
        public System.Data.Entity.DbSet<Navigation> Navigation { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
