﻿using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class,
    // please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        private List<string> grps;
        private List<string> roles;
        public ApplicationUser()
        {
            roles = new List<string>();
            grps = new List<string>();
            PermissionHash = "00000000000";           // nada " @/OOOO OO@"; // testing
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            // note any claims in the AspNetUSerClaims table are already there
            // this will pick up anything added for an AD user
            foreach(string grp in EffectiveRoles)
            {
                userIdentity.AddClaim(new Claim(ClaimTypes.Role, grp));
            }
            if (Proxy != null)
            {
                userIdentity.AddClaim(new Claim("Proxy", Proxy));
            }

            if (userIdentity.FindFirst("Permission") == null)
            {
                userIdentity.AddClaim(new Claim("Permission", PermissionHash));
            }

            return userIdentity;
        }
        public string Country { get; set; }
        // defines the menu to display for this user - this is a key in the Navigiations table
        public string MenuKey { get; set; }

        // attribute tells ef this property does not come from the database
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string Proxy { get; set; }          // return the name of the actual ASP.NET Identity user if the user identity was validated from the domain

        public void Proxify( string userName)
        {
            // configure  the user
            Proxy = UserName;
            if (!userName.StartsWith(System.Environment.UserDomainName + @"\"))
                userName = System.Environment.UserDomainName + @"\" + userName;
            Email = userName;

            UserName = Email;
        }

        // attribute tells ef this property does not come from the database
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string ClientMenu { get; set; }

        // represents fine-grained permissions on the database
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string PermissionHash { get; set; }

        public List<string> EffectiveRoles
        {
            get { return roles; }
        }
        // place to put validated AD groups
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public List<string> Groups
        {
            get { return grps; }
        }

        public async Task getEffectivePermissions()
        {
            using (ApplicationDbContext cxt = new ApplicationDbContext())
            {
                PermissionHash = await cxt.getEffectivePermissions(Groups);
            }
        }

        public async Task getEffectiveRoles()
        {
            using (ApplicationDbContext cxt = new ApplicationDbContext())
            {
                roles = await cxt.getEffectiveRoles(Groups);
            }
        }
        public async Task getEffectiveMenuKey()
        {
            using (ApplicationDbContext cxt = new ApplicationDbContext())
            {
                MenuKey = await cxt.getEffectiveMenuKey(Groups);
            }
        }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("MenuKey")]
        public virtual Navigation Navigation { get; set; }
    }

    // here we add an object to represent the menus
    //cf http://blogs.msdn.com/b/webdev/archive/2013/10/16/customizing-profile-information-in-asp-net-identity-in-vs-2013-templates.aspx
    //

    // note that to get any of this to go back to the client with the token at login
    // you have to set up
    // AuthenticationProperties CreateProperties()
    // in ApplicationOAuthProvider.cs

    // by default the table name is plurakl of object name
    [System.ComponentModel.DataAnnotations.Schema.Table("Navigation")]
    public class Navigation
    {
        [System.ComponentModel.DataAnnotations.Key]
        public string id { get; set; }
        public string homestate { get; set; }
        public string label { get; set; }


        private string menu_;

        public async Task<string> menu()
        {

            if (menu_ == null)
            {
                //XDocument navs =
                string navItems;
                using (ApplicationDbContext cxt = new ApplicationDbContext())
                {
                    navItems = await cxt.getNavItems(id);
                }
                System.Xml.Linq.XDocument x = System.Xml.Linq.XDocument.Parse(navItems);

                NavigationManager nm = new NavigationManager();
                System.Collections.Generic.List<NavigationItem> navs = nm.Build(x);
                menu_ = Newtonsoft.Json.JsonConvert.SerializeObject(navs);
            }
            return menu_;

        }

    }
}
