﻿using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Softwords.Web.Models;
using ad = System.DirectoryServices.AccountManagement;
using System.Configuration;



// this boilerplate comes out of the local App_Starand is referenced from here,
// picking up the Softwords.Web.Models versions of ApplicationUser
// and ApplicationDbContext

namespace Softwords.Web
{
    // Configure the application user manager used in this application.
    //  UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {

        }

        private ApplicationDbContext cxt = null;
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames

            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 8,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            manager.EmailService = new IdentityEmailService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {

                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    // increase to 3 days
                    TokenLifespan = System.TimeSpan.FromDays(3)
                };
                
            }

            // save the ApplicationDbcontext for later
            // this link has interesting things to say about these dependencies
            //https://aspnetidentity.codeplex.com/discussions/550953

            manager.cxt = context.Get<ApplicationDbContext>();

            // set up some stuff for account lockout
            // first if lockput is enabled by default?

            manager.UserLockoutEnabledByDefault =
                (ConfigurationManager.AppSettings["UserLockoutEnabledByDefault"] == "true" ? true: false);
            int minutes = 
                System.Convert.ToInt32(ConfigurationManager.AppSettings["DefaultAccountLockoutTimeSpan"] ?? "5");
            manager.DefaultAccountLockoutTimeSpan = new System.TimeSpan(0, minutes, 0);
            manager.MaxFailedAccessAttemptsBeforeLockout = 
                System.Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]??"10");

            return manager;

        }


        public override async Task<ApplicationUser> FindAsync(string userName, string password)
        {

            ApplicationUser user = await base.FindAsync(userName, password);

            if (user != null)
            {
                user.Proxy = null;
                // have to put this somewhere to get the ClientMenu set - for domain users, its in makeAdUser
                // needs a rethink....
                NavigationManager nm = new NavigationManager();
                user.ClientMenu = await nm.getNavigationMenu(user.MenuKey);
            }
            return user;
        }
        public async override Task<ApplicationUser> FindByNameAsync(string userName)
        {
            ApplicationUser user = await base.FindByNameAsync(userName);
            if (user != null)
            {
                user.Proxy = null;
            }
            // second attempt - try the active directory
            //////var pc = getPrincipalContext();
            //////ad.UserPrincipal p = ad.UserPrincipal.FindByIdentity(pc, userName);
            //////if (p != null)
            //////{
            //////    user = await makeAdUser(userName);
            //////}
            return user;
        }



        public ApplicationUser FindByName(string userName)
        {
            // the extension does call the async version
            ApplicationUser user = UserManagerExtensions.FindByName(this, userName);

            return user;
        }

        public ApplicationUser FindById(string userId)
        {
            // the extension does call the async version
            ApplicationUser user = UserManagerExtensions.FindById(this, userId);

            return user;
        }

        public IList<string> GetRoles(string userId)
        {
            IList<string> ls = UserManagerExtensions.GetRoles(this, userId);
            return ls;
        }
        public async override Task<IList<string>> GetRolesAsync(string userId)
        {
            return await base.GetRolesAsync(userId);
        }
    }
}
