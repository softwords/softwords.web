﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web.Models;

namespace Softwords.Web.Identity
{
    /// <summary>
    /// Interface to define security operations that may be performaned on an AD account, or ASP.NET API account
    /// </summary>
    interface ISecurityOps
    {
        Task<IdentityResult> changePassword(ChangePasswordBindingModel model);
    }
}
