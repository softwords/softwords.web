﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Security.Claims;

namespace Softwords.Web.Identity
{
    // class principally for dealing with "filter" claims
    // ie segmenting data access based on some filter
    // filterDistrict, filterSchoolNo, filterAwardType....
    // provides extension methods for extracting these
    public static class IdentityExtensions
    {
        const string filterTag = "filter";
        public static bool HasFilters(this IPrincipal user)
        {
            var claims = user.FilterClaims();
            return (claims.Count() > 0);
        }

        public static IEnumerable<Claim> AllClaims(this IPrincipal user)
        {
            ClaimsIdentity identity = user.Identity as ClaimsIdentity;
            return identity.Claims;
        }
        public static IEnumerable<Claim> FilterClaims(this IPrincipal user)
        {
            ClaimsIdentity identity = user.Identity as ClaimsIdentity;
            return (from c in identity.Claims where c.Type.StartsWith(filterTag) select c);
        }
        public static Dictionary<string,string> Filters(this IPrincipal user)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            foreach (Claim c in user.FilterClaims())
            {
                string f = c.Type.Substring(filterTag.Length);
                dict.Add(f, c.Value);
            }
            return dict;
        }
    }
}
