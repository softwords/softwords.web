﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;

namespace Softwords.Web
{

    public enum PermissionAccess
    {
        Admin = 32,
        Ops = 16,
        WriteX = 8,
        Write = 4,
        ReadX = 2,
        Read = 1
    }
    public class PermissionAttribute : AuthorizationFilterAttribute
    {
        public int Topic { get; set; }
        public PermissionAccess Access { get; set; }

        public PermissionAttribute() : base() { }

        public PermissionAttribute(int topic, PermissionAccess access) : base()
        {
            this.Topic = topic;
            this.Access = access;
        }

        // http://stackoverflow.com/questions/79126/create-generic-method-constraining-t-to-an-enum
        public static string Name<e>(e topic, PermissionAccess access) where e : struct, IConvertible
        {
            // return Enum.GetName(typeof(e), topic) + access.ToString();
            return topic.ToString() + access.ToString();
        }
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {

            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (!principal.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                return Task.FromResult<object>(null);
            }

            if (!(principal.HasClaim(x => x.Type == "Permission")))
            {
                throw new PermissionException(Topic, Access);
            }

            string permissionHash = principal.FindFirst("Permission").Value;

            // find the character offset for the permission area
            // 3 pieces need to be kept in synch on this array
            // 1) here
            // 2) in the stored proc permissionHash
            // 3) in the javascript configPermissions

            if (Topic < 0)
            {
                throw new PermissionException(Topic, Access);
            }

            byte charvalue = Encoding.ASCII.GetBytes(permissionHash)[Topic];
            PermissionAccess topicHash = (PermissionAccess)(charvalue - 48);            // remove the offset which is there to make a printable string

            if (!topicHash.HasFlag(Access))
            {
                throw new PermissionException(Topic, Access, topicHash);
            }

            //User has the required permission, complete execution
            return Task.FromResult<object>(null);
        }
    }
}
