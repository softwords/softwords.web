﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Softwords.Web
{
    // extension method to drill into Permissions from a Claims Identity
    public static class PermissionExtensions
    {
        public static bool hasPermission(this ClaimsIdentity ci, int topic, PermissionAccess access)
        {
            if (!(ci.HasClaim(x => x.Type == "Permission")))
            {
                return false;
            }

            string permissionHash = ci.FindFirst("Permission").Value;

            // find the character offset for the permission area
            // 3 pieces need to be kept in synch on this array
            // 1) here
            // 2) in the stored proc permissionHash
            // 3) in the javascript configPermissions

            if (topic < 0)
            {
               return false;
            }

            byte charvalue = Encoding.ASCII.GetBytes(permissionHash)[topic];
            PermissionAccess topicHash = (PermissionAccess)(charvalue - 48);            // remove the offset which is there to make a printable string

            return topicHash.HasFlag(access);
        }
    }
}
