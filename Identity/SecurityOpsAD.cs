﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web.Models;
using ad = System.DirectoryServices.AccountManagement;


namespace Softwords.Web.Identity
{
    class SecurityOpsAD : ISecurityOps
    {
        ApplicationUserManager userManager;
        System.Security.Claims.ClaimsPrincipal principal;
        public SecurityOpsAD(ApplicationUserManager userManager, System.Security.Claims.ClaimsPrincipal principal)
        {
            this.principal = principal;
            this.userManager = userManager;
        }
        public Task<IdentityResult> changePassword(ChangePasswordBindingModel model)
        {
            
            var errors = new List<string>();
            {
                using (var principalContext = AcquirePrincipalContext())
                {
                    string name = principal.Identity.Name;
                    var userPrincipal = AcquireUserPricipal(principalContext, name );

                    // Check if the user principal exists
                    if (userPrincipal == null)
                    {
                        ////errors.Add(new ApiErrorItem() { ErrorType = ApiErrorType.GeneralFailure, ErrorCode = ApiErrorCode.UserNotFound });
                        ////Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        ////return Json(result);
                        throw new Exception("User not found");    // Settings.ClientSettings.Alerts.ErrorPasswordChangeNotAllowed);
                    }

                    // Check if password change is allowed
                    if (userPrincipal.UserCannotChangePassword)
                    {
                        throw new Exception("Password change not allowed");    // Settings.ClientSettings.Alerts.ErrorPasswordChangeNotAllowed);
                    }

                    // Validate user credentials
                    if (principalContext.ValidateCredentials(userPrincipal.SamAccountName, model.OldPassword) == false)
                    {
                        throw new Exception("Invalid credentials: " + name + " " + userPrincipal.SamAccountName +  " " + model.OldPassword);             // Settings.ClientSettings.Alerts.ErrorInvalidCredentials);
                    }

                    // Change the password via 2 different methods. Try SetPassword if ChangePassword fails.
                    try
                    {
                        // Try by regular ChangePassword method
                        userPrincipal.ChangePassword(model.OldPassword, model.NewPassword);
                    }
                    catch (Exception ex2)
                    {
                        // If the previous attempt failed, use the SetPassword method.
                        ////if (Settings.PasswordChangeOptions.UseAutomaticContext == false)
                        ////    userPrincipal.SetPassword(model.NewPassword);
                        ////else
                            throw ex2;
                    }

                    // userPrincipal.Save();
                    
                    return Task.FromResult<IdentityResult>(IdentityResult.Success);
                }
            }
        }
        private static ad.UserPrincipal AcquireUserPricipal(ad.PrincipalContext context, string username)
        {
            return ad.UserPrincipal.FindByIdentity(context, username);
        }

        private ad.PrincipalContext AcquirePrincipalContext()
        {
            // PrincipalContext principalContext = null;
            //if (Settings.PasswordChangeOptions.UseAutomaticContext)
            //{
            //    principalContext = new PrincipalContext(ContextType.Domain);
            //}
            //else
            //{
            //    principalContext = new PrincipalContext(
            //        ContextType.Domain,
            //        $"{Settings.PasswordChangeOptions.LdapHostname}:{Settings.PasswordChangeOptions.LdapPort.ToString()}",
            //        Settings.PasswordChangeOptions.LdapUsername,
            //        Settings.PasswordChangeOptions.LdapPassword);
            //}

            return new ad.PrincipalContext(
                   (string.Equals(System.Environment.UserDomainName, System.Environment.MachineName, System.StringComparison.OrdinalIgnoreCase) ?
                          ad.ContextType.Machine : ad.ContextType.Domain),
                   System.Environment.UserDomainName);
        }
    }
}
