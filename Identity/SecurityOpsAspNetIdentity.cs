﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web.Models;

namespace Softwords.Web.Identity
{
    class SecurityOpsAspNetIdentity:ISecurityOps
    {
        ApplicationUserManager userManager;
        System.Security.Claims.ClaimsPrincipal principal;
        public SecurityOpsAspNetIdentity(ApplicationUserManager userManager, System.Security.Claims.ClaimsPrincipal principal)
        {
            this.principal = principal;
            this.userManager = userManager;
        }
        public async Task<IdentityResult> changePassword(ChangePasswordBindingModel model)
        {
            IdentityResult result = await userManager.ChangePasswordAsync(principal.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
			if (result.Succeeded)
			{
				// send the confirmation email
				ApplicationUser user = await userManager.FindByIdAsync(principal.Identity.GetUserId());
				await new Email.EmailGenerator(userManager).sendConfirmPasswordChangeEmail(user);
			}

			return result;
			
		}
    }
}
