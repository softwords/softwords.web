﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using Softwords.Web;


namespace Softwords.Web
{
    public enum TableOperation
    {
        Create,
        Read,
        Update,
        Delete
    }

    public interface ITableMaintenancePermissions
    {
        bool checkPermission(Softwords.Web.TableOperation operation, ClaimsPrincipal principal);
    }


    public class TablePermissionAttribute : AuthorizationFilterAttribute
    {
        public int Topic { get; set; }
        public TableOperation Operation { get; set; }

        public TablePermissionAttribute() : base() { }

        public TablePermissionAttribute(TableOperation operation) : base()
        {
            this.Operation = operation;
        }

        // http://stackoverflow.com/questions/79126/create-generic-method-constraining-t-to-an-enum

        public static string Name<e>(e topic, TableOperation operation) where e : struct, IConvertible
        {
            // return Enum.GetName(typeof(e), topic) + access.ToString();
            return topic.ToString() + operation.ToString();
        }
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {

            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (!principal.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                return Task.FromResult<object>(null);
            }

            ITableMaintenancePermissions controller = actionContext.ControllerContext.Controller as ITableMaintenancePermissions;
            
            if (!controller.checkPermission(Operation, principal))
            {
                throw new UnauthorizedAccessException();
            }


            //User has the required permission, complete execution
            return Task.FromResult<object>(null);
        }
    }
}

