﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Data;
using Softwords.Web.Utilities;

/// <summary>
/// Serialize a big dataset to json in a reduced form
/// Here, every row is represented as an array of its properties
/// and the column names are supplied independently - just once
/// so it is akin to a csv file
/// cf the Dataset object in the (long-defunct) miso project
/// https://bocoup.com/blog/introducing-miso-project-and-dataset-library
/// This hierarchical framework adapted from an old article of rick strahl
/// https://weblog.west-wind.com/posts/2008/Sep/03/DataTable-JSON-Serialization-in-JSONNET-and-JavaScriptSerializer
/// </summary>
namespace Softwords.Web.Json
{
    public static class DatasetSerializer
    {
        public static Stream toStream(DataTable dt)
		{
			return toStream((object)dt);
		}
		public static Stream toStream(DataSet ds)
		{
			return toStream((object)ds);
		}
		private static Stream toStream(object dt)
		{ 
			JsonSerializer json = getSerializer();

            System.IO.MemoryStream mstrm = new MemoryStream();
            System.IO.StreamWriter sw = new System.IO.StreamWriter(mstrm);
            Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
            writer.Formatting = Formatting.None;

            writer.QuoteChar = '"';
            json.Serialize(writer, dt);
            writer.Flush();

            mstrm.Position = 0;

            return mstrm;
        }
        public static string Serialize(DataTable dt)
        {
            return Serialize((object)dt);
        }
        public static string Serialize(DataSet ds)
        {
            return Serialize((object)ds);
        }
        private static string Serialize(object ds)
        {
            JsonSerializer json = getSerializer();

            System.IO.StringWriter sw = new System.IO.StringWriter();
            Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
            writer.Formatting = Formatting.None;

            writer.QuoteChar = '"';
            json.Serialize(writer, ds);

            string output = sw.ToString();
            writer.Close();
            sw.Close();

            return output;

        }
        public static JsonSerializer getSerializer()
        {

            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = NullValueHandling.Include;
            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            json.Converters.Add(new DataRowConverter());
            json.Converters.Add(new DataTableConverter());
            json.Converters.Add(new DataSetConverter());

            return json;
        }
    }

    /// <summary>
    /// Converts a <see cref="DataRow"/> object to and from JSON.
    /// </summary>
    public class DataRowConverter : JsonConverter
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        public override void WriteJson(JsonWriter writer, object dataRow, JsonSerializer serializer)
        {
            DataRow row = dataRow as DataRow;

            // the customised converter represents the row as an array, not an object
            writer.WriteStartArray();
            foreach (DataColumn column in row.Table.Columns)
            {
                if (row.IsNull(column))
                {
                    writer.WriteNull();
                }
                else
                {
                    writer.WriteValue(row[column]);
                }
            }

            writer.WriteEndArray();
        }

        /// <summary>
        /// Determines whether this instance can convert the specified value type.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type valueType)
        {
            return typeof(DataRow).IsAssignableFrom(valueType);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Converts a DataTable to JSON. Note no support for deserialization
    /// </summary>
    public class DataTableConverter : JsonConverter
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        public override void WriteJson(JsonWriter writer, object dataTable, JsonSerializer serializer)
        {
            DataTable table = dataTable as DataTable;
            DataRowConverter converter = new DataRowConverter();

            writer.WriteStartObject();
			// first write a propertty "columns" that is an array of the column names
			// the customised converter represents the row as an array, not an object
			writer.WritePropertyName("columns");
            writer.WriteStartArray();
            foreach (DataColumn column in table.Columns)
            {
                writer.WriteValue(column.ColumnName);
            }
            writer.WriteEndArray();
            writer.WritePropertyName("rows");
            writer.WriteStartArray();

            foreach (DataRow row in table.Rows)
            {
                converter.WriteJson(writer, row, serializer);
            }

            writer.WriteEndArray();
            writer.WriteEndObject();
        }

        /// <summary>
        /// Determines whether this instance can convert the specified value type.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type valueType)
        {
            return typeof(DataTable).IsAssignableFrom(valueType);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converts a <see cref="DataSet"/> object to JSON. No support for reading.
    /// </summary>
    public class DataSetConverter : JsonConverter
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
        /// <param name="value">The value.</param>
        public override void WriteJson(JsonWriter writer, object dataset, JsonSerializer serializer)
        {
            DataSet dataSet = dataset as DataSet;

            DataTableConverter converter = new DataTableConverter();

            writer.WriteStartObject();

            writer.WritePropertyName("tables");

				writer.WriteStartObject();
				foreach (DataTable table in dataSet.Tables)
				{
					writer.WritePropertyName(table.TableName);
					converter.WriteJson(writer, table, serializer);
				}
		        writer.WriteEndObject();
			writer.WriteEndObject();
        }

        /// <summary>
        /// Determines whether this instance can convert the specified value type.
        /// </summary>
        /// <param name="valueType">Type of the value.</param>
        /// <returns>
        ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type valueType)
        {
            return typeof(DataSet).IsAssignableFrom(valueType);
        }

        /// <summary>
        /// Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
  