﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Softwords.Web.Utilities;

namespace Softwords.Web.Json
{
    public class UtcDateTimeConverter : Newtonsoft.Json.JsonConverter
    {
      
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime)|| objectType == typeof(DateTime?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) return null;
            DateTime? v = (DateTime?)reader.Value;
            v.ToKindUtc();
            return v;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DateTime? v = (DateTime?)value;
            if (v!=null)
                v.ToKindUtc();
            writer.WriteValue(v);
        }
    }
}
