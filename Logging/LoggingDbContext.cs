﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Softwords.Web.Logging
{
    public class LoggingDbContext:DbContext
    {
        private const string LoggingConnection = "LoggingConnection";
        public LoggingDbContext()
            : base(LoggingConnection) { }

        public virtual DbSet<Activity> Activities { get; set; }

        // static method to detect if logging is enabled - ie
        // looks for the ConnectionStrings entry LoggingConnection
        public static bool IsLoggingEnabled()
        {
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings[LoggingConnection];
            return (connection != null);
        }
    }
}
