﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Softwords.Web.Logging
{
    [Table("ActivityLog")]
    public partial class Activity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid activityID { get; set; }
        public DateTime? activityTime { get; set; }
        public string activityLogin { get; set; }
        public string activityTask { get; set; }
        public string activityComment { get; set; }
        public string activityData { get; set; }            // its not really a string... its xml
		public string activityObject { get; set; }
		public int	activityRow { get; set; }
		public string activityScope { get; set; }

	}
}