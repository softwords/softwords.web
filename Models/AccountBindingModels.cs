﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Softwords.Web.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Menu Key")]
        public string MenuKey { get; set; }

        [Display(Name = "Permission Hash")]
        public string Permission { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

  
    }

    public class UserClaim
    {
        string ClaimType { get; set; }
        string ClaimValue { get; set; }
    }
    // administrator creates the record in Register page, creating the user
    // - without password. The user gets an email (including code and token) routing to the confirminvite route 
    // (which is an angualrjs route)
    // In confirm Invite, the user creates their own password. Then they submit the new password to ResetPassword.
    // 
    public class UserInfoBindingModel
    {
        public UserInfoBindingModel()
        {
            Roles = new List<string>();
        }
        
        public string Id { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Menu Key")]
        public string MenuKey { get; set; }

        [Display(Name = "Permission Hash")]
        public string PermissionHash { get; set; }

        [Display(Name = "FilterOn")]
        public string FilterOn { get; set; }

        [Display(Name = "FilterValue")]
        public string FilterValue { get; set; }


        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name ="Roles")]
        public List<string> Roles { get; set; }

        [Display(Name = "Claims")]
        public List<UserClaim> Claims { get; set; }

    }

    // user enters the email for confirmation - we'lll get the password later
    //
    public class ConfirmEmailBindingModel
    {

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        // this is passed in by register, in order that it can be embedded into the confirminvite url
        public string ReturnUrl { get; set; }

        // this allows an alternative form of the message to get used
        public string format { get; set; }

    }
    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    // support forgot password see
    //http://www.asp.net/identity/overview/features-api/account-confirmation-and-password-recovery-with-aspnet-identity
    // the implementation has moved this to web api, not mvc
    public class ForgotPasswordBindingModel
    {
        
        public string Email { get; set; }
    }
    // User goes to ForgotPassword - sends the ForgotPasswordBindingModel to _api/Account/FogotPassword
    // the resetToken is generated and recorded  on the AspNetUser record
    // the link to the reset page is emailed, with the Token as a parameter on that url
    // the user goes to the Url - the code is taken from the parameter and added to the view, the user supplies their email again and the new password
    // Server calls UserManager.ResetPasswordAsync which verifies the match between the token supplied and that recorded on the AspNetUser record
    // then it can overwrite the password.

    public class ResetPasswordBindingModel
    {
        public string Code { get; set; }

        public string UserId { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
