﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web.Models
{
    public class ChartParamsModel
    {
       
        public string Feature { get; set; }
        public System.Data.DataTable FieldOptions { get; set; }
       
    }
}
