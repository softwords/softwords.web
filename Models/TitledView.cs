﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web.Models
{
	public class TitledView
	{
		public TitledView(string title)
		{
			Title = title;
		}
		public TitledView(string title, string program)
		{
			Title = title;
			Program = program;
		}
		public TitledView(string title, string program, string context, string scope)
		{
			Title = title;
			Program = program;
			Context = context;
			Scope = scope;
		}
		public string Title { get; set; }
		public string Program { get; set; }
		public string Context { get; set; }
		public string Scope { get; set; }

		// some helpers
		public bool IsPublic
		{
			get { return Scope?.ToLower() == "public"; }
		}
	}
}
