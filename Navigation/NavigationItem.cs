﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Softwords.Web
{
    /// <summary>
    /// NavigationItem represents an item in the navigation menu
    /// at any level, this can be recursive, 
    /// becuase the NavigationItem children is a List<NavigationItem>
    /// When converted to Json, this structure (List<NavigationItem>) can be passed directly to the Nav menu component in Angular
    /// </summary>
    public class NavigationItem
    {
        public NavigationItem()
        {
            children = new List<NavigationItem>();
        }
        /// <summary>
        /// icons to show on the menu this string will contain fa fa-???
        /// </summary>
        public string iconClasses { get; set; }

        /// <summary>
        /// text in the naviation item
        /// </summary>
        public string label { get; set; }

        /// <summary>
        /// state to go to - this is currently an angular ui-router state that is currently available
        /// </summary>
        public string state { get; set; }

        /// <summary>
        /// chil navigation items
        /// an item should have children, or else a state ( ie be an end point)
        /// but not both
        /// </summary>
        public List<NavigationItem> children { get; private set; }

    }


    public class NavData
    {
        public string navs { get; set; }

    }
    // returned by identities.dbo.GetEffectivePermnissions - one record only
    public class EffectivePermissionData
    {
        public string Permission { get; set; }
    }
    // returned by identities.dbo.GetEffectiveRoles - multiple records
    public class EffectiveRoleData
    {
        public string Role { get; set; }
    }
    // returned by identities.dbo.GetEffectiveMenuKey - one record only
    public class EffectiveMenuKeyData
    {
        public string MenuKey { get; set; }
    }
}
