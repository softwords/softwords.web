﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Softwords.Web
{
    public class NavigationManager
    {

        public async Task<string> getNavigationMenu(string menuName)
        {

            // get the Xml from the database
            //XDocument navs = 
            string navItems;
            using (ApplicationDbContext cxt = new ApplicationDbContext())
            {
                navItems = await cxt.getNavItems(menuName);
            }
            XDocument x = XDocument.Parse(navItems);
            List<NavigationItem> navs = Build(x);
            return Newtonsoft.Json.JsonConvert.SerializeObject(navs);
        }

        /// <summary>
        /// converts the xml into the navigationItem List
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public List<NavigationItem> Build(XDocument xml)
        {

            // the nodes in this xml are already sorted
            // in the order they appear in the menu
            // so we use the depth to understand whether to ascend or decend into the hierarchy

            Stack<List<NavigationItem>> st = new Stack<List<NavigationItem>>();
            List<NavigationItem> top = new List<NavigationItem>();
            st.Push(top);
            List<NavigationItem> ln = top;

            int depth = 0;
            NavigationItem n = null;
            foreach (XElement nav in xml.Root.Elements())
            {

                int newdepth = Convert.ToInt32(nav.Element("depth").Value);
                while (newdepth > depth)
                {
                    // going deeper - push the current list, get the new list = children of the current item
                    // note that newdepth - depth <= 1 or this won;t work
                    st.Push(ln);
                    ln = n.children;

                    depth++;
                }
                while (newdepth < depth)
                {
                    // end of children, next item is ...a sibnling of the parent - an uncle!
                    ln = st.Pop();
                    // can go up multiple levels - to a great-uncle!
                    depth--;
                }
                if (newdepth == depth)
                {

                    // adding a new item - at this point we have the correct List to add to
                    n = new NavigationItem();
                    n.label = nav.Element("label").Value;
                    if (nav.Element("icon") != null)
                        n.iconClasses = nav.Element("icon").Value;
                    if (nav.Element("state") != null)
                        n.state = nav.Element("state").Value;
                    ln.Add(n);
                }


            }

            return top[0].children;
            //string jsn = JsonConvert.SerializeObject(ln);
            //Console.WriteLine(jsn);



        }

    }
}
