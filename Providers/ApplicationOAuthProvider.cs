﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;  // OAuthAuthorizationServerProvider etc
using Softwords.Web;        //common version of ApplicationUserManagwer in Identity.config
using Softwords.Web.Models;     // common version of ApplicationUser
using ad = System.DirectoryServices.AccountManagement;
using System.Configuration;

namespace Softwords.Web.Providers
{
    // inherits from default implementation of IOAuthAuthorizationServerProvider
    // for what these methods do, see:
    //https://msdn.microsoft.com/en-us/library/microsoft.owin.security.oauth.ioauthauthorizationserverprovider%28v=vs.113%29.aspx
    //
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;


        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        // Token request: grant_type= password
        // ie this handles the initial login from the client supplying name and password
        //
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ClaimsIdentity oAuthIdentity = null;
            ClaimsIdentity cookiesIdentity = null;

            // retrieve the ApplicationUserManager stored on the Owin context
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();


            // get the logging info here - before success or failure
            var xd = System.Xml.Linq.XDocument.Parse(string.Format("<request><userAgent><![CDATA[{0}]]></userAgent><clientIp><![CDATA[{1}]]></clientIp></request>"
              , context.Request.Headers["User-Agent"]
              , context.Request.RemoteIpAddress));


            Boolean PasswordValid = false;
            // use the userManager to get the user from the email and password
            // ie this will look first in ASP.NET Identity
            ApplicationUser user = await userManager.FindByEmailAsync(context.UserName);
            if (user != null)
            {
                #region valid identity account
                // user has been found in ASP.NET Identity
                // to log in , 
                // -- mut be not locked out
                // -- must be supplied the correct password

                // check for lockout
                if (await userManager.IsLockedOutAsync(user.Id))
                {
                    setLockoutError(context, userManager);
                    return;
                }
                // not locked out - test the password
                if (userManager.CheckPassword(user, context.Password))
                {
                    #region valid identity account and password
                    // password is correct - we have successfully provided an Identity account
                    PasswordValid = true;
                    // we have entered a correct password, we can reset the failed access count
                    await userManager.ResetAccessFailedCountAsync(user.Id);
                    #endregion
                }
                else

                {
                    #region valid identity account, wrong password
                    //  account is a valid identity account, but password is wrong
                    // deal with a valid asp identity account - but wrong password

                    // if lockout is enabled, increment the count and check if we are now locked out
                    if (await userManager.GetLockoutEnabledAsync(user.Id))
                    {
                        // Record the failure which also may cause the user to be locked out
                        await userManager.AccessFailedAsync(user.Id);

                        if (await userManager.IsLockedOutAsync(user.Id))
                        {
                            // user is now locked out... report this error, log it if logging
                            // and email the user
                            setLockoutError(context, userManager);
                            if (Logging.LoggingDbContext.IsLoggingEnabled())
                            {
                                using (var logDb = new Logging.LoggingDbContext())
                                {
                                    Logging.Activity activity = new Logging.Activity()
                                    {
                                        //activityID = Guid.NewGuid(),
                                        activityLogin = context.UserName,
                                        activityTask = "Web Login - Lockout",
                                        activityTime = DateTime.UtcNow,
                                        activityComment =
		$@"{ConfigurationManager.AppSettings["server"]}:{ConfigurationManager.AppSettings["database"]}
:{ConfigurationManager.AppSettings["context"]}",
										activityScope = ConfigurationManager.AppSettings["scope"],
									activityData = xd.ToString()

                                    };
                                    logDb.Activities.Add(activity);
                                    await logDb.SaveChangesAsync();
                                }
                            }
                            // now send a warnign email to the real account holder
                            string subject = "Password Lockout";
                            string body =
                                "Your password has been locked out on the Scholar Web Portal, due to too many unsuccessful attempts to login.\n" +
                                "The lockout will last for {0} minutes. The unsuccessful login attempts were received from Ip address {1}.";
                            body = String.Format(body, userManager.DefaultAccountLockoutTimeSpan.Minutes.ToString()
                                , context.Request.RemoteIpAddress);
                            await userManager.SendEmailAsync(user.Id, subject, body);

                            return;
                        }
                        // we are tracking failed logins, so report the remaining attempts
                        int accessFailedCount = await userManager.GetAccessFailedCountAsync(user.Id);

                        int attemptsLeft =
                            userManager.MaxFailedAccessAttemptsBeforeLockout - accessFailedCount;

                        if (attemptsLeft <= 2)
                        {
                            context.SetError("Account Lockout"
                                , string.Format(
                                "The user name or password is incorrect. You have {0} more attempt(s) before your account gets locked out.", attemptsLeft)
                                );
                            return;
                        }
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                // try again in the domain - but only if there is not an ASP net identity element for this
                // name....
                ADUserManager admgr = new ADUserManager(userManager);
                
                user = await admgr.FindAsync(context.UserName, context.Password);
                if (user != null)
                {
                    PasswordValid = true;
                }
            }

            if (!PasswordValid)
            {
                #region invalid login - default error and optional logging
                // invalid login - default error and optional logging
                //failed
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                // log failed login here
                if (Logging.LoggingDbContext.IsLoggingEnabled())
                {
                    using (var logDb = new Logging.LoggingDbContext())
                    {
						Logging.Activity activity = new Logging.Activity()
						{
							//activityID = Guid.NewGuid(),
							activityLogin = context.UserName,
							activityTask = "Web Login - Failed",
							activityTime = DateTime.UtcNow,
							activityComment =
		$@"{ConfigurationManager.AppSettings["server"]}:{ConfigurationManager.AppSettings["database"]}
:{ConfigurationManager.AppSettings["context"]}",
							activityScope = ConfigurationManager.AppSettings["scope"],

							activityData = xd.ToString()

                        };
                        logDb.Activities.Add(activity);
                        await logDb.SaveChangesAsync();
                    }
                }
                #endregion
            }
            else
            {
                #region Valid login

                // Log successful login
                // record the user agent
                if (Logging.LoggingDbContext.IsLoggingEnabled())
                {
                    using (var logDb = new Logging.LoggingDbContext())
                    {
                        Logging.Activity activity = new Logging.Activity()
                        {
                            //activityID = Guid.NewGuid(),
                            activityLogin = user.UserName,              // get this from the user object now, it will pick up the domain if AD account
                            activityTask = "Web Login",
                            activityTime = DateTime.UtcNow,
							activityComment =
$@"{ConfigurationManager.AppSettings["server"]}:{ConfigurationManager.AppSettings["database"]}:{ConfigurationManager.AppSettings["context"]}",
							activityScope = ConfigurationManager.AppSettings["scope"],

							activityData = xd.ToString()
                        };
                        logDb.Activities.Add(activity);
                        await logDb.SaveChangesAsync();
                    }
                }
                oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
                    OAuthDefaults.AuthenticationType);
                cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                NavigationManager nm = new NavigationManager();
                user.ClientMenu = await nm.getNavigationMenu(user.MenuKey);

                // see http://bitoftech.net/2014/06/01/token-based-authentication-asp-net-web-_api-2-owin-asp-net-identity/
                // for some explanation of what this is all about

                AuthenticationProperties properties = CreateProperties(user, context.UserName, oAuthIdentity);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

                // validated by ticket allows us to pass authentication properties back to the client
                // validation of ClaimsIdentity is shown in the link above, so this is the more general form
                //
                // from IOAuthAuthorizationServerProvider docs:
                // ' To issue an access token the context.Validated must be called with a new ticket
                // containing the claims about the resource owner which should be associated with the access token '
                context.Validated(ticket);

                // cookie sign in???? presumably if we take this out there
                //context.Request.Context.Authentication.SignIn(cookiesIdentity);
                #endregion
            }
        }

        // called at the final stage of a successful token endpoint request.
        // this call can:
        // make final modification of the claims being used to issue access
        // add additional response parameters to go in the JSON
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            // clientID means ... the client app? so can be generally ignored
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }


        // this: http://stackoverflow.com/questions/20693082/setting-the-redirect-uri-in-asp-net-identity
        // explains a bit about where the below comes from,
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        //--------------------
        // this is the critical place for adding any properties you want returned with the login token
        //---------------------
        public static AuthenticationProperties CreateProperties(ApplicationUser user, string userName,
                                    ClaimsIdentity oAuthIdentity)
        {
           
            string menuContent = user.ClientMenu;
            string menuHome = user.Navigation.homestate??String.Empty;
            string proxy = user.Proxy;
            string permissions = user.PermissionHash;

            // remove any Permission claims, and add only the aggregate one
            if (user.Proxy == null)
            {
                permissions = oAuthIdentity.FindFirst("Permission").Value;
            }
            else
            {
                // an AD account
                var c = oAuthIdentity.FindFirst("Permission");
                while (c != null)
                {
                    oAuthIdentity.RemoveClaim(c);
                    c = oAuthIdentity.FindFirst("Permission");
                }
                permissions = user.PermissionHash;
                oAuthIdentity.AddClaim(new Claim("Permission", permissions));
            }



            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", user.Email },             // get a version that is right for both the ad and ef cases
                // here we send back the roles - note the change of signature in this function
                // see http://stackoverflow.com/questions/23734791/how-can-i-send-authorization-information-back-to-my-client-app-when-using-angula
                // however have modified this idea to send roles as an array, as expected by the client
                // based on this example
                //http://plnkr.co/edit/UkHDqFD8P7tTEqSaCOcc?p=preview
                 { "roles",string.Join(",",oAuthIdentity.Claims.Where(c=> c.Type == ClaimTypes.Role).Select(c => c.Value).ToArray())},
                 { "menu", menuContent},
                 { "home", menuHome},
                { "filters", string.Join(",",oAuthIdentity.Claims.Where(c=> c.Type.StartsWith("filter")).Select(c => c.ToString().Substring("filter".Length)).ToArray())},
                {"p",permissions }   // permission Hash - defaulting to all permissions ON
            };
            // 7 7 2016
            // deal with some specific values - you cannot send a null back here
            // it gives an error at Microsoft.Owin.Security.DataHandler.Serializer.PropertiesSerializer.Write
            if (proxy != null)
            {
                data.Add("proxy", proxy);
            };

            return new AuthenticationProperties(data);
        }
        #region helpers
        private void setLockoutError(OAuthGrantResourceOwnerCredentialsContext context, ApplicationUserManager userManager)
        {
            context.SetError(
                string.Format("Account Lockout", "Your account has been locked out for {0} minute(s) due to multiple failed login attempts."
                    , userManager.DefaultAccountLockoutTimeSpan.Minutes.ToString()
                )
            );
        }
        #endregion
    }

    class AdIdentity : System.Security.Principal.IIdentity
    {
        string _name = String.Empty;
        public AdIdentity(string name)
        {
            _name = name;
        }
        public string Name
        {
            get { return _name; }
        }
        public bool IsAuthenticated
        {
            get { return true; }
        }
        public string AuthenticationType
        {
            get { return "Bearer"; }
        }
    }

}