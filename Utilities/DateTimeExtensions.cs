﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web.Utilities
{
    /// <summary>
    /// This code from
    /// https://stackoverflow.com/questions/29496306/how-to-specify-that-datetime-objects-retrieved-from-entityframework-should-be-da
    /// The point is to set the DateTime.Kind property which will affect how JSON serializes the date, and how it is
    /// ulitmately interprested and displayed in the web client.
    /// Converting to Utc will include Z at the end of the end ie the date is in TimeZone 0
    /// Simiarly converting to Local will put Z10 (e.g.) ie the serv'ers time zone.
    /// In both cases, Javascript will adjust the date to local time zone.
    /// If the DataTime is unspecified, there is no timeZone in the generated JSON, so it is handled "verbatim" in the Javascript.
    /// see https://www.w3.org/TR/timezone for a good theoretical discussion of "floating" or Timezone-independent dates.
    /// e.g. when reporting a Date of Birth, we don;t want to see it converted to the client's time zone!
    /// 
    /// </summary>
    public static class DateTimeExtensions
    {
        public static DateTime ToKindUtc(this DateTime value)
        {
            return KindUtc(value);
        }
        public static DateTime? ToKindUtc(this DateTime? value)
        {
            return KindUtc(value);
        }
        public static DateTime ToKindLocal(this DateTime value)
        {
            return KindLocal(value);
        }
        public static DateTime? ToKindLocal(this DateTime? value)
        {
            return KindLocal(value);
        }
        public static DateTime SpecifyKind(this DateTime value, DateTimeKind kind)
        {
            if (value.Kind != kind)
            {
                return DateTime.SpecifyKind(value, kind);
            }
            return value;
        }
        public static DateTime? SpecifyKind(this DateTime? value, DateTimeKind kind)
        {
            if (value.HasValue)
            {
                return DateTime.SpecifyKind(value.Value, kind);
            }
            return value;
        }
        public static DateTime KindUtc(DateTime value)
        {
            if (value.Kind == DateTimeKind.Unspecified)
            {
                return DateTime.SpecifyKind(value, DateTimeKind.Utc);
            }
            else if (value.Kind == DateTimeKind.Local)
            {
                return value.ToUniversalTime();
            }
            return value;
        }
        public static DateTime? KindUtc(DateTime? value)
        {
            if (value.HasValue)
            {
                return KindUtc(value.Value);
            }
            return value;
        }
        public static DateTime KindLocal(DateTime value)
        {
            if (value.Kind == DateTimeKind.Unspecified)
            {
                return DateTime.SpecifyKind(value, DateTimeKind.Local);
            }
            else if (value.Kind == DateTimeKind.Utc)
            {
                return value.ToLocalTime();
            }
            return value;
        }
        public static DateTime? KindLocal(DateTime? value)
        {
            if (value.HasValue)
            {
                return KindLocal(value.Value);
            }
            return value;
        }
    }
}
