﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web
{
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Add to dictionary if key does not already exist
        /// </summary>
        /// <param name="dic">dictionary</param>
        /// <param name="key">key</param>
        /// <param name="value">value to add</param>
        public static void Insert(this IDictionary<string, object> dic, string key, object value)
        {
            if (!dic.ContainsKey(key))
                dic.Add(key, value);
        }

        /// <summary>
        /// Update dictionary only if the key already exists
        /// </summary>
        /// <param name="dic">dictionary</param>
        /// <param name="key">key</param>
        /// <param name="value">value to add</param>
        public static void Update(this IDictionary<string, object> dic, string key, object value)
        {
            if (dic.ContainsKey(key))
            {
                dic[key] = value;
            }
        }

        /// <summary>
        /// Add to dictionary if key not currently in the dictionary, otherwise update existing entry
        /// </summary>
        /// <param name="dic">dictionary</param>
        /// <param name="key">key</param>
        /// <param name="value">value to add</param>
        public static void InsertOrUpdate(this IDictionary<string, object> dic, string key, object value)
        {
            if (dic.ContainsKey(key))
            {
                dic[key] = value;
            }
            else
            {
                dic.Add(key, value);
            }
        }

        #region object to Dictionary

        /// <summary>
        /// Take an object, and convert it to a dictionary, based on its properties
        /// https://gist.github.com/jarrettmeyer/798667/a87f9bcac2ec68541511f17da3c244c0e05bdc49
        /// </summary>
        /// <param name="source">the object</param>
        /// <returns>the dictionary</returns>
        public static IDictionary<string, object> ToDictionary(this object source)
        {
            return source.ToDictionary<object>(false);
        }

        public static IDictionary<string, object> ToDictionary(this object source, bool convertNames)
        {
            return source.ToDictionary<object>(convertNames);
        }

        public static IDictionary<string, T> ToDictionary<T>(this object source)
        {
            return ToDictionary<T>(source, false);
        }
        /// <summary>
        /// load a dictionary from the properties of an object
        /// </summary>
        /// <typeparam name="T">type of dictionary value</typeparam>
        /// <param name="source">object to convert from</param>
        /// <param name="convertNames">if true, replace _ in peroperty names with - (defaut: false)</param>
        /// <returns></returns>
        public static IDictionary<string, T> ToDictionary<T>(this object source, bool convertNames)
        {
            if (source == null) ThrowExceptionWhenSourceArgumentIsNull();

            var dictionary = new Dictionary<string, T>();
            foreach (System.ComponentModel.PropertyDescriptor property in System.ComponentModel.TypeDescriptor.GetProperties(source))
            {
                object value = property.GetValue(source);
                string name = String.Empty;
                if (IsOfType<T>(value))
                {
                    name = (convertNames ? property.Name.Replace("_", "-") : property.Name);
                    dictionary.Add(name, (T)value);
                }
            }
            return dictionary;
        }

        private static bool IsOfType<T>(object value)
        {
            return value is T;
        }

        private static void ThrowExceptionWhenSourceArgumentIsNull()
        {
            throw new NullReferenceException("Unable to convert anonymous object to a dictionary. The source anonymous object is null.");
        }

        #endregion
    }
}
