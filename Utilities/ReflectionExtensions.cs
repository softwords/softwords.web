﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace Softwords.Web
{
    public static class ReflectionExtensions
    {
        /// <summary>
        /// return an attribute from the memberInfo
        /// </summary>
        /// <typeparam name="T">the type of the attribute to return</typeparam>
        /// <param name="memberInfo">the memberInfo of the describing the property to get the attribute of</param>
        /// <returns>the attribute of type T</returns>
        public static T getCustomAttribute<T>(this System.Reflection.MemberInfo memberInfo)
        {
            return memberInfo
                        .GetCustomAttributes(typeof(T), false)
                        .Cast<T>()
                        .SingleOrDefault();
        }

        /// <summary>
        /// Return True if the member has the specified attribute
        /// </summary>
        /// <typeparam name="T">type of attribute to look for</typeparam>
        /// <param name="memberInfo">member to look in</param>
        /// <returns></returns>
        public static bool hasCustomAttribute<T>(this System.Reflection.MemberInfo memberInfo)
        {
            return (memberInfo.getCustomAttribute<T>() != null);
        }

        /// <summary>
        /// Return the ValidationAttributes on the member
        /// </summary>
        /// <param name="memberInfo">memberInfo object to test</param>
        /// <returns>array of ValidationAttributes</returns>
        public static ValidationAttribute[] getValidationAttributes(this System.Reflection.MemberInfo memberInfo)
        {
            return memberInfo
                       .GetCustomAttributes(false)
                       .Where(attr =>
                      {
                          return (attr is ValidationAttribute);
                      })
                      .Cast<ValidationAttribute>()
                      .ToArray();
        }

    }
}
