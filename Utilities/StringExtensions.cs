﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softwords.Web
{
    public static class StringExtensions
    {
        /// <summary>
        /// Replace a string with a default value, if it is null or empty
        /// </summary>
        /// <param name="stringToTest">string to test</param>
        /// <param name="defaultString">default string value</param>
        /// <returns></returns>
        public static string applyDefault(this string stringToTest, string defaultString)
        {
            if (stringToTest == null || stringToTest == String.Empty)
                return defaultString;
            return stringToTest;
        }
        public static bool isValidEmail(this string stringToTest)
        {
            if (stringToTest == null || stringToTest == String.Empty)
                return false;
            string patternEmail =
                    @"^$|" +           // allow an empty string ^$
                    @"^([0-9a-zA-Z]" + //Start with a digit or alphabetical
                    @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continuous or ending +-_. chars in email
                    @")+" +
                    @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
            var rx = new System.Text.RegularExpressions.Regex(patternEmail);
            return rx.IsMatch(stringToTest);
        }

    }
}
