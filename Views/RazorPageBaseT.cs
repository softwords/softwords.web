﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Softwords.Web
{
    // why abstract?
    // http://stackoverflow.com/questions/3980032/asp-mvc-razor-view-extension-methods-how-to-create-global-view-methods
    //
    // see also http://haacked.com/archive/2011/02/21/changing-base-type-of-a-razor-view.aspx/
    // This needs to exist in generic and nongeneric versions
    //
    public abstract class RazorPageBase<TModel> : WebViewPage<TModel> where TModel : class
    {
        private string instanceContext;
        public string InstanceContext
        {
            get
            {   if (instanceContext == null)
                {
                    instanceContext = AppSetting("context");
                }
                return instanceContext;
            }
        }
        public bool IsInstanceContext(string context)
        {
            return (InstanceContext == context);
        }
        public bool IsInstanceContext(string[] contexts)
        {
            foreach (string context in contexts)
            {
                if (InstanceContext == context)
                {
                    return true;
                }
            }
            return false;
        }

        public string AppSetting(string settingName)
        {
            return System.Web.Configuration.WebConfigurationManager.AppSettings[settingName] ?? String.Empty;
        }

    }
}
